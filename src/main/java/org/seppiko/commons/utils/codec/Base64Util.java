/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.codec;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import org.seppiko.commons.utils.CharUtil;

/**
 * Base64 encoding and decoding utility.
 * base on Java 8+.
 *
 * @see Base64
 * @author Leonard Woo
 */
public class Base64Util {

  private static final Charset DEFAULT_CHARSET = StandardCharsets.ISO_8859_1;

  private Base64Util() {}

  /**
   * Get Base64 code.
   *
   * @param data data source.
   * @return Base64 byte array.
   */
  public static byte[] encode(byte[] data) {
    return Base64.getEncoder().encode(data);
  }

  /**
   * Get Base64 code to String.
   *
   * @param data data source.
   * @return Base64 string.
   */
  public static String encodeString(byte[] data) {
    return CharUtil.charsetDecode(DEFAULT_CHARSET, encode(data)).toString();
  }

  /**
   * Base64 decode.
   *
   * @param data Base64 source.
   * @return raw byte array.
   */
  public static byte[] decode(byte[] data) {
    return Base64.getDecoder().decode(data);
  }

  /**
   * Base64 decode from String.
   *
   * @param data Base64 source.
   * @return raw byte array.
   */
  public static byte[] decode(String data) {
    return decode(DEFAULT_CHARSET.encode(data).array());
  }

  /**
   * Base64 decode to string.
   *
   * @param data Base64 byte array.
   * @return raw string.
   */
  public static String decodeString(byte[] data) {
    return new String(decode(data));
  }

  /**
   * Base64 decode to string.
   *
   * @param data Base64 string.
   * @return raw string.
   */
  public static String decodeString(String data) {
    return new String(decode(data));
  }

  /**
   * Get Base64 code with URL, and without padding.
   *
   * @param data data source.
   * @return Base64 byte array.
   */
  public static byte[] encodeUrl(byte[] data) {
    return Base64.getUrlEncoder().withoutPadding().encode(data);
  }

  /**
   * Get Base64 code to String with URL, and without padding.
   *
   * @param data data source.
   * @return Base64 string.
   */
  public static String encodeUrlString(byte[] data) {
    return CharUtil.charsetDecode(DEFAULT_CHARSET, encodeUrl(data)).toString();
  }

  /**
   * Base64 decode with URL.
   *
   * @param data data source.
   * @return Base64 byte array.
   */
  public static byte[] decodeUrl(byte[] data) {
    return Base64.getUrlDecoder().decode(data);
  }

  /**
   * Base64 decode from String with URL.
   *
   * @param data Base64 source.
   * @return Base64 decode.
   */
  public static byte[] decodeUrl(String data) {
    return decodeUrl(DEFAULT_CHARSET.encode(data).array());
  }

  /**
   * Get Base64 code with MIME type.
   *
   * @param data data source.
   * @return Base64 byte array.
   */
  public static byte[] encodeMime(byte[] data) {
    return Base64.getMimeEncoder().encode(data);
  }

  /**
   * Get Base64 code with MIME type.
   *
   * @param data data source.
   * @return Base64 string.
   */
  public static String encodeMimeString(byte[] data) {
    return CharUtil.charsetDecode(DEFAULT_CHARSET, encodeMime(data)).toString();
  }

  /**
   * Base64 decode with MIME type.
   *
   * @param data Base64 byte array.
   * @return raw data.
   */
  public static byte[] decodeMime(byte[] data) {
    return Base64.getMimeDecoder().decode(data);
  }

  /**
   * Base64 decode from String with MIME type.
   *
   * @param data Base64 string.
   * @return raw data.
   */
  public static byte[] decodeMime(String data) {
    return decodeMime(DEFAULT_CHARSET.encode(data).array());
  }
}
