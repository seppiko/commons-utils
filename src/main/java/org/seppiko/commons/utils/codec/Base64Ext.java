/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.codec;

import java.io.Serial;
import java.io.Serializable;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;
import org.seppiko.commons.utils.CharUtil;

/**
 * Base64 EXTension encoding and decoding with custom alphabet.
 *
 * @author Leonard Woo
 * @see java.util.Base64
 */
public class Base64Ext implements BaseNCodec, Serializable {

  @Serial
  private static final long serialVersionUID = -7263003180491333921L;

  /** Default padding char */
  private static final char DEFAULT_PADDING = CharUtil.EQUALS_SIGN;

  /** Default newline chars */
  private static final char[] DEFAULT_NEWLINE_CHARS = CharUtil.CRLF.toCharArray();

  /** Default linebreak length, identity in RFC2045 and RFC4880 */
  private static final int DEFAULT_LINEBREAK_LENGTH = 76;

  /** base64 encode table */
  private final char[] base64Table;
  /** base64 decode table */
  private final byte[] base64DecodeTable;
  /** base64 padding */
  private boolean doPadding;
  /** base64 padding char */
  private char padding;
  /** base64 auto newline */
  private final boolean autoNewline;
  /** base64 linebreak length */
  private int linebreakLength;
  /** base64 newline chars */
  private char[] newline;
  /** base64 encoding */
  private Charset encoding;

  /**
   * Base64 EXTension is a custom alphabet implementation based on the Base64 codec algorithm.
   *
   * @param base64Table base64 alphabet table.
   * @param autoNewline enable auto newline. Default is {@code '\r\n'} like MIME with at most 76.
   * @throws IllegalArgumentException base64 alphabet table must have 64 chars.
   */
  public Base64Ext(char[] base64Table, boolean autoNewline) {
    if (base64Table.length != 64) {
      throw new IllegalArgumentException("Base64 alphabet must have 64 chars");
    }
    this.base64Table = base64Table;

    this.doPadding = true;
    this.padding = DEFAULT_PADDING;

    this.autoNewline = autoNewline;
    this.linebreakLength = autoNewline ? DEFAULT_LINEBREAK_LENGTH : -1;
    this.newline = DEFAULT_NEWLINE_CHARS;

    this.encoding = StandardCharsets.ISO_8859_1;

    this.base64DecodeTable = new byte[256];
    Arrays.fill(this.base64DecodeTable, (byte) -1);
    for (int i = 0; i < base64Table.length; i++) {
      this.base64DecodeTable[base64Table[i]] = (byte) i;
    }
    base64DecodeTable[DEFAULT_PADDING] = -2;
  }

  /**
   * Set newline chars and line break length.
   *
   * @param newline newline chars, default is {@code \r\n}.
   * @param linebreakLength line break length, default is 76.
   * @return this {@link Base64Ext} instance
   * @throws IllegalArgumentException If autoNewline is false throw this.
   * @throws NullPointerException If newline is null or empty or linebreakLength is less than or equal to 0.
   */
  public Base64Ext setNewline(char[] newline, int linebreakLength)
      throws IllegalArgumentException, NullPointerException {
    if (!autoNewline) {
      throw new IllegalArgumentException("auto newline must be enable");
    }
    if (null == newline || newline.length == 0) {
      throw new NullPointerException("newline chars must be not null or empty");
    }
    if (linebreakLength <= 0) {
      throw new NullPointerException("linebreak length must than more 0");
    }
    this.newline = newline;
    this.linebreakLength = linebreakLength;
    return this;
  }

  /**
   * Returns a Base64 EXTension instance, but without adding any padding
   * character at the end of the encoded byte data.
   *
   * @return this {@link Base64Ext} instance
   */
  public Base64Ext withoutPadding() {
    this.doPadding = false;
    base64DecodeTable[padding] = -2;
    return this;
  }

  /**
   * Returns a Base64 EXTension instance, enable padding and adding a padding
   * character at the end of the encoded byte data.
   *
   * @param padding the padding char.
   * @return this {@link Base64Ext} instance.
   * @throws IllegalArgumentException padding character is {@code '\0'}.
   */
  public Base64Ext setPadding(char padding) throws IllegalArgumentException {
    if (padding == 0) {
      throw new IllegalArgumentException("padding character must not be '\\0'");
    }
    this.doPadding = true;
    this.padding = padding;
    base64DecodeTable[padding] = -2;
    return this;
  }

  /**
   * Set Base64 EXTension encoding, default is {@code ISO-8859-1}.
   *
   * @param encoding this {@link Base64Ext} encoding.
   * @return this {@link Base64Ext} instance.
   */
  public Base64Ext setEncoding(Charset encoding) {
    this.encoding = encoding;
    return this;
  }

  /**
   * Base64 EXTension encode the given data and return a newly allocated byte array with the result.
   *
   * @param data the data to encode.
   * @return a newly allocated byte array with the result.
   */
  public byte[] encode(byte[] data) {
    return encode0(data);
  }

  /**
   * Base64 EXTension encode the given data and return a newly allocated String with the result.
   *
   * @param data the data to encode.
   * @return a newly allocated String with the result.
   */
  @Override
  public String encodeString(byte[] data) {
    return CharUtil.charsetDecode(encoding, encode(data)).toString();
  }

  /**
   * Decode the Base64 EXTension data in input and return the data in a new byte array.
   *
   * @param data Base64 source.
   * @return raw byte array.
   * @throws IllegalArgumentException data something is wrong.
   */
  public byte[] decode(byte[] data) throws IllegalArgumentException {
    return decode0(data);
  }

  /**
   * Decode the Base64 EXTension data in input and return the data in a new byte array.
   *
   * @param data the data to decode.
   * @return the data in a new byte array.
   * @throws NullPointerException data is {@code null}.
   * @throws IllegalArgumentException data something is wrong.
   */
  @Override
  public byte[] decode(String data) throws IllegalArgumentException, NullPointerException {
    Objects.requireNonNull(data);
    return decode(encoding.encode(data).array());
  }

  private byte[] encode0(byte[] data) {
    byte[] newline = CharUtil.charsetEncode(encoding, CharBuffer.wrap(this.newline));
    return new Encoder(base64Table, newline, linebreakLength, doPadding, (byte) padding)
        .encode0(data, 0, data.length);
  }

  private byte[] decode0(byte[] data) throws IllegalArgumentException {
    return new Decoder(base64DecodeTable, autoNewline, (byte) padding)
        .decode0(data, 0, data.length);
  }

  /**
   * @see java.util.Base64.Encoder
   */
  private record Encoder(char[] base64Table, byte[] newline, int lineMax, boolean doPadding,
                         byte padding) {

    /**
     * Calculates the length of the encoded output bytes.
     *
     * @param srclen length of the bytes to encode.
     * @return length of the encoded bytes, or -1 if the length overflows.
     *
     */
    private int encodedOutLength(int srclen) {
      int len;
      try {
        if (doPadding) {
          len = Math.multiplyExact(4, (Math.addExact(srclen, 2) / 3));
        } else {
          int n = srclen % 3;
          len = Math.addExact(Math.multiplyExact(4, (srclen / 3)), (n == 0 ? 0 : n + 1));
        }
        if (lineMax > 0) {                             // line separators
          len = Math.addExact(len, (len - 1) / lineMax * newline.length);
        }
      } catch (ArithmeticException ex) {
        len = -1;
      }
      return len;
    }

    private void encodeBlock(byte[] src, int sp, int sl, byte[] dst, int dp) {
      char[] base64 = base64Table;
      for (int sp0 = sp, dp0 = dp; sp0 < sl; ) {
        int bits = (src[sp0++] & ((int) MASK_8BITS)) << 16 |
            (src[sp0++] & ((int) MASK_8BITS)) << 8 |
            (src[sp0++] & ((int) MASK_8BITS));
        dst[dp0++] = (byte) base64[((bits >>> 18) & ((int) MASK_6BITS))];
        dst[dp0++] = (byte) base64[(bits >>> 12) & ((int) MASK_6BITS)];
        dst[dp0++] = (byte) base64[(bits >>> 6) & ((int) MASK_6BITS)];
        dst[dp0++] = (byte) base64[bits & ((int) MASK_6BITS)];
      }
    }

    private byte[] encode0(byte[] src, int off, int end) {
      char[] base64 = base64Table;

      byte[] dst = new byte[encodedOutLength(src.length)];

      int sp = off;
      int slen = (end - off) / 3 * 3;
      int sl = off + slen;
      if (lineMax > 0 && slen > (lineMax / 4 * 3)) {
        slen = (lineMax / 4 * 3);
      }
      int dp = 0;
      while (sp < sl) {
        int sl0 = Math.min(sp + slen, sl);
        encodeBlock(src, sp, sl0, dst, dp);
        int dlen = (sl0 - sp) / 3 * 4;
        dp += dlen;
        sp = sl0;
        if (dlen == lineMax && sp < end) {
          for (byte b : newline) {
            dst[dp++] = b;
          }
        }
      }
      if (sp < end) {               // 1 or 2 leftover bytes
        int b0 = src[sp++] & ((int) MASK_8BITS);
        dst[dp++] = (byte) base64[b0 >> 2];
        if (sp == end) {
          dst[dp++] = (byte) base64[(b0 << 4) & ((int) MASK_6BITS)];
          if (doPadding) {
            dst[dp++] = padding;
            dst[dp++] = padding;
          }
        } else {
          int b1 = src[sp++] & ((int) MASK_8BITS);
          dst[dp++] = (byte) base64[(b0 << 4) & ((int) MASK_6BITS) | (b1 >> 4)];
          dst[dp++] = (byte) base64[(b1 << 2) & ((int) MASK_6BITS)];
          if (doPadding) {
            dst[dp++] = padding;
          }
        }
      }

      return (dp != dst.length) ? Arrays.copyOf(dst, dp) : dst;
    }
  }

  /**
   * @see java.util.Base64.Decoder
   */
  private record Decoder(byte[] base64DecodeTable, boolean autoNewline, byte padding) {

    private int decodedOutLength(byte[] src, int sp, int sl) {
      int paddings = 0;
      int len = sl - sp;
      if (len == 0) {
        return 0;
      }
      if (len < 2) {
        if (autoNewline && base64DecodeTable[0] == -1) {
          return 0;
        }
        throw new IllegalArgumentException(
            "Input byte[] should at least have 2 bytes for base64 bytes");
      }
      if (autoNewline) {
        // scan all bytes to fill out all non-alphabet. a performance
        // trade-off of pre-scan or Arrays.copyOf
        int n = 0;
        while (sp < sl) {
          int b = src[sp++] & ((int) MASK_8BITS);
          if (b == padding) {
            len -= (sl - sp + 1);
            break;
          }
          if ((b = base64DecodeTable[b]) == -1) {
            n++;
          }
        }
        len -= n;
      } else {
        if (src[sl - 1] == padding) {
          paddings++;
          if (src[sl - 2] == padding) {
            paddings++;
          }
        }
      }
      if (paddings == 0 && (len & ((int) MASK_2BITS)) != 0) {
        paddings = (4 - (len & ((int) MASK_2BITS)));
      }

      // If len is near to Integer.MAX_VALUE, (len + 3)
      // can possibly overflow, perform this operation as
      // long and cast it back to integer when the value comes under
      // integer limit. The final value will always be in integer
      // limits
      return 3 * (int) ((len + 3L) / 4) - paddings;
    }

    private int decodeBlock(byte[] src, int sp, int sl, byte[] dst, int dp) {
      int sl0 = sp + ((sl - sp) & ~0b11);
      int new_dp = dp;
      while (sp < sl0) {
        int b1 = base64DecodeTable[src[sp++] & ((int) MASK_8BITS)];
        int b2 = base64DecodeTable[src[sp++] & ((int) MASK_8BITS)];
        int b3 = base64DecodeTable[src[sp++] & ((int) MASK_8BITS)];
        int b4 = base64DecodeTable[src[sp++] & ((int) MASK_8BITS)];
        if ((b1 | b2 | b3 | b4) < 0) {    // non base64 byte
          return new_dp - dp;
        }
        int bits0 = b1 << 18 | b2 << 12 | b3 << 6 | b4;
        dst[new_dp++] = (byte) (bits0 >> 16);
        dst[new_dp++] = (byte) (bits0 >> 8);
        dst[new_dp++] = (byte) (bits0);
      }
      return new_dp - dp;
    }

    private byte[] decode0(byte[] src, int sp, int sl) throws IllegalArgumentException {
      byte[] dst = new byte[decodedOutLength(src, 0, src.length)];
      int dp = 0;
      int bits = 0;
      int shiftto = 18;       // pos of first byte of 4-byte atom

      while (sp < sl) {
        if (shiftto == 18 && sp < sl - 4) {       // fast path
          int dl = decodeBlock(src, sp, sl, dst, dp);
          /*
           * Calculate how many characters were processed by how many
           * bytes of data were returned.
           */
          int chars_decoded = ((dl + 2) / 3) * 4;

          sp += chars_decoded;
          dp += dl;
        }
        if (sp >= sl) {
          // we're done
          break;
        }
        int b = src[sp++] & ((int) MASK_8BITS);
        if ((b = base64DecodeTable[b]) < 0) {
          if (b == -2) {         // padding byte '='
            // =     shiftto==18 unnecessary padding
            // x=    shiftto==12 a dangling single x
            // x     to be handled together with non-padding case
            // xx=   shiftto==6&&sp==sl missing last =
            // xx=y  shiftto==6 last is not =
            if (shiftto == 6 && (sp == sl || src[sp++] != padding) ||
                shiftto == 18) {
              throw new IllegalArgumentException(
                  "Input byte array has wrong 4-byte ending unit");
            }
            break;
          }
          if (autoNewline) { // skip if for rfc2045
            continue;
          } else {
            throw new IllegalArgumentException(
                "Illegal base64 character " + Integer.toString(src[sp - 1], 16));
          }
        }
        bits |= (b << shiftto);
        shiftto -= 6;
        if (shiftto < 0) {
          dst[dp++] = (byte) (bits >> 16);
          dst[dp++] = (byte) (bits >> 8);
          dst[dp++] = (byte) (bits);
          shiftto = 18;
          bits = 0;
        }
      }
      // reached end of byte array or hit padding '=' characters.
      if (shiftto == 6) {
        dst[dp++] = (byte) (bits >> 16);
      } else if (shiftto == 0) {
        dst[dp++] = (byte) (bits >> 16);
        dst[dp++] = (byte) (bits >> 8);
      } else if (shiftto == 12) {
        // dangling single "x", incorrectly encoded.
        throw new IllegalArgumentException(
            "Last unit does not have enough valid bits");
      }
      // anything left is invalid, if is not MIME.
      // if MIME, ignore all non-base64 character
      while (sp < sl) {
        if (autoNewline && base64DecodeTable[src[sp++] & ((int) MASK_8BITS)] < 0) {
          continue;
        }
        throw new IllegalArgumentException(
            "Input byte array has incorrect ending byte at " + sp);
      }

      return (dp != dst.length) ? Arrays.copyOf(dst, dp) : dst;
    }
  }
}
