/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.codec;

/**
 * Abstract superclass for Base-N encoders and decoders.
 *
 * @author Leonard Woo
 */
public interface BaseNCodec {

  /** Mask used to extract 8 bits, used in decoding bytes */
  long MASK_8BITS = 0xFFL;
  /** Mask used to extract 6 bits, used when encoding bytes */
  long MASK_6BITS = 0x3FL;
  /** Mask used to extract 5 bits, used when encoding bytes */
  long MASK_5BITS = 0x1FL;
  /** Mask used to extract 4 bits, used when decoding character. */
  long MASK_4BITS = 0x0FL;
  /** Mask used to extract 2 bits, used when decoding character. */
  long MASK_2BITS = 0x03L;

  /** Mask used to extract the upper 4 bits, used when decoding character. */
  long MASK_UPPER_4BITS = 0xF0L;

  /** bits per encoded */
  int BITS_PER_ENCODED_BYTE = 5;
  /** bytes pre encoded */
  int BYTES_PER_ENCODED_BLOCK = 8;
  /** bits width */
  int BIT_WIDTH = 5;

  /**
   * BaseN-encode the given data and return a newly allocated String with the result.
   *
   * @param data the data to encode.
   * @return a newly allocated String with the result.
   */
  String encodeString(byte[] data);

  /**
   * Decode the BaseN-encoded data in input and return the data in a new byte array.
   *
   * @param data the data to decode.
   * @return the data in a new byte array.
   * @throws IllegalArgumentException data something is wrong.
   * @throws NullPointerException data is {@code null}.
   */
  byte[] decode(String data) throws IllegalArgumentException, NullPointerException;
}
