/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.codec;

import java.io.Serial;
import java.io.Serializable;
import org.seppiko.commons.utils.StringUtil;

/**
 * Base16 encoding and decoding.
 *
 * @see <a href="https://datatracker.ietf.org/doc/html/rfc4648#section-8">RFC4648</a>
 * @author Leonard Woo
 */
public class Base16 implements BaseNCodec, Serializable {

  @Serial
  private static final long serialVersionUID = -3844227952413070095L;

  /** Base16 characters are 4 bits in length. */
  private static final int BITS_PER_ENCODED_BYTE = 4;

  /** Lowercase character array */
  private static final char[] LOWER_CASE_TABLE = {
      'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
      'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'
  };

  /** Uppercase character array */
  private static final char[] UPPER_CASE_TABLE = {
      'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
      'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P'
  };

  private static final byte[] CASE_DECODE_TABLE = {
      //  0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, // 00-0f
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, // 10-1f
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, // 20-2f
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, // 30-3f
      -1,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, // 40-4f A-O
      15,                                                             // 50 P
          -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, // 51-5f
      -1,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, // 60-6f a-o
      15, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,                     // 70-7a p
  };

  /**
   * The most widely used Base16 alphabet is defined in RFC 4648.
   */
  public static final Base16 RFC4648 = new Base16(HexUtil.HEXADECIMAL, HexUtil.HEXADECIMAL_DECODE_TABLE);

  /**
   * The most widely used Base16 alphabet is defined in RFC 4648 with uppercase.
   */
  public static final Base16 RFC4648_UPPER = new Base16(HexUtil.HEXADECIMAL_UPPER, HexUtil.HEXADECIMAL_DECODE_TABLE);

  /**
   * The used uppercase alphabet [A-P]
   */
  public static final Base16 BASE16_UPPERCASE = new Base16(UPPER_CASE_TABLE, CASE_DECODE_TABLE);

  /**
   * The used lowercase alphabet [a-p]
   */
  public static final Base16 BASE16_LOWERCASE = new Base16(LOWER_CASE_TABLE, CASE_DECODE_TABLE);

  /** base16 encode table */
  private final char[] encodeTable;
  /** base16 decode table */
  private final byte[] decodeTable;

  private Base16(char[] encodeTable, byte[] decodeTable) {
    this.encodeTable = encodeTable;
    this.decodeTable = decodeTable;
  }

  /**
   * convert byte array to Base16.
   *
   * @param data byte array data.
   * @return hex char array.
   */
  @Override
  public String encodeString(byte[] data) {
    return new String(encode0(data, encodeTable));
  }

  private char[] encode0(byte[] data, char[] hexChar) {
    char[] hex = new char[data.length * 2];
    for (int i = 0, j = 0; i < data.length; i++) {
      byte b = data[i];
      hex[j++] = hexChar[(int) ((b >> BITS_PER_ENCODED_BYTE) & MASK_4BITS)];
      hex[j++] = hexChar[(int) (b & MASK_4BITS)];
    }
    return hex;
  }

  /**
   * convert Base16 to byte array.
   *
   * @param str hex char array.
   * @return byte array.
   * @throws IllegalArgumentException data include invalid character.
   * @throws NullPointerException data is null or empty.
   */
  @Override
  public byte[] decode(String str) throws IllegalArgumentException, NullPointerException {
    if (StringUtil.isNullOrEmpty(str)) {
      throw new NullPointerException();
    }
    char[] data = str.toCharArray();
    byte[] hex = new byte[data.length / 2];
    for (int i = 0, j = 0; i < hex.length; i++) {
      int f = decodeTable[data[j++]] << BITS_PER_ENCODED_BYTE;
      f |= decodeTable[data[j++]];
      hex[i] = (byte) f;
    }
    return hex;
  }
}
