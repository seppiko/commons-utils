/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils;

import java.nio.ByteBuffer;

/**
 * Bytes utility
 *
 * @author Leonard Woo
 */
public class BytesUtil {

  private BytesUtil() {}

  /**
   * Convert long to byte array
   *
   * @param l long number.
   * @return byte array.
   */
  public static byte[] longToBytes(long l) {
    return ByteBuffer.allocate(8).putLong(l).array();
  }

  /**
   * Convert byte array to long
   *
   * @param bs byte array.
   * @return long number.
   */
  public static long bytesToLong(byte[] bs) {
    return getByteBuffer(bs).getLong();
  }

  /**
   * Convert int to byte array
   *
   * @param i int number.
   * @return byte array.
   */
  public static byte[] intToBytes(int i) {
    return ByteBuffer.allocate(4).putInt(i).array();
  }

  /**
   * Convert byte array to int
   *
   * @param bs byte array.
   * @return int number.
   */
  public static int bytesToInt(byte[] bs) {
    return getByteBuffer(bs).getInt();
  }

  /**
   * Convert short to byte array
   *
   * @param s short number.
   * @return byte array.
   */
  public static byte[] shortToBytes(short s) {
    return ByteBuffer.allocate(2).putShort(s).array();
  }

  /**
   * Convert byte array to short
   *
   * @param bs byte array.
   * @return short number.
   */
  public static short bytesToShort(byte[] bs) {
    return getByteBuffer(bs).getShort();
  }

  /**
   * Convert double to byte array
   *
   * @param d double number.
   * @return byte array.
   */
  public static byte[] doubleToBytes(double d) {
    return ByteBuffer.allocate(8).putDouble(d).array();
  }

  /**
   * Convert byte array to double
   *
   * @param bs byte array.
   * @return double number.
   */
  public static double bytesToDouble(byte[] bs) {
    return getByteBuffer(bs).getDouble();
  }

  /**
   * Convert float to byte array
   *
   * @param f float number.
   * @return byte array.
   */
  public static byte[] floatToBytes(float f) {
    return ByteBuffer.allocate(4).putFloat(f).array();
  }

  /**
   * Convert byte array to float
   *
   * @param bs byte array.
   * @return float number.
   */
  public static float bytesToFloat(byte[] bs) {
    return getByteBuffer(bs).getFloat();
  }

  private static ByteBuffer getByteBuffer(byte[] bytes) {
    ByteBuffer buffer = ByteBuffer.allocate(bytes.length);
    buffer.put(bytes);
    buffer.flip();
    return buffer;
  }

  /**
   * Returns a string representation of the byte argument as an unsigned byte in base 2.
   *
   * @param b a byte to be converted to a string.
   * @return the string representation of the unsigned byte value represented by the argument in
   *   binary (base 2).
   */
  public static String toBinaryString(byte b) {
    return String.format("%8s", Integer.toBinaryString(b & 0xFF))
        .replace(' ', '0');
  }
}
