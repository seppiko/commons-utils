/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.concurrent;

import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import org.seppiko.commons.utils.Assert;
import org.seppiko.commons.utils.StringUtil;

/**
 * Retry task
 *
 * @author Leonard Woo
 */
public class RetryableTask {

  private RetryableTask() {}

  /**
   * Retry task caller
   *
   * @param callable retry callable.
   * @param times retry times.
   * @param condition retry termination condition.
   *   If the result returned by the callable method meets this condition, the retry is terminated.
   * @param delay retry delay between execution calls.
   * @param unit retry delay time unit.
   * @return retry callable task computed result, if failure return {@code null}.
   * @param <T> task return type.
   * @throws NullPointerException if the retry callable or time unit is {@code null}.
   * @throws IllegalArgumentException if times less than one or delay is negative.
   * @throws RuntimeException
   *   callable may throw this, which will break the retrying task being executed.
   */
  public static <T> T caller(RetryCallable<T> callable, int times, Function<T, Boolean> condition,
      long delay, TimeUnit unit)
      throws NullPointerException, IllegalArgumentException, RuntimeException {
    if (callable == null) {
      throw new NullPointerException("retry callable must not be null.");
    }
    if (times <= 0) {
      throw new IllegalArgumentException("times must be at least 1.");
    }
    if (delay < 0) {
      throw new IllegalArgumentException("delay time must not be negative.");
    }
    if (unit == null) {
      throw new NullPointerException("time unit must not be null.");
    }

    T result;
    int i = 0;
    do {
      result = callable.call();

      // condition break
      if (Boolean.TRUE.equals(condition.apply(result))) {
        break;
      }

      // do not delay the last task
      if (i < (times - 1)) {
        try {
          unit.sleep(delay);
        } catch (InterruptedException ignored) {
          // ignored sleep exception
        }
      }

      i++;
    } while (i < times);

    return result;
  }

}
