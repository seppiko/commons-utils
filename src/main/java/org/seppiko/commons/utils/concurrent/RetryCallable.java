/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.concurrent;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * A tasks that return retryable results.
 * Implementors define a single method with no arguments called {@code call}.
 *
 * @param <V> the result type of method {@code call}
 * @author Leonard Woo
 */
@FunctionalInterface
public interface RetryCallable<V> {

  /**
   * Computes a result.
   *
   * @see RetryableTask#caller(RetryCallable, int, Function, long, TimeUnit)
   * @return computed result
   */
  V call();
}
