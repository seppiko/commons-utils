/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.jdbc;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Objects;

/**
 * Simply SQL executor
 *
 * @author Leonard Woo
 */
public abstract class SQLExecutor implements Closeable {

  private final Connection conn;

  /**
   * Initialize SQL executor constructor
   *
   * @param conn the connection.
   * @throws NullPointerException connection is {@code null}.
   */
  public SQLExecutor(Connection conn) throws NullPointerException {
    Objects.requireNonNull(conn, "Connection must not be null.");
    this.conn = conn;
  }

  /**
   * Query {@code SELECT} statement
   *
   * @param sql SQL Statement.
   * @return a {@code ResultSet} object that contains the data produced by the given query; never {@code null}
   * @throws SQLException if a database access error occurs.
   * @throws IllegalArgumentException if SQL is {@code null} or empty.
   */
  public ResultSet query(final String sql) throws SQLException, IllegalArgumentException {
    sqlValidation(sql);

    try (Statement stmt = conn.createStatement()) {
      return stmt.executeQuery(sql);
    }
  }

  /**
   * Query {@code SELECT} statement
   *
   * @param sql SQL Statement.
   * @param params parameters.
   * @return a {@code ResultSet} object that contains the data produced by the given query; never {@code null}
   * @throws SQLException if a database access error occurs.
   * @throws IllegalArgumentException if SQL is {@code null} or empty.
   */
  public ResultSet query(final String sql, Object... params) throws SQLException, IllegalArgumentException {
    sqlValidation(sql);

    try(PreparedStatement pstmt = conn.prepareStatement(sql)) {
      if (params.length == 1) {
        SqlTypeUtil.setParameter(pstmt, 1, params[0]);
      } else {
        for (int i = 0; i < params.length; i++) {
          SqlTypeUtil.setParameter(pstmt, i + 1, params[i]);
        }
      }
      return pstmt.executeQuery();
    }
  }

  /**
   * Execute {@code INSERT} {@code UPDATE} {@code DELETE} or other statement.
   *
   * @param sql SQL Statement.
   * @return SQL execute row count.
   * @throws SQLException if a database access error occurs.
   * @throws IllegalArgumentException if SQL is {@code null} or empty.
   */
  public int execute(final String sql) throws SQLException, IllegalArgumentException {
    sqlValidation(sql);

    try(Statement stmt = conn.createStatement()) {
      return stmt.executeUpdate(sql);
    }
  }

  /**
   * Execute {@code INSERT} {@code UPDATE} {@code DELETE} or other statement.
   *
   * @param sql SQL Statement.
   * @param params parameters.
   * @return SQL execute row count.
   * @throws SQLException if a database access error occurs.
   * @throws IllegalArgumentException if SQL is {@code null} or empty.
   */
  public int execute(final String sql, Object... params) throws SQLException, IllegalArgumentException {
    sqlValidation(sql);
    try(PreparedStatement pstmt = conn.prepareStatement(sql)) {
      if (params.length == 1) {
        SqlTypeUtil.setParameter(pstmt, 1, params[0]);
      } else {
        for (int i = 0; i < params.length; i++) {
          SqlTypeUtil.setParameter(pstmt, i + 1, params[i]);
        }
      }
      return pstmt.executeUpdate();
    }
  }

  /**
   * Execute {@code INSERT} {@code UPDATE} {@code DELETE} or other statement.
   * And return generated key.
   *
   * @param sql SQL Statement.
   * @return auto-generated key, if -1 is failed. if 0 is not get key.
   * @throws SQLException if a database access error occurs.
   * @throws IllegalArgumentException if SQL is {@code null} or empty.
   */
  public long executeWithGeneratedKey(final String sql) throws SQLException, IllegalArgumentException {
    sqlValidation(sql);

    Statement stmt = null;
    ResultSet rs = null;
    try {
      stmt = conn.createStatement();
      if (0 < stmt.executeUpdate(sql)) {
        rs = stmt.getGeneratedKeys();
        if (rs != null && rs.next()) {
          return rs.getLong(1);
        }
      }
      return -1L;
    } finally {
      if (rs != null && !rs.isClosed()) {
        rs.close();
      }
      if (stmt != null && !stmt.isClosed()) {
        stmt.close();
      }
    }
  }

  /**
   * Execute {@code INSERT} {@code UPDATE} {@code DELETE} or other statement.
   * And return generated key.
   *
   * @param sql SQL Statement.
   * @param params parameters.
   * @return auto-generated key, if -1 is failed, if 0 is not get key.
   * @throws SQLException if a database access error occurs.
   * @throws IllegalArgumentException if SQL is {@code null} or empty.
   */
  public long executeWithGeneratedKey(final String sql, Object... params) throws SQLException, IllegalArgumentException {
    sqlValidation(sql);

    PreparedStatement pstmt = null;
    ResultSet rs = null;
    try {
      pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      if (params.length == 1) {
        SqlTypeUtil.setParameter(pstmt, 1, params[0]);
      } else {
        for (int i = 0; i < params.length; i++) {
          SqlTypeUtil.setParameter(pstmt, i + 1, params[i]);
        }
      }
      if (pstmt.executeUpdate() > 0) {
        rs = pstmt.getGeneratedKeys();
        if (rs != null && rs.next()) {
          return rs.getLong(1);
        }
      }
      return -1L;
    } finally {
      if (rs != null && !rs.isClosed()) {
        rs.close();
      }
      if (pstmt != null && !pstmt.isClosed()) {
        pstmt.close();
      }
    }
  }

  /**
   * Close Connection
   *
   * @throws IOException if a database access error occurs.
   */
  @Override
  public void close() throws IOException {
    try {
      if (!conn.isClosed()) {
        conn.close();
      }
    } catch (SQLException ex) {
      throw new IOException(ex);
    }
  }

  /**
   * SQL validation
   *
   * @param sql SQL statement
   * @throws SQLException if there is something wrong with SQL.
   * @throws IllegalArgumentException if SQL is {@code null} or empty.
   */
  public abstract void sqlValidation(String sql) throws SQLException, IllegalArgumentException;
}
