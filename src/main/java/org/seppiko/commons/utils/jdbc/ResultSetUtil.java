/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.jdbc;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import org.seppiko.commons.utils.Assert;
import org.seppiko.commons.utils.ObjectUtil;
import org.seppiko.commons.utils.reflect.ClassUtil;
import org.seppiko.commons.utils.reflect.ReflectionUtil;

/**
 * JDBC ResultSet utility
 *
 * @see ResultSet
 * @author Leonard Woo
 */
public class ResultSetUtil {

  private ResultSetUtil() {}

  /**
   * Convert {@code ResultSet} to {@code List<Map<ColumnName(String), ColumnValue(Object)>>} .
   *
   * @param rs {@code ResultSet} instance. Without close.
   * @return List of Map, value type is sql type.
   * @throws SQLException if a database access error occurs or this method is called on a closed
   *     result set.
   */
  public static ArrayList<HashMap<String, Object>> convert(ResultSet rs)
      throws SQLException {
    Assert.notNull(rs, "ResultSet must not be null");
    ArrayList<HashMap<String, Object>> result = new ArrayList<>();
    ResultSetMetaData row = rs.getMetaData();
    int colSize = row.getColumnCount();
    while (rs.next()) {
      HashMap<String, Object> rowMap = new HashMap<>();
      for (int i = 1; i <= colSize; i++) {
        rowMap.put(row.getColumnName(i), rs.getObject(i));
      }
      result.add(rowMap);
    }
    return result;
  }

  /**
   * Simply ORM utility for convert {@code ResultSet} to entity class list.
   *
   * @param rs {@code ResultSet} object without close.
   * @param clazz entity class.
   * @return entity list.
   * @param <T> entity class type.
   * @throws SQLException if a database access error occurs or this method is called on a closed
   *   result set.
   * @throws IllegalAccessException target entity create failed.
   */
  public static <T> ArrayList<T> convert(ResultSet rs, Class<T> clazz)
      throws SQLException, IllegalAccessException {
    Assert.notNull(rs, "ResultSet must not be null");
    ArrayList<T> result = new ArrayList<>();

    ResultSetMetaData row = rs.getMetaData();
    int colSize = row.getColumnCount();

    while (rs.next()) {
      String[] colNames = new String[colSize];
      Object[] rowData = new Object[colSize];
      Class<?>[] rowTypes = new Class<?>[colSize];
      for (int i = 1; i <= colSize; i++) {
        colNames[i - 1] = row.getColumnName(i);
        var obj = rs.getObject(i);
        var type = SqlTypeUtil.parseType(row.getColumnType(i));
        if (Objects.nonNull(obj)) {
          rowData[i - 1] = obj;
          rowTypes[i - 1] = ObjectUtil.requireWithElse(type, obj.getClass());
        }
      }

      T t;
      try {
        t = ClassUtil.getInstance(clazz);

        Field[] fields;
        try {
          fields = ReflectionUtil.getDeclaredFields(clazz);
        } catch (SecurityException e) {
          try {
            fields = ReflectionUtil.getRecordFields(clazz);
          } catch (NoSuchFieldException | SecurityException ex) {
            throw new IllegalAccessException("Failed to get entity class field");
          }
        }

        for (int i = 0; i < fields.length; i++) {
          try {
            if (fields[i].getName().equals(colNames[i])) {
              ReflectionUtil.setField(fields[i], true, t, rowData[i]);
            }
          } catch (ArrayIndexOutOfBoundsException ignore) {
            break;
          }
        }
      } catch (NoSuchMethodException e) {
        try {
          t = ReflectionUtil.setRecordAllFields(clazz, true, rowTypes, rowData);
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException ex) {
          throw new RuntimeException(ex);
        }
      }

      result.add(t);
    }

    return result;
  }

  /**
   * Close ResultSet
   *
   * @param rs ResultSet instance.
   * @throws SQLException if a database access error occurs.
   */
  public static void close(ResultSet rs) throws SQLException {
    if (rs != null && !rs.isClosed()) {
      rs.close();
    }
  }
}
