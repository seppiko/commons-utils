/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.jdbc;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * SQL Type utility
 *
 * @author Leonard Woo
 */
public class SqlTypeUtil {

  private SqlTypeUtil() {}

  /**
   * Convert SQL {@link Date} and {@link Time} to {@link LocalDateTime}.
   *
   * @param date SQL date.
   * @param time SQL time.
   * @return {@link LocalDateTime} instance.
   */
  public static LocalDateTime convertSqlDateTime(Date date, Time time) {
    return date.toLocalDate().atTime(time.toLocalTime());
  }

  /**
   * Convert {@link java.util.Date} to SQL {@link Timestamp}
   *
   * @see org.seppiko.commons.utils.DatetimeUtil#toDate(LocalDateTime, ZoneId)
   * @param data {@link java.util.Date} instance.
   * @return SQL {@link Timestamp} instance.
   */
  public static Timestamp toSqlTimestamp(java.util.Date data) {
    return new Timestamp(data.getTime());
  }

  /**
   * Convert {@link Instant} to SQL {@link Timestamp}
   *
   * @param instant Instant instance.
   * @return SQL {@link Timestamp} instance.
   */
  public static Timestamp toSqlTimestamp(Instant instant) {
    return Timestamp.from(instant);
  }

  /**
   * set parameter object
   *
   * @param pstmt {@link PreparedStatement} instance.
   * @param i parameter index.
   * @param obj parameter object.
   * @throws SQLException if parameterIndex does not correspond to a parameter marker in the SQL
   *   statement; if a database access error occurs or this method is called on a closed
   *   {@code PreparedStatement}.
   * @see <a href="https://github.com/mybatis/mybatis-3/tree/master/src/main/java/org/apache/ibatis/type">
   *   Mybatis 3 typeHandlers</a>
   */
  protected static void setParameter(PreparedStatement pstmt, int i, Object obj)
      throws SQLException {
    if (obj instanceof java.util.Date) {
      if (obj instanceof java.sql.Date) {
        pstmt.setDate(i, (java.sql.Date) obj);
      } else if (obj instanceof java.sql.Time) {
        pstmt.setTime(i, (java.sql.Time) obj);
      } else {
        pstmt.setTimestamp(i, SqlTypeUtil.toSqlTimestamp((java.util.Date) obj));
      }
    } else if (obj instanceof java.time.temporal.TemporalAccessor){
      if(obj instanceof java.time.Instant) {
        pstmt.setTimestamp(i, SqlTypeUtil.toSqlTimestamp((Instant) obj));
      } else if (obj instanceof java.time.YearMonth) {
        pstmt.setString(i, ((java.time.YearMonth) obj).toString());
      } else if (obj instanceof java.time.Year) {
        pstmt.setInt(i, ((java.time.Year) obj).getValue());
      } else if (obj instanceof java.time.Month){
        pstmt.setInt(i, ((java.time.Month) obj).getValue());
      } else {
        pstmt.setObject(i, obj);
      }
    } else if (obj instanceof Number) {
      if (obj instanceof BigDecimal) {
        pstmt.setBigDecimal(i, (BigDecimal) obj);
      } else if (obj instanceof BigInteger) {
        pstmt.setBigDecimal(i, new BigDecimal((BigInteger) obj));
      } else if (obj instanceof Byte) {
        pstmt.setByte(i, (Byte) obj);
      } else if (obj instanceof Short) {
        pstmt.setShort(i, (Short) obj);
      } else if (obj instanceof Integer) {
        pstmt.setInt(i, (Integer) obj);
      } else if (obj instanceof Long) {
        pstmt.setLong(i, (Long) obj);
      } else if (obj instanceof Float) {
        pstmt.setFloat(i, (Float) obj);
      } else if (obj instanceof Double) {
        pstmt.setDouble(i, (Double) obj);
      } else {
        pstmt.setString(i, obj.toString());
      }
    } else if (obj instanceof String) {
      pstmt.setString(i, (String) obj);
    } else {
      pstmt.setObject(i, obj);
    }
  }

  /**
   * Parse JDBC type to Java type class
   *
   * @param sqlType JDBC type
   * @return type class
   * @throws SQLException unknown type.
   * @see <a href="https://mybatis.org/mybatis-3/configuration.html#typehandlers">typeHandlers</a>
   */
  public static Class<?> parseType(int sqlType) throws SQLException {
    return switch (sqlType) {
      case Types.BIT -> Boolean.class;
      case Types.BOOLEAN -> Boolean.class;
      case Types.TINYINT -> Byte.class;
      case Types.SMALLINT -> Short.class;
      case Types.INTEGER -> Integer.class;
      case Types.BIGINT -> Long.class;
      case Types.FLOAT -> Float.class;
      case Types.DOUBLE -> Double.class;
      case Types.NUMERIC -> BigDecimal.class;
      case Types.DECIMAL -> BigDecimal.class;
      case Types.CHAR -> String.class;
      case Types.VARCHAR -> String.class;
      case Types.LONGVARCHAR -> String.class;
      case Types.DATE -> LocalDate.class;
      case Types.TIME -> LocalTime.class;
      case Types.TIMESTAMP -> ZonedDateTime.class;
      case Types.BINARY -> byte[].class;
      case Types.VARBINARY -> byte[].class;
      case Types.LONGVARBINARY -> byte[].class;
      case Types.BLOB -> byte[].class;
//      case Types.NULL -> null;
      case Types.OTHER -> Object.class;
      case Types.JAVA_OBJECT -> Object.class;
      case Types.DISTINCT -> Object.class;
      case Types.STRUCT -> Object.class;
      case Types.ARRAY -> Object.class;
      case Types.REF -> Object.class;
      case Types.DATALINK -> Object.class;
      // JDBC 4.0
      case Types.ROWID -> Object.class;
      case Types.NCHAR -> String.class;
      case Types.NVARCHAR -> String.class;
      case Types.LONGNVARCHAR -> String.class;
      case Types.NCLOB -> String.class;
      case Types.SQLXML -> String.class;
      // JDBC 4.2
      case Types.REF_CURSOR -> Object.class;
      case Types.TIME_WITH_TIMEZONE -> ZonedDateTime.class;
      case Types.TIMESTAMP_WITH_TIMEZONE -> ZonedDateTime.class;
      default -> Object.class;
    };
  }

}
