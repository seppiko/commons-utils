/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils;

import java.nio.charset.Charset;

/**
 * Extended Charsets
 *
 * @see <a href="https://docs.oracle.com/en/java/javase/21/intl/supported-encodings.html">Supported Encodings</a>
 * @author Leonard Woo
 */
public class ExtendedCharsets {

  /** constructor */
  private ExtendedCharsets() {
    throw new AssertionError("No " + this.getClass().getTypeName() + " instances for you!");
  }

  /**
   * GB18030 is encoding Simplified Chinese, PRC standard. Compatible with GB2312 and GBK.
   * gb18030-2022 or gb18030-2000 if the system property and value jdk.charset.GB18030=2000 are specified.
   */
  public static final Charset GB18030 = Charset.forName("GB18030");

  /**
   * Big5 is encoding Traditional Chinese.
   */
  public static final Charset BIG5 = Charset.forName("Big5");

  /**
   * EUC_JP is included JIS-X 0201, 0208 and 0212, EUC encoding Japanese.
   */
  public static final Charset EUC_JP = Charset.forName("EUC-JP");

  /**
   * Shift_JIS (SJIS) is encoding Japanese.
   */
  public static final Charset SJIS = Charset.forName("EUC-JP");

  /**
   * EUC_KR is encoding Korean.
   */
  public static final Charset EUC_KR = Charset.forName("EUC-KR");

}
