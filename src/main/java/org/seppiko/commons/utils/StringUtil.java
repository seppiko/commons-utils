/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils;

import java.math.RoundingMode;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

/**
 * String utility
 *
 * @author Leonard Woo
 */
public class StringUtil {

  private StringUtil() {}

  /**
   * Require a CharSequence with default value.
   *
   * @param value CharSequence instance.
   * @return CharSequence without null or empty.
   * @throws NullPointerException If value is null, throw this.
   */
  public static CharSequence requireNonBlank(CharSequence value) throws NullPointerException {
    if (isNullOrEmpty(value)) {
      throw new NullPointerException();
    }
    return value;
  }

  /**
   * Require a CharSequence with default value.
   *
   * @param value CharSequence instance.
   * @param message if value is null or empty throw {@code NullPointerException} with this message
   * @return CharSequence without null or empty.
   * @throws NullPointerException If value is null, throw this.
   */
  public static CharSequence requireNonBlank(CharSequence value, String message)
      throws NullPointerException {
    if (isNullOrEmpty(value)) {
      throw new NullPointerException(message);
    }
    return value;
  }

  /**
   * Require a CharSequence with default value.
   *
   * @param value CharSequence instance.
   * @param defaultValue default CharSequence instance.
   * @return CharSequence without null or empty.
   */
  public static CharSequence requireNonBlankElse(CharSequence value, CharSequence defaultValue) {
    return isNullOrEmpty(value)? defaultValue: value;
  }

  /**
   * Test CharSequence is null or empty.
   *
   * @param value CharSequence instance.
   * @return true is this CharSequence is null or empty.
   */
  public static boolean isNullOrEmpty(CharSequence value) {
    return Objects.isNull(value) || value.isEmpty();
  }

  /**
   * Check that the given {@code CharSequence} is neither {@code null} nor
   * of length 0.
   *
   * <p>Note: this method returns {@code true} for a {@code CharSequence}
   * that purely consists of whitespace.
   * <pre><code>
   * StringUtil.hasLength(null) = false
   * StringUtil.hasLength("") = false
   * StringUtil.hasLength(" ") = true
   * StringUtil.hasLength("Hello") = true
   * </code></pre>
   *
   * @param input the {@code CharSequence} to check (maybe {@code null})
   * @return {@code true} if the {@code CharSequence} is not {@code null} and has length
   * @see #hasText(CharSequence)
   */
  public static boolean hasLength(CharSequence input) {
    return Objects.nonNull(input) && !input.isEmpty();
  }

  /**
   * Check whether the given {@code CharSequence} contains actual <em>text</em>.
   *
   * <p>More specifically, this method returns {@code true} if the
   * {@code CharSequence} is not {@code null}, its length is greater than
   * 0, and it contains at least one non-whitespace character.
   * <pre><code>
   * StringUtil.hasText(null) = false
   * StringUtil.hasText("") = false
   * StringUtil.hasText(" ") = false
   * StringUtil.hasText("12345") = true
   * StringUtil.hasText(" 12345 ") = true
   * </code></pre>
   *
   * @param input the {@code CharSequence} to check (maybe {@code null})
   * @return {@code true} if the {@code CharSequence} is not {@code null},
   * its length is greater than 0, and it does not contain whitespace only
   * @see #hasLength(CharSequence)
   * @see Character#isWhitespace
   */
  public static boolean hasText(CharSequence input) {
    if (Objects.isNull(input)) {
      return false;
    }

    return hasLength(input) && containsText(input);
  }

  /**
   * Check whether the given {@code CharSequence} contains any whitespace or ISO control characters.
   * 
   * @param input the {@code CharSequence} to check (maybe {@code null})
   * @return {@code true} if the {@code CharSequence} is not empty and contains
   *   at least 1 whitespace or ISO control character
   * @see Character#isWhitespace
   * @see Character#isISOControl
   */
  public static boolean containsText(CharSequence input) {
    if (!hasLength(input)) {
      return false;
    }

    for (int i = 0; i < input.length(); i++) {
      char ch = input.charAt(i);
      if (!Character.isWhitespace(ch) || !Character.isISOControl(ch)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Test CharSequence is uppercase
   *
   * @param input uppercase CharSequence.
   * @return true is uppercase.
   */
  public static boolean isUppercase(CharSequence input) {
    return RegexUtil.matches("^[\\p{Upper}Ａ-Ｚ]+$", input);
  }

  /**
   * Test CharSequence is lowercase
   *
   * @param input lowercase CharSequence.
   * @return true is lowercase.
   */
  public static boolean isLowercase(CharSequence input) {
    return RegexUtil.matches("^[\\p{Lower}ａ-ｚ]+$", input);
  }

  /**
   * Test CharSequence is numeric
   *
   * @param input numeric CharSequence.
   * @return true is numeric.
   */
  public static boolean isNumeric(CharSequence input) {
    return RegexUtil.matches("^[-－+＋]?[\\d０-９]+([\\.．]{1}[\\d０-９]+)?$", input);
  }

  /**
   * Test CharSequence is integer
   *
   * @param input integer CharSequence.
   * @return true is integer.
   */
  public static boolean isInteger(CharSequence input) {
    return RegexUtil.matches("^[-－+＋]?[\\d０-９]+$", input);
  }

  /**
   * Test CharSequence is decimal
   *
   * @param input decimal CharSequence.
   * @return true is decimal.
   */
  public static boolean isDecimal(CharSequence input) {
    return RegexUtil.matches("^[-－+＋]?[\\d０-９]+[\\.．]{1}[\\d０-９]+$", input);
  }

  /**
   * Test CharSequence is punctuation
   *
   * @param input punctuation CharSequence.
   * @return true is punctuation.
   */
  public static boolean isPunctuation(CharSequence input) {
    return RegexUtil.matches("^[\\p{Punct}！＂＃＄％＆＇（）＊＋，－．／：；＜＝＞？＠［＼］＾＿｀｛｜｝]+$", input);
  }

  /**
   * Test CharSequence is alphabet or numeric
   *
   * @param input alphabet or numeric CharSequence.
   * @return true is alphabet or numeric.
   */
  public static boolean isAlphaNum(CharSequence input) {
    return RegexUtil.matches("^[\\p{Alnum}０-９Ａ-Ｚａ-ｚ]+$", input);
  }

  /**
   * Convert string data from old encoding to new encoding
   *
   * @param data CharSequence data.
   * @param oldEncoding old encoding.
   * @param newEncoding new encoding.
   * @return new encoding CharSequence.
   * @throws NullPointerException old encode or new decode exception.
   */
  public static String transcoding(CharSequence data, Charset oldEncoding, Charset newEncoding)
      throws NullPointerException {
    byte[] oldData = CharUtil.charsetEncode(oldEncoding, CharBuffer.wrap(data));
    if (oldData == Environment.EMPTY_BYTE_ARRAY) {
      throw new NullPointerException("old encode exception");
    }
    CharBuffer cb = CharUtil.charsetDecode(newEncoding, oldData);
    if (cb.capacity() == 0) {
      throw new NullPointerException("new decode exception");
    }
    return cb.toString();
  }

  /**
   * Return fixed length string
   *
   * @param str string object.
   * @param length count length.
   * @param preChar pre-padded character.
   * @param cutPre {@code true} if string length greater than the length, cut string to length from 0.
   * @return fixed length string object.
   */
  public static String fixedLengthWithPreChar(String str, int length, char preChar, boolean cutPre) {
    if (str.length() < length) {
      return String.valueOf(preChar).repeat(length - str.length()) +
          str;
    } else if ((str.length() > length) && cutPre) {
      return str.substring(str.length() - length);
    }
    return str;
  }

  /**
   * Return fixed length string
   *
   * @param str string.
   * @param length count length.
   * @param fillChar fill character.
   * @param cutFill {@code true} if string length greater than the length, cut string to length from 0.
   * @return fixed length string.
   */
  public static String fixedLengthWithFillChar(String str, int length, char fillChar, boolean cutFill) {
    if (str.length() < length) {
      return str + String.valueOf(fillChar).repeat(length - str.length());
    } else if ((str.length() > length) && cutFill) {
      return str.substring(str.length() - length);
    }
    return str;
  }

  /**
   * Cut slices str around the first instance of sep, returning the text before and after sep. The
   * found result reports whether sep appears in str. If sep does not appear in str, cut returns
   * [str, ""].
   *
   * @param str string.
   * @param sep separator.
   * @return string array {@code [before, after]}.
   */
  public static String[] cuts(String str, String sep) {
    int i = str.indexOf(sep);
    if (i > 0) {
      return new String[]{str.substring(0, i), str.substring(i + sep.length())};
    }
    return new String[]{str, ""};
  }

  /**
   * Capitalize the first letter
   *
   * @param input origin string.
   * @return new string.
   */
  public static String toFirstUpperCase(String input) {
    String s = String.valueOf(Character.toUpperCase(input.charAt(0)));
    if (input.length() > 1) {
      return s + input.subSequence(1, input.length());
    }
    return s;
  }

  /**
   * Replace CharSequence between start and end
   *
   * @param data origin data.
   * @param start replace start index.
   * @param end replace end index.
   * @param replacement replace data.
   * @return new string.
   */
  public static String replaceBetween(
      CharSequence data, int start, int end, String replacement) {
    String input = data.toString();
    return input.substring(0, start) +
        replacement +
        input.substring(end);
  }

  /**
   * Convert CharSequence to String with separate
   *
   * @param input Raw data.
   * @param splitNum Separation interval.
   * @param split Separator.
   * @return encoded string.
   */
  public static String convertToString(CharSequence input, int splitNum, String split) {
    String[] result = splitToArray(input.toString(), splitNum);
    return String.join(split, result);
  }

  /**
   * Split to String array with split length.
   * <p>If the result of dividing xx by yy is not an integer,
   * the total length is equal to the result plus one.
   *
   * @param input input data.
   * @param splitLen split length.
   * @return String array.
   */
  public static String[] splitToArray(String input, int splitLen) {
    Assert.notNull(input, "input must not be null.");
    Assert.isTrue((splitLen > 0), "split length must not be zero or negative.");
    int len = MathUtil.divide(input.length(), splitLen, RoundingMode.HALF_UP).intValue();
    String[] array = new String[len];
    for (int i = 0, j = 0; i < array.length; i++, j += splitLen) {
      if ((j + splitLen) <= input.length()) {
        array[i] = input.substring(j, j + splitLen);
      } else {
        array[i] = input.substring(j);
      }
    }
    return array;
  }

  /**
   * Delete string separator and to char array.
   *
   * @param src string.
   * @param separator separator.
   * @return char array.
   * @throws NullPointerException when data or separator is null.
   */
  public static char[] convertToCharArray(String src, String separator) throws NullPointerException {
    Objects.requireNonNull(src, "src must not be null");
    Objects.requireNonNull(separator, "separator must not be null");

    Matcher m = Objects.requireNonNull(RegexUtil.getMatcher(separator, src),
        "split matcher failed");
    if (m.find()) {
      return m.replaceAll("").toCharArray();
    }
    return src.toCharArray();
  }

  /** between {@code '\uFF01'} and {\u0021} number */
  protected static final Integer BETWEEN =
      (CharUtil.FULLWIDTH_EXCLAMATION_MARK - CharUtil.EXCLAMATION_MARK);

  /**
   * Convert half-width string ({@code '\u005Cu0021'} through {@code '\u005Cu007E'}) to
   * full-width string ({@code '\u005CuFF01'} through {@code '\u005CuFF5E'})
   *
   * @param src Half-width string.
   * @return Full-width string.
   */
  public static String toFullWidth(CharSequence src) {
    StringBuilder sb = new StringBuilder(src.length());
    for (int i = 0; i < src.length(); i++) {
      char c = src.charAt(i);
      if (NumberUtil.between(c, CharUtil.EXCLAMATION_MARK, CharUtil.TILDE)) {
        sb.append((char) (c + BETWEEN));
      } else {
        sb.append(c);
      }
    }
    return sb.toString();
  }

  /**
   * Convert full-width string ({@code '\u005CuFF01'} through {@code '\u005CuFF5E'}) to
   * half-width string ({@code '\u005Cu0021'} through {@code '\u005Cu007E'})
   *
   * @param src Full-width string.
   * @return Half-width string.
   */
  public static String toHalfWidth(CharSequence src) {
    StringBuilder sb = new StringBuilder(src.length());
    for (int i = 0; i < src.length(); i++) {
      char c = src.charAt(i);
      if (NumberUtil.between(c, CharUtil.FULLWIDTH_EXCLAMATION_MARK, CharUtil.FULLWIDTH_TILDE)) {
        sb.append((char) (c - BETWEEN));
      } else {
        sb.append(c);
      }
    }
    return sb.toString();
  }

  /**
   * Unicode decoding.
   * When not found return origin String
   *
   * @param src unicode string like {@code '\\uXXXX'}.
   * @return String. If src is null or empty return empty string.
   */
  public static String unicodeDecode(String src) {
    if (isNullOrEmpty(src)) {
      return "";
    }
    Matcher m = RegexUtil.getMatcher("(\\\\u(\\p{XDigit}{4}))", src);
    while (m.find()) {
      String group = m.group(2);
      char ch = (char) Integer.parseInt(group, 16);
      String group1 = m.group(1);
      src = src.replace(group1, String.valueOf(ch));
    }
    return src;
  }

  /**
   * Unicode encoding
   *
   * @param src String
   * @return unicode string like {@code '\\uXXXX'}. If src is null or empty return empty string.
   */
  public static String unicodeEncode(String src) {
    if (isNullOrEmpty(src)) {
      return "";
    }
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < src.length(); i++) {
      char ch = src.charAt(i);
      if (NumberUtil.between(ch, CharUtil.SPACE, CharUtil.TILDE)) {
        sb.append(ch);
      } else {
        sb.append("\\u");
        String hex = Integer.toHexString(ch & 0xFFFF).toUpperCase();
        sb.append(fixedLengthWithPreChar(hex, 4, CharUtil.DIGIT_ZERO, true));
      }
    }
    return sb.toString();
  }
}
