/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils;

import java.util.Collection;
import java.util.Map;

/**
 * Assertion utility class that assists in validating arguments.
 *
 * @author Leonard Woo
 */
public class Assert {

  private Assert() {}

  /**
   * Assert a boolean expression, throwing an {@code IllegalArgumentException}
   * if the expression evaluates to {@code false}.
   *
   * <pre class="code">Assert.isTrue(i &gt; 0, "The value must be greater than zero");</pre>
   *
   * @param expression a boolean expression
   * @param message the exception message to use if the assertion fails
   * @throws IllegalArgumentException if {@code expression} is {@code false}
   */
  public static void isTrue(boolean expression, String message) {
    if (!expression) {
      throw new IllegalArgumentException(message);
    }
  }

  /**
   * Assert that an object is {@code null}.
   *
   * <pre class="code">Assert.isNull(value, "The value must be null");</pre>
   *
   * @param object the object to check
   * @param message the exception message to use if the assertion fails
   * @throws IllegalArgumentException if the object is not {@code null}
   */
  public static void isNull(Object object, String message) {
    if (null != object) {
      throw new IllegalArgumentException(message);
    }
  }

  /**
   * Assert that an object is not {@code null}.
   *
   * <pre class="code">Assert.notNull(clazz, "The class must not be null");</pre>
   *
   * @param object the object to check
   * @param message the exception message to use if the assertion fails
   * @throws IllegalArgumentException if the object is {@code null}
   */
  public static void notNull(Object object, String message) {
    if (null == object) {
      throw new IllegalArgumentException(message);
    }
  }

  /**
   * Assert that an array contains elements; that is, it must not be {@code null} and must contain
   * at least one element.
   *
   * <pre class="code">Assert.notEmpty(array, "The array must contain elements");</pre>
   *
   * @param array the array to check
   * @param message the exception message to use if the assertion fails
   * @throws IllegalArgumentException if the object array is {@code null} or contains no elements
   */
  public static void notEmpty(Object[] array, String message) {
    if (ArrayUtil.isEmpty(array)) {
      throw new IllegalArgumentException(message);
    }
  }

  /**
   * Assert that a collection contains elements; that is, it must not be {@code null} and must
   * contain at least one element.
   *
   * <pre class="code">Assert.notEmpty(collection, "Collection must contain elements");</pre>
   *
   * @param collection the collection to check
   * @param message the exception message to use if the assertion fails
   * @throws IllegalArgumentException if the collection is {@code null} or
   * contains no elements
   */
  public static void notEmpty(Collection<?> collection, String message) {
    if (CollectionUtil.isEmpty(collection)) {
      throw new IllegalArgumentException(message);
    }
  }

  /**
   * Assert that a Map contains entries; that is, it must not be {@code null} and must contain at
   * least one entry.
   *
   * <pre class="code">Assert.notEmpty(map, "Map must contain entries");</pre>
   *
   * @param map the map to check
   * @param message the exception message to use if the assertion fails
   * @throws IllegalArgumentException if the map is {@code null} or contains no entries
   */
  public static void notEmpty(Map<?, ?> map, String message) {
    if (CollectionUtil.isEmpty(map)) {
      throw new IllegalArgumentException(message);
    }
  }

  /**
   * Assert that a string is {@code null} or blank.
   *
   * <pre class="code">Assert.notBlank(str, "String must not blank");</pre>
   *
   * @param str the string to check
   * @param message the exception message to use if the assertion fails
   * @throws IllegalArgumentException if the string is {@code null} or contains no entries
   */
  public static void notBlank(String str, String message) {
    if (StringUtil.isNullOrEmpty(str) || str.isBlank()) {
      throw new IllegalArgumentException(message);
    }
  }

  /**
   * Assert that expected and actual are equal.
   *
   * @param expected expected value.
   * @param actual the value to check against {@code expected}.
   */
  public static void equals(Object expected, Object actual) {
    equals(expected, actual, null);
  }

  /**
   * Assert that expected and actual are equal.
   *
   * @param expected expected value.
   * @param actual the value to check against {@code expected}.
   * @param message the identifying message for the {@link AssertionError} ({@code null} okay).
   */
  public static void equals(Object expected, Object actual, String message) {
    if (!expected.equals(actual)) {
      throwAssertionError(message, null);
    }
  }

  /**
   * Constructs an {@link AssertionError} with a message and a cause but without expected/actual
   * values.
   *
   * @param message the detail message; null or blank will be converted to the empty String.
   * @param cause the cause of the failure.
   * @throws AssertionError always.
   */
  static void throwAssertionError(String message, Throwable cause) {
    throw new AssertionError(message, cause);
  }

  /**
   * Assert that the given String is not empty; that is, it must not be {@code null} and not the
   * empty String.
   *
   * <pre class="code">Assert.hasLength(name, "Name must not be empty");</pre>
   *
   * @param text the String to check
   * @param message the exception message to use if the assertion fails
   * @throws IllegalArgumentException if the text is empty
   * @see StringUtil#hasLength
   */
  public static void hasLength(String text, String message) {
    if (!StringUtil.hasLength(text)) {
      throw new IllegalArgumentException(message);
    }
  }

  /**
   * Assert that the given String contains valid text content; that is, it must not be {@code null}
   * and must contain at least one non-whitespace character.
   *
   * <pre class="code">Assert.hasText(name, "'name' must not be empty");</pre>
   *
   * @param text the String to check
   * @param message the exception message to use if the assertion fails
   * @throws IllegalArgumentException if the text does not contain valid text content
   * @see StringUtil#hasText
   */
  public static void hasText(String text, String message) {
    if (!StringUtil.hasText(text)) {
      throw new NullPointerException(message);
    }
  }

  /**
   * Throw Exception to RuntimeException.
   *
   * <pre><code>
   *  try {
   *    throw new Exception("test");
   *  } catch (Throwable t) {
   *    throw Assert.sneakyThrow(t);
   *  }
   * </code></pre>
   *
   * @param t any exception.
   * @return {@link RuntimeException}.
   * @throws NullPointerException if t is {@code null} throw this.
   */
  public static RuntimeException sneakyThrow(Throwable t) {
    if (t == null) {
      throw new NullPointerException();
    }
    return sneakyThrow0(t);
  }

  @SuppressWarnings("unchecked")
  private static <T extends Throwable> T sneakyThrow0(Throwable t) throws T {
    throw (T) t;
  }
}
