/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils;

import java.io.Serial;
import java.io.Serializable;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.MonthDay;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.SignStyle;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Datetime format and parse utility
 *
 * @see java.time.LocalDateTime
 * @see java.time.LocalDate
 * @see java.time.LocalTime
 * @see java.time.OffsetDateTime
 * @see java.time.OffsetTime
 * @see java.time.ZonedDateTime
 * @see java.time.ZoneOffset
 * @see java.time.Instant
 * @author Leonard Woo
 */
public class DatetimeUtil implements Serializable {

  @Serial
  private static final long serialVersionUID = -3239240029308812973L;

  private DatetimeUtil() {}

  /** System timezone zone */
  public static final ZoneId SYSTEM_ZONE = ZoneId.systemDefault();
  /** UTC zone */
  public static final ZoneId UTC_ZONE = Clock.systemUTC().getZone();
  /** Default locale */
  public static final Locale DEFAULT_LOCALE = Locale.getDefault(Locale.Category.FORMAT);
  /**
   * The database time formatter that formats or parses a time without an offset, such as
   * '2021-07-16 21:08:45'.
   */
  public static final DateTimeFormatter DATABASE_DATE_TIME;
  /**
   * The RFC 3339 time formatter that formats or parses a time with an offset, such as
   * '2021-09-24T23:55:00.1234Z+09:00'.
   */
  public static final DateTimeFormatter RFC_3339_OFFSET_DATE_TIME;
  /**
   * The ISO date formatter that formats or parses a date without an offset, such as '20111203'.
   * @see DateTimeFormatter#BASIC_ISO_DATE
   */
  public static final DateTimeFormatter BASIC_ISO_DATE;
  /** The basic time formatter that formats or parses a time without an offset, such as '225803'. */
  public static final DateTimeFormatter BASIC_ISO_TIME;
  /**
   * The basic time formatter that formats or parses a time with an offset, such as '225803+09:00'
   * or '135803Z'.
   */
  public static final DateTimeFormatter BASIC_ISO_OFFSET_TIME;

  static {
    DATABASE_DATE_TIME =
        new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .appendValue(ChronoField.YEAR, 4, 10, SignStyle.EXCEEDS_PAD)
            .appendLiteral('-')
            .appendValue(ChronoField.MONTH_OF_YEAR, 2)
            .appendLiteral('-')
            .appendValue(ChronoField.DAY_OF_MONTH, 2)
            .appendLiteral(' ')
            .appendValue(ChronoField.HOUR_OF_DAY, 2)
            .appendLiteral(':')
            .appendValue(ChronoField.MINUTE_OF_HOUR, 2)
            .appendLiteral(':')
            .appendValue(ChronoField.SECOND_OF_MINUTE, 2)
            .toFormatter();

    RFC_3339_OFFSET_DATE_TIME =
        new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .appendValue(ChronoField.YEAR, 4, 10, SignStyle.EXCEEDS_PAD)
            .appendLiteral('-')
            .appendValue(ChronoField.MONTH_OF_YEAR, 2)
            .appendLiteral('-')
            .appendValue(ChronoField.DAY_OF_MONTH, 2)
            .appendLiteral('T')
            .appendValue(ChronoField.HOUR_OF_DAY, 2)
            .appendLiteral(':')
            .appendValue(ChronoField.MINUTE_OF_HOUR, 2)
            .appendLiteral(':')
            .appendValue(ChronoField.SECOND_OF_MINUTE, 2)
            .appendFraction(ChronoField.NANO_OF_SECOND, 0, 4, true)
            .appendLiteral('Z')
            .parseLenient()
            .appendOffset("+HH:MM", "")
            .parseStrict()
            .toFormatter();

    BASIC_ISO_DATE = DateTimeFormatter.BASIC_ISO_DATE;

    BASIC_ISO_TIME =
        new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .appendValue(ChronoField.HOUR_OF_DAY, 2)
            .appendValue(ChronoField.MINUTE_OF_HOUR, 2)
            .appendValue(ChronoField.SECOND_OF_MINUTE, 2)
            .toFormatter();

    BASIC_ISO_OFFSET_TIME =
        new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .appendValue(ChronoField.HOUR_OF_DAY, 2)
            .appendValue(ChronoField.MINUTE_OF_HOUR, 2)
            .appendValue(ChronoField.SECOND_OF_MINUTE, 2)
            .parseLenient()
            .appendOffset("+HH:MM", "Z")
            .parseStrict()
            .toFormatter();
  }

  /**
   * Datetime format
   *
   * @param pattern see {@link DateTimeFormatter}
   * @param temporal java.time datetime class
   * @return Datetime formatted string
   */
  public static String format(String pattern, TemporalAccessor temporal) {
    return format(pattern, DEFAULT_LOCALE, temporal);
  }

  /**
   * Datetime format
   *
   * @param pattern see {@link DateTimeFormatter}
   * @param locale datetime local
   * @param temporal java.time datetime class
   * @return Datetime formatted string
   */
  public static String format(String pattern, Locale locale, TemporalAccessor temporal) {
    return DateTimeFormatter.ofPattern(pattern, locale).format(temporal);
  }

  /**
   * Datetime parser
   *
   * @param pattern datetime layout {@link DateTimeFormatter}
   * @param timestamp timestamp
   * @return DateTime interface
   */
  public static ZonedDateTime parse(String pattern, String timestamp) {
    return parse(pattern, DEFAULT_LOCALE, timestamp);
  }

  /**
   * Datetime parser
   *
   * @param pattern datetime layout {@link DateTimeFormatter}
   * @param locale timestamp local
   * @param timestamp timestamp
   * @return DateTime interface
   */
  public static ZonedDateTime parse(String pattern, Locale locale, String timestamp) {
    return ZonedDateTime.parse(timestamp, DateTimeFormatter.ofPattern(pattern, locale));
  }

  /**
   * Datetime to epoch second
   *
   * @param temporal datetime
   * @return epoch second
   */
  public static long toEpochSecond(TemporalAccessor temporal) {
    return Instant.from(temporal).getEpochSecond();
  }

  /**
   * Datetime to epoch millisecond
   *
   * @param temporal datetime
   * @return epoch millisecond
   */
  public static long toEpochMilliSecond(TemporalAccessor temporal) {
    return Instant.from(temporal).toEpochMilli();
  }

  /**
   * Epoch second parser
   *
   * @param epoch epoch second
   * @param zoneId time zone
   * @return Zoned DateTime
   */
  public static ZonedDateTime parseEpochSecond(long epoch, ZoneId zoneId) {
    return Instant.ofEpochSecond(epoch).atZone(zoneId);
  }

  /**
   * Epoch millisecond parser
   *
   * @param milli epoch millisecond
   * @param zoneId time zone
   * @return Zoned DateTime
   */
  public static ZonedDateTime parseEpochMilliSecond(long milli, ZoneId zoneId) {
    return Instant.ofEpochMilli(milli).atZone(zoneId);
  }

  /**
   * Get UTC now epoch millisecond
   *
   * @return epoch millisecond
   */
  public static long now() {
    return Instant.now().toEpochMilli();
  }

  /**
   * Get now epoch millisecond
   *
   * @param zoneId zone id
   * @return epoch millisecond
   */
  public static long now(ZoneId zoneId) {
    return Instant.now(Clock.system(zoneId)).toEpochMilli();
  }

  /**
   * Get today
   *
   * @return today
   */
  public static LocalDate today() {
    return LocalDate.now();
  }

  /**
   * Get today with zone
   *
   * @param zoneId zone id
   * @return today
   */
  public static LocalDate today(ZoneId zoneId) {
    return LocalDate.now(zoneId);
  }

  /**
   * Get LocalDate year and month
   *
   * @param date local date
   * @return year and month
   */
  public static YearMonth getYearMonth(LocalDate date) {
    return YearMonth.from(date);
  }

  /**
   * Get LocalDate month and day
   *
   * @param date local date
   * @return month and day of month
   */
  public static MonthDay getMonthDay(LocalDate date) {
    return MonthDay.from(date);
  }

  /**
   * Convert to {@link Date}
   *
   * @param datetime local datetime
   * @param zoneId local datetime zone e.g. {@link ZoneId#systemDefault()}
   * @return date instance
   */
  public static Date toDate(LocalDateTime datetime, ZoneId zoneId) {
    return Date.from(datetime.atZone(zoneId).toInstant());
  }

  /**
   * Convert to {@link Date}
   *
   * @param datetime zone datetime
   * @return date instance
   */
  public static Date toDate(ZonedDateTime datetime) {
    return Date.from(datetime.toInstant());
  }

  /**
   * Convert from {@link Date}
   *
   * @param datetime date object
   * @param zoneId date zone e.g. {@link ZoneId#systemDefault()}
   * @return local datetime instance
   */
  public static LocalDateTime fromDate(Date datetime, ZoneId zoneId) {
    return LocalDateTime.ofInstant(datetime.toInstant(), zoneId);
  }

  /**
   * Convert to {@link Calendar}
   *
   * @param dateTime local datetime
   * @param zoneId local datetime zone e.g. {@link ZoneId#systemDefault()}
   * @return calender object
   */
  public static Calendar toCalendar(LocalDateTime dateTime, ZoneId zoneId) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeZone(TimeZone.getTimeZone(zoneId));
    calendar.setTimeInMillis(Instant.from(dateTime).toEpochMilli());
    return calendar;
  }

  /**
   * Convert from {@link Calendar}
   *
   * @param calendar calender object
   * @param zoneId date zone e.g. {@link ZoneId#systemDefault()}
   * @return local datetime instance
   */
  public static LocalDateTime fromCalendar(Calendar calendar, ZoneId zoneId) {
    return LocalDateTime.ofInstant(calendar.toInstant(), zoneId);
  }

  /**
   * Return now is between start and end.
   *
   * @param now current datetime
   * @param start start datetime
   * @param end end datetime
   * @return {@code true} if the specified datetime is within the range.
   */
  public static boolean between(ZonedDateTime now, ZonedDateTime start, ZonedDateTime end) {
    return now.isAfter(start) && now.isBefore(end);
  }
}
