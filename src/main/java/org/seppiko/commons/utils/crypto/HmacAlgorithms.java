/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.crypto;

/**
 * Standard {@link HashUtil} algorithm names from the <cite>Java Cryptography Architecture Standard
 * Algorithm Name Documentation</cite>.
 *
 * @see <a
 *     href="https://docs.oracle.com/en/java/javase/21/docs/specs/security/standard-names.html#messagedigest-algorithms">
 *     Java Cryptography Architecture Standard Algorithm Name Documentation</a>
 * @see MessageDigestAlgorithms
 * @author Leonard Woo
 */
public enum HmacAlgorithms {

  /**
   * The HmacMD5 Message Authentication Code (MAC) algorithm specified in RFC 2104 and RFC 1321.
   * This Algorithm is unsafe.
   */
  @Deprecated
  HMAC_MD5("HmacMD5"),

  /**
   * The HmacSHA1 Message Authentication Code (MAC) algorithm specified in RFC 2104 and FIPS PUB
   * 180-2.
   * This Algorithm is unsafe.
   */
  @Deprecated
  HMAC_SHA_1("HmacSHA1"),

  /**
   * The HmacSHA224 Message Authentication Code (MAC) algorithm specified in RFC 2104 and FIPS PUB
   * 180-2.
   */
  HMAC_SHA_224("HmacSHA224"),

  /**
   * The HmacSHA256 Message Authentication Code (MAC) algorithm specified in RFC 2104 and FIPS PUB
   * 180-2.
   */
  HMAC_SHA_256("HmacSHA256"),

  /**
   * The HmacSHA384 Message Authentication Code (MAC) algorithm specified in RFC 2104 and FIPS PUB
   * 180-2.
   */
  HMAC_SHA_384("HmacSHA384"),

  /**
   * The HmacSHA512 Message Authentication Code (MAC) algorithm specified in RFC 2104 and FIPS PUB
   * 180-2.
   */
  HMAC_SHA_512("HmacSHA512"),

  /** The HmacSHA512/224 Message Authentication Code (MAC) algorithm specified in RFC 2104. */
  HMAC_SHA_512_224("HmacSHA512/224"),

  /** The HmacSHA512/256 Message Authentication Code (MAC) algorithm specified in RFC 2104. */
  HMAC_SHA_512_256("HmacSHA512/256"),

  /** The HmacSHA3-224 Message Authentication Code (MAC) algorithm specified in RFC 2104. */
  HMAC_SHA3_224("HmacSHA3-224"),

  /** The HmacSHA3-256 Message Authentication Code (MAC) algorithm specified in RFC 2104. */
  HMAC_SHA3_256("HmacSHA3-256"),

  /** The HmacSHA3-384 Message Authentication Code (MAC) algorithm specified in RFC 2104. */
  HMAC_SHA3_384("HmacSHA3-384"),

  /** The HmacSHA3-512 Message Authentication Code (MAC) algorithm specified in RFC 2104. */
  HMAC_SHA3_512("HmacSHA3-512"),

  // Add with Java 17
  /**
   * The HmacPBESHA1 Message Authentication Code (MAC) algorithm specified in Appendix B.4 of RFC
   * 7292.
   */
  HMAC_PBE_SHA_1("HmacPBESHA1"),

  /**
   * The HmacPBESHA224 Message Authentication Code (MAC) algorithm specified in Appendix B.4 of RFC
   * 7292.
   */
  HMAC_PBE_SHA_224("HmacPBESHA224"),

  /**
   * The HmacPBESHA256 Message Authentication Code (MAC) algorithm specified in Appendix B.4 of RFC
   * 7292.
   */
  HMAC_PBE_SHA_256("HmacPBESHA256"),

  /**
   * The HmacPBESHA384 Message Authentication Code (MAC) algorithm specified in Appendix B.4 of RFC
   * 7292.
   */
  HMAC_PBE_SHA_384("HmacPBESHA384"),

  /**
   * The HmacPBESHA512 Message Authentication Code (MAC) algorithm specified in Appendix B.4 of RFC
   * 7292.
   */
  HMAC_PBE_SHA_512("HmacPBESHA512"),

  /**
   * The HmacPBESHA512 Message Authentication Code (MAC) algorithm specified in Appendix B.4 of RFC
   * 7292.
   */
  HMAC_PBE_SHA_512_224("HmacPBESHA512/224"),

  /**
   * The HmacPBESHA512 Message Authentication Code (MAC) algorithm specified in Appendix B.4 of RFC
   * 7292.
   */
  HMAC_PBE_SHA_512_256("HmacPBESHA512/256");

  private final String name;

  HmacAlgorithms(final String algorithm) {
    this.name = algorithm;
  }

  /**
   * Gets the algorithm name.
   *
   * @return the algorithm name
   */
  public String getName() {
    return name;
  }
}
