/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.crypto;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.KeyStore.LoadStoreParameter;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Objects;
import javax.crypto.KeyAgreement;
import javax.crypto.SecretKeyFactory;

/**
 * Key generator utility
 *
 * @see <a
 *     href="https://docs.oracle.com/en/java/javase/21/docs/specs/security/standard-names.html">Security
 *     Standard Algorithm Names</a>
 * @author Leonard Woo
 */
public class KeyUtil {

  private KeyUtil() {}

  /**
   * Returns a SecretKeyFactory object that converts secret keys of the specified algorithm.
   *
   * @see SecretKeyFactory
   * @param algorithm Secret Key Factory algorithm.
   * @param provider an instance of the provider.
   * @param keySpec KeySpec impl e.g. {@code PBEKeySpec}.
   * @return Secret Key Factory instance.
   * @throws NoSuchAlgorithmException if no Provider supports a SecretKeyFactorySpi implementation
   *     for the specified algorithm.
   * @throws InvalidKeySpecException if the given key specification is inappropriate for this
   *     secret-key factory to produce a secret key.
   * @throws NullPointerException if algorithm is null.
   */
  public static byte[] secretKeyFactory(String algorithm, Provider provider, KeySpec keySpec)
      throws NoSuchAlgorithmException, InvalidKeySpecException, NullPointerException {
    return secretKeyFactory(algorithm, provider).generateSecret(keySpec).getEncoded();
  }

  /**
   * Returns a SecretKeyFactory object that converts secret keys of the specified algorithm.
   *
   * @see SecretKeyFactory
   * @param algorithm Secret Key Factory algorithm.
   * @param provider an instance of the provider.
   * @return Secret Key Factory instance.
   * @throws NoSuchAlgorithmException if no Provider supports a SecretKeyFactorySpi implementation
   *     for the specified algorithm.
   * @throws NullPointerException if algorithm is null.
   */
  public static SecretKeyFactory secretKeyFactory(String algorithm, Provider provider)
      throws NoSuchAlgorithmException, NullPointerException {
    if (provider == CryptoUtil.NONPROVIDER) {
      return SecretKeyFactory.getInstance(algorithm);
    }
    return SecretKeyFactory.getInstance(algorithm, provider);
  }

  /**
   * Returns a KeyFactory object that converts public/private keys of the specified algorithm.
   *
   * @see KeyFactory
   * @param algorithm key algorithm.
   * @param provider an instance of the provider.
   * @return KeyFactory instance.
   * @throws NullPointerException if algorithm is null.
   * @throws NoSuchAlgorithmException if a KeyFactorySpi implementation for the specified algorithm
   *     is not available from the specified provider.
   */
  public static KeyFactory keyFactory(String algorithm, Provider provider)
      throws NullPointerException, NoSuchAlgorithmException {
    if (provider == CryptoUtil.NONPROVIDER) {
      return KeyFactory.getInstance(algorithm);
    }
    return KeyFactory.getInstance(algorithm, provider);
  }

  /**
   * Returns a {@link KeyAgreement} instance
   *
   * @param algorithm the standard name of the requested key agreement algorithm.
   * @param provider the provider.
   * @return {@link KeyAgreement} instance.
   * @throws NullPointerException if algorithm is null.
   * @throws NoSuchAlgorithmException if no Provider supports a KeyAgreementSpi implementation for
   *     the specified algorithm.
   */
  public static KeyAgreement keyAgreement(String algorithm, Provider provider)
      throws NullPointerException, NoSuchAlgorithmException {
    if (provider == CryptoUtil.NONPROVIDER) {
      return KeyAgreement.getInstance(algorithm);
    }
    return KeyAgreement.getInstance(algorithm, provider);
  }

  /**
   * Returns a {@link KeyAgreement} object that implements the specified key agreement algorithm.
   *
   * @see KeyAgreement
   * @param algorithm the standard name of the requested key agreement algorithm.
   * @param provider the provider.
   * @param privateKey the party's private information.
   * @param publicKey the key for this phase.
   * @return the new buffer with the shared secret.
   * @throws NoSuchAlgorithmException if no Provider supports a KeyAgreementSpi implementation for
   *     the specified algorithm.
   * @throws NullPointerException if algorithm is null.
   * @throws InvalidKeyException if the given key is inappropriate for this key agreement or phase.
   */
  public static byte[] keyAgreement(
      String algorithm, Provider provider, Key privateKey, Key publicKey)
      throws NoSuchAlgorithmException, NullPointerException, InvalidKeyException {
    KeyAgreement ka = keyAgreement(algorithm, provider);
    ka.init(privateKey);
    ka.doPhase(publicKey, true);
    return ka.generateSecret();
  }

  /**
   * KeyStore util
   *
   * @see KeyStore
   * @param type KeyStore type.
   * @param is the input stream from which the keystore is loaded, or {@code null}.
   * @param password the password used to check the integrity of the keystore, the password used to
   *     unlock the keystore, or {@code null}.
   * @return KeyStore instance.
   * @throws KeyStoreException if no Provider supports a KeyStoreSpi implementation for the
   *     specified type.
   * @throws CertificateException if any of the certificates in the keystore could not be loaded.
   * @throws IOException if there is an I/O or format problem with the keystore data, if a password
   *     is required but not given, or if the given password was incorrect.
   * @throws NoSuchAlgorithmException if the algorithm used to check the integrity of the keystore
   *     cannot be found.
   */
  public static KeyStore keyStore(KeyStoreAlgorithms type, InputStream is, char[] password)
      throws KeyStoreException, CertificateException, IOException, NoSuchAlgorithmException {
    KeyStore keyStore = KeyStore.getInstance(type.getType());
    keyStore.load(is, password);
    return keyStore;
  }

  /**
   * KeyStore util
   *
   * @see KeyStore
   * @param type KeyStore type.
   * @param loadStore the {@link LoadStoreParameter} that specifies how to load the keystore, which may be {@code null}
   * @return KeyStore instance.
   * @throws KeyStoreException if no Provider supports a KeyStoreSpi implementation for the
   *     specified type.
   * @throws CertificateException if any of the certificates in the keystore could not be loaded.
   * @throws IOException if there is an I/O or format problem with the keystore data, if a password
   *     is required but not given, or if the given password was incorrect.
   * @throws NoSuchAlgorithmException if the algorithm used to check the integrity of the keystore
   *     cannot be found.
   */
  public static KeyStore keyStore(KeyStoreAlgorithms type, LoadStoreParameter loadStore)
      throws KeyStoreException, CertificateException, IOException, NoSuchAlgorithmException {
    KeyStore keyStore = KeyStore.getInstance(type.getType());
    keyStore.load(loadStore);
    return keyStore;
  }

  /**
   * Generate RSA public key using RSA private key
   *
   * @param rsaPrivateKey RSA private key
   * @return RSA public key,
   * @throws NullPointerException if RSA private key is null.
   * @throws IllegalArgumentException if something is wrong.
   */
  public static RSAPublicKey getRSAPublicKey(RSAPrivateKey rsaPrivateKey)
      throws NullPointerException, IllegalArgumentException {
    Objects.requireNonNull(rsaPrivateKey);
    try {
      RSAPrivateCrtKey rsaPrivateCrtKey = (RSAPrivateCrtKey) rsaPrivateKey;
      RSAPublicKeySpec rsaPublicKeySpec = new RSAPublicKeySpec(rsaPrivateCrtKey.getModulus(),
          rsaPrivateCrtKey.getPublicExponent());

      return (RSAPublicKey) keyFactory("RSA", null)
          .generatePublic(rsaPublicKeySpec);
    } catch (InvalidKeySpecException | NoSuchAlgorithmException ex) {
      throw new IllegalArgumentException(ex);
    }
  }

}
