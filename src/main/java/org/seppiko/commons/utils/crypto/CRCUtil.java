/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.crypto;

import java.util.zip.CRC32C;
import org.seppiko.commons.utils.crypto.crc.CRC16MAXIM;
import org.seppiko.commons.utils.crypto.crc.CRC24;
import org.seppiko.commons.utils.crypto.crc.CRC64ECMA182;

/**
 * CRC utility
 *
 * @author Leonard Woo
 */
public class CRCUtil {

  private CRCUtil() {}

  /**
   * Return CRC-16 MAXIM
   *
   * @param data data byte array
   * @return the current checksum value
   */
  public static long getCRC16M(byte[] data) {
    CRC16MAXIM crc16MAXIM = new CRC16MAXIM();
    crc16MAXIM.update(data);
    return crc16MAXIM.getValue();
  }

  /**
   * Return CRC-24
   *
   * @param data data byte array
   * @return the current checksum value
   */
  public static long getCRC24C(byte[] data) {
    CRC24 crc24 = new CRC24();
    crc24.update(data);
    return crc24.getValue();
  }

  /**
   * Return CRC-32C
   *
   * @param data data byte array
   * @return the current checksum value
   */
  public static long getCRC32(byte[] data) {
    CRC32C crc32 = new CRC32C();
    crc32.update(data);
    return crc32.getValue();
  }

  /**
   * Return CRC64 ECMA182
   *
   * @param data data byte array
   * @return the current checksum value
   */
  public static long getCRC64E(byte[] data) {
    CRC64ECMA182 crc64e = new CRC64ECMA182();
    crc64e.update(data);
    return crc64e.getValue();
  }
}
