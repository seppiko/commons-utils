/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.crypto;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;
import org.seppiko.commons.utils.StringUtil;

/**
 * Cipher name utility
 *
 * @author Leonard Woo
 */
public class CipherNameUtil implements Serializable {

  @Serial
  private static final long serialVersionUID = -215141540814890802L;

  /** algorithm name */
  private final String name;

  /** algorithm mode */
  private final String mode;

  /** algorithm padding */
  private final String padding;

  /**
   * Initialization cipher name
   *
   * @param name Algorithm Name.
   * @param mode Algorithm Mode.
   * @param padding Algorithm Padding.
   * @throws NullPointerException Name is {@code null}.
   * @throws IllegalArgumentException Mode and Padding must be {@code null} at the same time,
   *     or not {@code null} at the same time.
   */
  public CipherNameUtil(String name, String mode, String padding)
      throws NullPointerException, IllegalArgumentException {
    if (StringUtil.isNullOrEmpty(name)) {
      throw new NullPointerException("name must be not empty");
    }
    boolean isMode = StringUtil.isNullOrEmpty(mode);
    boolean isPadding = StringUtil.isNullOrEmpty(padding);
    if (!(isMode && isPadding)) {
      throw new IllegalArgumentException("algorithm mode must have padding");
    }
    this.name = name;
    this.mode = mode;
    this.padding = padding;
  }

  /**
   * Parser cipher name
   *
   * @param algorithm {@code Name[/Mode/Padding]]}.
   * @throws IllegalArgumentException Algorithm String format is wrong.
   * @throws NullPointerException Algorithm name is {@code null}.
   */
  public CipherNameUtil(String algorithm) throws IllegalArgumentException, NullPointerException {
    if (StringUtil.isNullOrEmpty(algorithm)) {
      throw new NullPointerException("algorithm name must be not null");
    }
    if (algorithm.indexOf('/') < 0) {
      this.name = algorithm;
      this.mode = null;
      this.padding = null;
    } else {
      String[] strs = algorithm.split("/");
      if (strs.length == 2) {
        throw new IllegalArgumentException("algorithm mode must with padding");
      } else if (strs.length == 3) {
        this.name = strs[0];
        this.mode = strs[1];
        this.padding = strs[2];
      } else {
        throw new IllegalArgumentException("algorithm name string parser failed");
      }
    }
  }

  /**
   * Get name
   *
   * @return Name
   */
  public String getName() {
    return name;
  }

  /**
   * Get mode
   *
   * @return Mode
   */
  public String getMode() {
    return mode;
  }

  /**
   * Get padding
   *
   * @return Padding
   */
  public String getPadding() {
    return padding;
  }

  /**
   * Check {@link CipherNameUtil} instance equal
   *
   * @param o a {@link CipherNameUtil} instance.
   * @return true, if object is {@link CipherNameUtil} and equals.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    CipherNameUtil that = (CipherNameUtil) o;
    return Objects.equals(name, that.name)
        && Objects.equals(mode, that.mode)
        && Objects.equals(padding, that.padding);
  }

  /** CipherNameUtil object hashcode */
  @Override
  public int hashCode() {
    return Objects.hash(name, mode, padding);
  }

  /** Re-splicing algorithm name */
  @Override
  public String toString() {
    if (null == mode && null == padding) {
      return name;
    }
    return String.format("%s/%s/%s", name, mode, padding);
  }
}
