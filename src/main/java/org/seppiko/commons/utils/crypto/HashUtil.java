/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.crypto;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import org.seppiko.commons.utils.codec.Base64Util;
import org.seppiko.commons.utils.codec.HexUtil;
import org.seppiko.commons.utils.crypto.spec.KeySpecUtil;

/**
 * Hash utility
 *
 * @author Leonard Woo
 */
public class HashUtil {

  private HashUtil() {}

  /**
   * Simplifies common {@link java.security.MessageDigest} tasks.
   *
   * @param algorithm Message Digest Algorithm
   * @param provider Hash provider, null is use default provider
   * @param data data to hash
   * @return complete hash value
   * @throws IllegalArgumentException when a {@link NoSuchAlgorithmException} is caught.
   */
  public static byte[] mdHash(MessageDigestAlgorithms algorithm, Provider provider, byte[] data)
      throws IllegalArgumentException {
    try {
      return CryptoUtil.md(algorithm.getName(), provider, data);
    } catch (NoSuchAlgorithmException ex) {
      throw new IllegalArgumentException(ex);
    }
  }

  /**
   * Simplifies common {@link java.security.MessageDigest} tasks.
   *
   * @param algorithm Message Digest Algorithm
   * @param provider Hash provider, null is use default provider
   * @param data data to hash
   * @return complete hash hex string
   * @throws IllegalArgumentException when a {@link NoSuchAlgorithmException} is caught.
   */
  public static String mdHashString(
      MessageDigestAlgorithms algorithm, Provider provider, byte[] data)
      throws IllegalArgumentException {
    return HexUtil.encode(mdHash(algorithm, provider, data));
  }

  /**
   * Simplifies common {@link javax.crypto.Mac} tasks.
   *
   * @param algorithm Hmac Algorithm
   * @param provider Hash provider, null is use default provider
   * @param data data to hash with key
   * @param key the keyed digest
   * @return complete hash value with salt
   * @throws InvalidKeyException if the given key is {@code null} or does not match the allowed
   *     pattern.
   * @throws IllegalArgumentException when a {@link NoSuchAlgorithmException} is caught.
   */
  public static byte[] hmacHash(
      HmacAlgorithms algorithm, Provider provider, byte[] data, byte[] key)
      throws InvalidKeyException, IllegalArgumentException {
    try {
      return CryptoUtil.mac(algorithm.getName(), provider, algorithm.getName(), data, key);
    } catch (NoSuchAlgorithmException ignored) {
    }
    throw new IllegalArgumentException();
  }

  /**
   * Simplifies common {@link javax.crypto.Mac} tasks.
   *
   * @param algorithm Hmac Algorithm
   * @param provider Hash provider, null is use default provider
   * @param data data to hash with key
   * @param key the keyed digest
   * @return complete hash hex string with salt
   * @throws InvalidKeyException if the given key is {@code null} or does not match the allowed
   *     pattern.
   * @throws IllegalArgumentException when a {@link NoSuchAlgorithmException} is caught.
   */
  public static String hmacHashString(
      HmacAlgorithms algorithm, Provider provider, byte[] data, byte[] key)
      throws InvalidKeyException, IllegalArgumentException {
    return HexUtil.encode(hmacHash(algorithm, provider, data, key));
  }

  /**
   * Simplifies common PBKDF2 tasks
   *
   * @param algorithm PBKDF2With&lt;Hmac Algorithm&gt;
   * @param provider Hash provider, null is use default provider
   * @param password raw password
   * @param salt password salt
   * @param iterations iteration
   * @param keySize key size
   * @return PBKDF2With&lt;Hmac Algorithm&gt; password
   * @throws IllegalArgumentException
   *     password or salt is null, number is wrong or could not execute task.
   */
  public static byte[] pbkdf2Hash(HmacAlgorithms algorithm, Provider provider, char[] password,
      byte[] salt, int iterations, int keySize) throws IllegalArgumentException {
    try {
      if (algorithm.getName().contains("PBE")) {
        throw new NoSuchAlgorithmException("Algorithm must be exclude PBE");
      }
      return KeyUtil.secretKeyFactory("PBKDF2With" + algorithm.getName(), provider,
          KeySpecUtil.getPBE(password, salt, iterations, keySize));
    } catch (NoSuchAlgorithmException | InvalidKeySpecException | IllegalArgumentException
        | NullPointerException ex) {
      throw new IllegalArgumentException(ex);
    }
  }

  /**
   * Simplifies common PBKDF2 tasks with Base64 and PHC String Format
   *
   * @see <a href="https://github.com/P-H-C/phc-string-format/blob/master/phc-sf-spec.md">PHC String Format</a>
   * @param algorithm PBKDF2With&lt;Hmac Algorithm&gt;
   * @param provider Hash provider, null is use default provider
   * @param password raw password
   * @param salt password salt
   * @param iterations iteration
   * @param keySize key size, usually is salt length * 8
   * @return PBKDF2With&lt;Hmac Algorithm&gt; with PHC String
   * @throws IllegalArgumentException
   *   password or salt is null, number is wrong or could not execute task.
   */
  public static String pbkdf2HashString(HmacAlgorithms algorithm, Provider provider, char[] password,
      byte[] salt, int iterations, int keySize) throws IllegalArgumentException {
    byte[] result = pbkdf2Hash(algorithm, provider, password, salt, iterations, keySize);
    String saltBase64 = Base64Util.encodeString(salt);
    String hashBase64 = Base64Util.encodeString(result);

    HashMap<String, Object> params = new HashMap<>();
    params.put("i", iterations);
    return PHCFormatUtil.format("pbkdf2-" + algorithm.getName(), null, params, saltBase64, hashBase64);
  }
}
