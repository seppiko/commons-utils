/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.crypto;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import org.seppiko.commons.utils.CharUtil;
import org.seppiko.commons.utils.CollectionUtil;
import org.seppiko.commons.utils.RegexUtil;
import org.seppiko.commons.utils.StringUtil;
import org.seppiko.commons.utils.exceptions.IllegalFormatException;

/**
 * PHC(Password Hashing Competition) String Format utility
 *
 * @see <a href="https://github.com/P-H-C/phc-string-format/blob/master/phc-sf-spec.md">PHC string format</a>
 * @author Leonard Woo
 */
public class PHCFormatUtil {

  private static final char SEPARATION = CharUtil.DOLLAR_SIGN;
  private static final String VERSION_KEY = "v=";

  private PHCFormatUtil() {}

  /**
   * Format to PHC String
   *
   * <pre><code>
   *   $&lt;id>[$v=&lt;version>][$&lt;param>=&lt;value>(,&lt;param>=&lt;value>)*][$&lt;salt>[$&lt;hash>]]
   * </code></pre>
   *
   * @param id The symbolic name for the function.
   * @param version The algorithm version. May be null or empty.
   * @param params A parameter map. May be null or empty. But value must be implementation {@link #toString}.
   * @param salt An encoding of the salt. May be null or empty.
   * @param hash An encoding of the hash output. May be null or empty, but must be with salt.
   * @return PHC String
   */
  public static String format(
      String id, String version, Map<String, Object> params, String salt, String hash) {
    return format(new Entity(id, version, params, salt, hash));
  }

  /**
   * Format to PHC String
   *
   * <pre><code>
   *   $&lt;id>[$v=&lt;version>][$&lt;param>=&lt;value>(,&lt;param>=&lt;value>)*][$&lt;salt>[$&lt;hash>]]
   * </code></pre>
   *
   * @param entity PHC String Entity
   * @return PHC String
   */
  public static String format(PHCFormatUtil.Entity entity) {
    StringBuilder sb = new StringBuilder();

    sb.append(SEPARATION);
    sb.append(StringUtil.requireNonBlank(entity.id()));
    if (!StringUtil.isNullOrEmpty(entity.version())) {
      sb.append(SEPARATION);
      sb.append(VERSION_KEY).append(entity.version());
    }
    if (null != entity.params() && !entity.params().isEmpty()) {
      sb.append(SEPARATION);
      sb.append(CollectionUtil.toString(entity.params(), key -> key, Object::toString, "=", ","));
    }
    if (!StringUtil.isNullOrEmpty(entity.salt())) {
      sb.append(SEPARATION);
      sb.append(entity.salt());
      if (!StringUtil.isNullOrEmpty(entity.hash())) {
        sb.append(SEPARATION);
        sb.append(entity.hash());
      }
    }

    return sb.toString();
  }

  /**
   * Parse PHC String format
   *
   * @param phc PHC String
   * @return PHC String Entity
   */
  public static PHCFormatUtil.Entity parser(final String phc) {
    check(phc);
    String[] strs = RegexUtil.split("\\" + SEPARATION, phc);

    String id = strs[1];
    String version = null;
    LinkedHashMap<String, Object> params = new LinkedHashMap<>();
    String salt = null;
    String hash = null;

    int nextIndex = 2;
    if (strs[nextIndex].startsWith(VERSION_KEY)) {
      version = strs[nextIndex].substring(VERSION_KEY.length());
      nextIndex++;
    }

    if (strs[nextIndex].matches("\\w+=\\w+,?")) {
      CollectionUtil.fromString(params, strs[nextIndex], "=", ",", String.class, Object.class);
      nextIndex++;
    }

    if (nextIndex < strs.length) {
      salt = strs[nextIndex++];
    }
    if (nextIndex < strs.length) {
      hash = strs[nextIndex];
    }

    return new PHCFormatUtil.Entity(id, version, params, salt, hash);
  }

  /**
   * Check PHC String Format
   *
   * @param phc PHC String
   */
  private static void check(String phc) {
    if (phc.charAt(0) != SEPARATION) {
      throw new IllegalFormatException("PHC Format must be start with '" + SEPARATION + "'");
    }
  }

  /**
   * PHC String Entity
   *
   * @param id The symbolic name for the function.
   * @param version The algorithm version.
   * @param params A parameter map.
   * @param salt An encoding of the salt.
   * @param hash An encoding of the hash output.
   */
  public record Entity(
      String id, String version, Map<String, Object> params, String salt, String hash) {

    @Override
    public boolean equals(Object o) {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;

      Entity entity = (Entity) o;

      if (!Objects.equals(id, entity.id))
        return false;
      if (!Objects.equals(version, entity.version))
        return false;
      if (!Objects.equals(params, entity.params))
        return false;
      if (!Objects.equals(salt, entity.salt))
        return false;
      return Objects.equals(hash, entity.hash);
    }

    @Override
    public int hashCode() {
      int result = id != null ? id.hashCode() : 0;
      result = 31 * result + (version != null ? version.hashCode() : 0);
      result = 31 * result + (params != null ? params.hashCode() : 0);
      result = 31 * result + (salt != null ? salt.hashCode() : 0);
      result = 31 * result + (hash != null ? hash.hashCode() : 0);
      return result;
    }
  }
}
