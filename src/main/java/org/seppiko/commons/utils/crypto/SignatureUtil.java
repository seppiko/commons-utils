/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.crypto;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;

/**
 * Signature utility
 *
 * @see <a
 *     href="https://docs.oracle.com/en/java/javase/21/docs/specs/security/standard-names.html#signature-algorithms">
 *     Java Security Standard Algorithm Names</a>
 * @author Leonard Woo
 */
public class SignatureUtil {

  private SignatureUtil() {}

  /**
   * Returns a Signature instance that implements the specified signature algorithm.
   *
   * @param algorithm the standard name of the algorithm requested.
   * @param provider the provider.
   * @return the new Signature instance.
   * @throws NoSuchAlgorithmException if no Provider supports a Signature implementation for the
   *     specified algorithm.
   * @throws NullPointerException if algorithm is {@code null}.
   */
  public static Signature signature(String algorithm, Provider provider)
      throws NoSuchAlgorithmException, NullPointerException {
    if (provider == CryptoUtil.NONPROVIDER) {
      return Signature.getInstance(algorithm);
    }
    return Signature.getInstance(algorithm, provider);
  }

  /**
   * Data Signature with private key.
   *
   * @see Signature
   * @param algorithm signature algorithm.
   * @param privateKey signature private key.
   * @param rawData data.
   * @return signed data.
   * @throws NoSuchAlgorithmException if no Provider supports a Signature implementation for the
   *     specified algorithm.
   * @throws InvalidKeyException if the key is invalid.
   * @throws SignatureException if this signature object is not initialized properly.
   * @throws NullPointerException if algorithm is {@code null}.
   */
  public static byte[] sign(String algorithm, PrivateKey privateKey, byte[] rawData)
      throws NoSuchAlgorithmException, NullPointerException, InvalidKeyException,
          SignatureException {
    return sign(algorithm, CryptoUtil.NONPROVIDER, privateKey, rawData);
  }

  /**
   * Data Verification with public key.
   *
   * @see Signature
   * @param algorithm signature algorithm.
   * @param publicKey signature private key.
   * @param rawData data.
   * @param signedData signed data.
   * @return true is verified.
   * @throws NoSuchAlgorithmException if no Provider supports a Signature implementation for the
   *     specified algorithm.
   * @throws InvalidKeyException if the key is invalid.
   * @throws SignatureException if this signature object is not initialized properly.
   * @throws NullPointerException if algorithm is {@code null}.
   */
  public static boolean verify(
      String algorithm, PublicKey publicKey, byte[] rawData, byte[] signedData)
      throws NoSuchAlgorithmException, NullPointerException, InvalidKeyException,
          SignatureException {
    return verify(algorithm, CryptoUtil.NONPROVIDER, publicKey, rawData, signedData);
  }

  /**
   * Data Signature with private key.
   *
   * @see Signature
   * @param algorithm signature algorithm.
   * @param provider the provider.
   * @param privateKey signature private key.
   * @param rawData data.
   * @return signed data.
   * @throws NoSuchAlgorithmException if no Provider supports a Signature implementation for the
   *     specified algorithm.
   * @throws InvalidKeyException if the key is invalid.
   * @throws SignatureException if this signature object is not initialized properly.
   * @throws NullPointerException if algorithm is {@code null}.
   */
  public static byte[] sign(
      String algorithm, Provider provider, PrivateKey privateKey, byte[] rawData)
      throws NoSuchAlgorithmException, NullPointerException, IllegalArgumentException,
          InvalidKeyException, SignatureException {
    Signature signature = signature(algorithm, provider);
    signature.initSign(privateKey);
    signature.update(rawData);
    return signature.sign();
  }

  /**
   * Data Verification with public key.
   *
   * @see Signature
   * @param algorithm signature algorithm.
   * @param provider the provider.
   * @param publicKey signature private key.
   * @param rawData data.
   * @param signedData signed data.
   * @return true is verified.
   * @throws NoSuchAlgorithmException if no Provider supports a Signature implementation for the
   *     specified algorithm.
   * @throws InvalidKeyException if the key is invalid.
   * @throws SignatureException if this signature object is not initialized properly.
   * @throws NullPointerException if algorithm is {@code null}.
   */
  public static boolean verify(
      String algorithm, Provider provider, PublicKey publicKey, byte[] rawData, byte[] signedData)
      throws NoSuchAlgorithmException, NullPointerException, IllegalArgumentException,
          InvalidKeyException, SignatureException {
    Signature signature = signature(algorithm, provider);
    signature.initVerify(publicKey);
    signature.update(rawData);
    return signature.verify(signedData);
  }
}
