/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.crypto.crc;

import java.util.Objects;
import java.util.zip.Checksum;

/**
 * CRC-24 implementation as described in RFC4880.
 *
 * @see <a href="https://datatracker.ietf.org/doc/html/rfc4880#section-6.1">
 *   RFC4880 §6.1. An Implementation of the CRC-24 in "C"</a>
 * @author Leonard Woo
 */
public class CRC24 implements Checksum {

  private static final int CRC24_INIT    = 0x0b704ce;
  private static final int CRC24_POLY    = 0x1864cfb;
  private static final int CRC24_OUTMASK = 0xffffff;

  private int crc;

  /** Create CRC-24 instance. */
  public CRC24() {
    crc = CRC24_INIT;
  }

  /**
   * Updates the current checksum with the specified byte.
   *
   * @param b the byte to update the checksum with.
   */
  @Override
  public void update(int b) {
    crc ^= b << 16;
    for (int i = 0; i < 8; i++) {
      crc <<= 1;
      if ((crc & 0x1000000) != 0) {
        crc ^= CRC24_POLY;
      }
    }
  }

  /**
   * Updates the current checksum with the specified array of bytes.
   *
   * @param b the byte array to update the checksum with.
   * @param off the start offset of the data.
   * @param len the number of bytes to use for the update.
   */
  @Override
  public void update(byte[] b, int off, int len) {
    updateCheck(b, off, len);
    for (int i = off; len > 0; i++, len--) {
      update(b[i]);
    }
  }

  /** Check bytes */
  private void updateCheck(byte[] b, int off, int len)
      throws NullPointerException, ArrayIndexOutOfBoundsException {
    Objects.requireNonNull(b);
    if (off < 0 || len < 0) {
      throw new IllegalArgumentException();
    }
    if (off > (b.length - len)) {
      throw new ArrayIndexOutOfBoundsException();
    }
  }

  /**
   * Returns the current checksum value.
   *
   * @return the current checksum value.
   */
  @Override
  public long getValue() {
    return crc & CRC24_OUTMASK;
  }

  /** Resets the checksum to its initial value. */
  @Override
  public void reset() {
    crc = CRC24_INIT;
  }
}
