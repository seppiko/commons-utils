/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.crypto;

/**
 * Standard {@link java.security.MessageDigest} algorithm names from the <cite>Java Cryptography
 * Architecture Standard Algorithm Name Documentation</cite>.
 *
 * @see <a
 *     href="https://docs.oracle.com/en/java/javase/21/docs/specs/security/standard-names.html#messagedigest-algorithms">
 *     Java Cryptography Architecture Standard Algorithm Name Documentation</a>
 * @see <a href="http://dx.doi.org/10.6028/NIST.FIPS.180-4">FIPS PUB 180-4</a>
 * @see <a href="http://dx.doi.org/10.6028/NIST.FIPS.202">FIPS PUB 202</a>
 * @author Leonard Woo
 */
public enum MessageDigestAlgorithms {

  /**
   * The MD2 message digest algorithm defined in RFC 1319.
   * This Algorithm is unsafe.
   */
  @Deprecated
  MD2("MD2"),

  /**
   * The MD5 message digest algorithm defined in RFC 1321.
   * This Algorithm is unsafe.
   */
  @Deprecated
  MD5("MD5"),

  /**
   * The SHA-1 hash algorithm defined in the FIPS PUB 180-2.
   * This Algorithm is unsafe.
   */
  @Deprecated
  SHA_1("SHA-1"),

  /** The SHA-224 hash algorithm defined in the FIPS PUB 180-3. */
  SHA_224("SHA-224"),

  /** The SHA-256 hash algorithm defined in the FIPS PUB 180-2. */
  SHA_256("SHA-256"),

  /** The SHA-384 hash algorithm defined in the FIPS PUB 180-2. */
  SHA_384("SHA-384"),

  /** The SHA-512 hash algorithm defined in the FIPS PUB 180-2. */
  SHA_512("SHA-512"),

  /** The SHA-512 hash algorithm defined in the FIPS PUB 180-4. */
  SHA_512_224("SHA-512/224"),

  /** The SHA-512 hash algorithm defined in the FIPS PUB 180-4. */
  SHA_512_256("SHA-512/256"),

  /** The SHA3-224 hash algorithm defined in the FIPS PUB 202. */
  SHA3_224("SHA3-224"),

  /** The SHA3-256 hash algorithm defined in the FIPS PUB 202. */
  SHA3_256("SHA3-256"),

  /** The SHA3-384 hash algorithm defined in the FIPS PUB 202. */
  SHA3_384("SHA3-384"),

  /** The SHA3-512 hash algorithm defined in the FIPS PUB 202. */
  SHA3_512("SHA3-512");

  private final String name;

  MessageDigestAlgorithms(final String name) {
    this.name = name;
  }

  /**
   * Gets this algorithm name.
   *
   * @return this algorithm name
   */
  public String getName() {
    return name;
  }
}
