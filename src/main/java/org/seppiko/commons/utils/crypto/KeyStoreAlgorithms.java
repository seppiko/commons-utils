/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.crypto;

/**
 * KeyStore algorithm
 *
 * @see <a
 *     href="https://docs.oracle.com/en/java/javase/21/docs/specs/security/standard-names.html#keystore-types">Security
 *     Standard Algorithm Names</a>
 * @author Leonard Woo
 */
public enum KeyStoreAlgorithms {

  /** The proprietary keystore implementation provided by the SunJCE provider. */
  JCEKS("jceks"),

  /** The proprietary keystore implementation provided by the SUN provider. */
  JKS("jks"),

  /**
   * A domain keystore is a collection of keystores presented as a single logical keystore.
   * It is specified by configuration data whose syntax is described in
   * the {@link java.security.DomainLoadStoreParameter} class.
   */
  DKS("dks"),

  /** A keystore backed by a PKCS #11 token. */
  PKCS11("pkcs11"),

  /**
   * The transfer syntax for personal identity information as defined in PKCS #12 specified in RFC
   * 7292
   */
  PKCS12("pkcs12"),
  ;

  private final String type;

  KeyStoreAlgorithms(String type) {
    this.type = type;
  }

  /**
   * get algorithm type
   *
   * @return type
   */
  public String getType() {
    return type;
  }
}
