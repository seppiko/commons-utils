/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import org.seppiko.commons.utils.reflect.TypeUtil;

/**
 * Collections utility
 *
 * @see Collection#stream()
 * @see java.util.stream.Stream
 * @see java.util.Collections
 * @author Leonard Woo
 */
public class CollectionUtil {

  private CollectionUtil() {}

  /**
   * Return {@code true} if the supplied Collection is {@code null} or empty.
   * Otherwise, return {@code false}.
   *
   * @param collection the Collection to check.
   * @return whether the given Collection is empty.
   */
  public static boolean isEmpty(Collection<?> collection) {
    return (collection == null || collection.isEmpty());
  }

  /**
   * Return {@code true} if the supplied Map is {@code null} or empty.
   * Otherwise, return {@code false}.
   *
   * @param map the Map to check.
   * @return whether the given Map is empty.
   */
  public static boolean isEmpty(Map<?, ?> map) {
    return (map == null || map.isEmpty());
  }

  /**
   * Populates a Map using the supplied {@code Function} to transform the elements into keys, using
   * the unaltered element as the value in the {@link Map}. If the key is duplicated, only the new
   * key-value is retained.
   *
   * @param elements the {@link Collection} containing the input values for the map.
   * @param keyFunction the {@link Function} used to transform the element into a key value.
   * @return the {@code Map} to populate.
   * @param <K> the key type.
   * @param <V> the value type.
   */
  public static <K, V> Map<K, V> populateMap(
      final Collection<? extends V> elements, final Function<? super V, ? extends K> keyFunction) {
    return populateMap(elements, keyFunction, Function.identity());
  }

  /**
   * Populates a Map using the supplied {@link Function}s to transform the elements into keys and
   * values. If the key is duplicated, only the new key-value is retained.
   *
   * @param elements the {@link Collection} containing the input values for the map.
   * @param keyFunction the {@link Function} used to transform the element into a key value.
   * @param valueFunction the {@link Function} used to transform the element into a value.
   * @return the {@link Map} to populate.
   * @param <E> the type of object contained in the {@link Collection}.
   * @param <K> the key type.
   * @param <V> the value type.
   */
  public static <E, K, V> Map<K, V> populateMap(
      final Collection<? extends E> elements,
      final Function<? super E, ? extends K> keyFunction,
      final Function<? super E, ? extends V> valueFunction) {
    return elements.stream()
        .collect(Collectors.toMap(keyFunction, valueFunction, (key1, key2) -> key2));
  }

  /**
   * Populates a Map using the supplied {@link Function}s to transform the elements into keys and
   * values.
   *
   * @param elements the {@link Collection} containing the input values for the map.
   * @param keyFunction the {@link Function} used to transform the element into a key value.
   * @return the {@code Map} to populate.
   * @param <K> the key type.
   * @param <V> the value type.
   */
  public static <K, V> Map<K, V> populateMapWithoutFilter(
      final Collection<? extends V> elements, final Function<? super V, ? extends K> keyFunction) {
    return populateMapWithoutFilter(elements, keyFunction, Function.identity());
  }

  /**
   * Populates a Map using the supplied {@link Function}s to transform the elements into keys and
   * values.
   *
   * @param elements the {@link Collection} containing the input values for the map.
   * @param keyFunction the {@link Function} used to transform the element into a key value.
   * @param valueFunction the {@link Function} used to transform the element into a value.
   * @return the {@link Map} to populate.
   * @param <E> the type of object contained in the {@link Collection}.
   * @param <K> the key type.
   * @param <V> the value type.
   * @throws IllegalStateException Duplicate keys.
   */
  public static <E, K, V> Map<K, V> populateMapWithoutFilter(
      final Collection<? extends E> elements,
      final Function<? super E, ? extends K> keyFunction,
      final Function<? super E, ? extends V> valueFunction)
      throws IllegalStateException {
    return elements.stream()
        .collect(Collectors.toMap(keyFunction, valueFunction));
  }

  /**
   * Populates a List using the supplied {@link Function}s to transform the fields into values.
   *
   * @param elements Collection object.
   * @param valueFunction Object field function.
   * @return Field list.
   * @param <E> the type of object contained in the {@link Collection}.
   * @param <T> the type of object contained in the {@link List}.
   */
  public static <E, T> List<T> populateList(
      final Collection<? extends E> elements,
      final Function<? super E, ? extends T> valueFunction) {
    return elements.stream()
        .map(valueFunction)
        .collect(Collectors.toList());
  }

  /**
   * Populates a Set using the supplied {@link Function}s to transform the fields into values.
   *
   * @param elements Collection object.
   * @param valueFunction Object field function.
   * @return Field set.
   * @param <E> the type of object contained in the {@link Collection}.
   * @param <T> the type of object contained in the {@link Set}.
   */
  public static <E, T> Set<T> populateSet(
      final Collection<? extends E> elements,
      final Function<? super E, ? extends T> valueFunction) {
    return elements.stream()
        .map(valueFunction)
        .collect(Collectors.toSet());
  }

  /**
   * Returns a clone that accumulates the input elements into a new {@link Collection}, in encounter
   * order.
   *
   * @param collection source collection object e.g. {@link List} {@link Set} or {@link Map}.
   * @param supplier a supplier providing a new empty {@link Collection} into which the results will
   *     be inserted.
   * @return a {@link Collector} which collects all the input elements into a {@link Collection}, in
   *     encounter order.
   * @param <T> the type of the input elements.
   * @param <C> the type of the resulting {@link Collection}.
   */
  public static <T, C extends Collection<T>> Collection<T> clone(
      final Collection<? extends T> collection, final Supplier<C> supplier) {
    Assert.notEmpty(collection, "collection must not be null or empty.");
    Assert.notNull(supplier, "supplier must not be null.");
    return collection.stream()
        .collect(Collectors.toCollection(supplier));
  }

  /**
   * Returns a clone that accumulates the input elements into a new {@link Map}, in encounter
   * order.
   *
   * @param map source map object
   * @return a {@link Collector} which collects all the input elements into a {@link Map}, in
   *    encounter order.
   * @param <K> the type of the map key.
   * @param <V> the type of the map value.
   */
  public static <K, V> Map<K, V> clone(final Map<K, V> map) {
    Assert.notEmpty(map, "map must not be null or empty.");
    return map.entrySet()
        .stream()
        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
  }

  /**
   * Returns a Collector that concatenates the input elements, separated by the specified delimiter,
   * with the specified prefix and suffix, in encounter order. Only support {@link List} and {@link
   * Set}.
   *
   * @param elements the {@link Collection} containing the input values.
   * @param elementFunction Object field function.
   * @param delimiter the delimiter to be used between each element.
   * @return String
   * @param <E> the type of the input elements.
   */
  public static <E> String toString(
      final Collection<E> elements,
      final Function<? super E, String> elementFunction,
      final CharSequence delimiter) {
    return toString(elements, elementFunction, delimiter, "", "");
  }

  /**
   * Returns a Collector that concatenates the input elements, separated by the specified delimiter,
   * with the specified prefix and suffix, in encounter order. Only support {@link List} and {@link
   * Set}.
   *
   * @param elements the {@link Collection} containing the input values.
   * @param elementFunction Object field function.
   * @param delimiter the delimiter to be used between each element.
   * @param prefix the sequence of characters to be used at the beginning of the joined result.
   * @param suffix the sequence of characters to be used at the end of the joined result.
   * @return String
   * @param <E> the type of the input elements.
   */
  public static <E> String toString(
      final Collection<E> elements,
      final Function<? super E, String> elementFunction,
      final CharSequence delimiter,
      final CharSequence prefix,
      final CharSequence suffix) {
    return elements.stream()
        .map(elementFunction)
        .collect(Collectors.joining(delimiter, prefix, suffix));
  }

  /**
   * Returns a Map that concatenates the input KVs(key/values), separated by the specified follower
   * and delimiter, with the specified prefix and suffix, in encounter order.
   *
   * @param map the {@link Map} containing the input KVs.
   * @param keyFunction key field function.
   * @param valueFunction value field function with {@link #toString}.
   * @param follower the follower to be used between Key and Value.
   * @param delimiter the delimiter to be used between each Key/Value.
   * @return String
   * @param <K> the type of the input keys.
   * @param <V> the type of the input values.
   */
  public static <K, V> String toString(
      final Map<K, V> map,
      final Function<? super K, String> keyFunction,
      final Function<V, Object> valueFunction,
      final CharSequence follower,
      final CharSequence delimiter) {
    return toString(map, keyFunction, valueFunction, follower, delimiter, "", "");
  }

  /**
   * Returns a Map that concatenates the input Key/Values, separated by the specified follower and
   * delimiter, with the specified prefix and suffix, in encounter order.
   *
   * @param map the {@link Map} containing the input KVs.
   * @param keyFunction key field function.
   * @param valueFunction value field function with {@link #toString}.
   * @param follower the follower to be used between Key and Value.
   * @param delimiter the delimiter to be used between each Key/Value.
   * @param prefix the sequence of characters to be used at the beginning of the joined result.
   * @param suffix the sequence of characters to be used at the end of the joined result.
   * @return String
   * @param <K> the type of the input keys.
   * @param <V> the type of the input values.
   */
  public static <K, V> String toString(
      final Map<K, V> map,
      final Function<? super K, String> keyFunction,
      final Function<V, Object> valueFunction,
      final CharSequence follower,
      final CharSequence delimiter,
      final CharSequence prefix,
      final CharSequence suffix) {
    return map.keySet().stream()
        .map(k -> keyFunction.apply(k) + follower + valueFunction.apply(map.get(k)).toString())
        .collect(Collectors.joining(delimiter, prefix, suffix));
  }

  /**
   * Swap T list index a and b.
   *
   * @param list origin T list.
   * @param a first index.
   * @param b second index.
   * @param <T> Object type.
   */
  public static <T> void swap(List<T> list, final int a, final int b) {
    T tmp = list.get(a);
    list.set(a, list.get(b));
    list.set(b, tmp);
  }

  /**
   * Parse key/value pairs string to map.
   *
   * @param map the {@link Map} subclass instance.
   * @param str the pairs String.
   * @param follower the follower to be used between key and value.
   * @param delimiter the delimiter to be used between each key/value pair.
   * @param keyType key type class.
   * @param valueType value type class.
   * @param <K> the type of the key.
   * @param <V> the type of the values.
   * @throws NullPointerException map or str is null.
   * @throws IllegalArgumentException if string contains duplicate keys
   */
  public static <K, V> void fromString(
      Map<K, V> map,
      final String str,
      final String follower,
      final String delimiter,
      Class<K> keyType,
      Class<V> valueType)
      throws NullPointerException, IllegalArgumentException {
    Objects.requireNonNull(map, "map must not be null.");
    Objects.requireNonNull(str, "str must not be null.");
    if (str.contains(delimiter)) {
      String[] pairs = RegexUtil.split(delimiter, str);
      for (String pair : pairs) {
        parsePair(map, pair, follower, keyType, valueType);
      }
    } else {
      parsePair(map, str, follower, keyType, valueType);
    }
  }

  /**
   * Parse a key/value pair string.
   *
   * @param map the {@link Map} subclass instance.
   * @param pair the key/value pair.
   * @param follower the follower to be used between key and value.
   * @param keyType key type class.
   * @param valueType value type class.
   * @param <K> the type of the keys.
   * @param <V> the type of the values.
   * @throws IllegalArgumentException if string contains duplicate keys.
   * @throws NullPointerException if pair is null.
   */
  private static <K, V> void parsePair(
      Map<K, V> map,
      final String pair,
      final String follower,
      Class<K> keyType,
      Class<V> valueType)
      throws IllegalArgumentException {
    Objects.requireNonNull(pair, "pair must not be null.");
    String[] keyValue = RegexUtil.split(follower, pair);
    K key = TypeUtil.fromObject(keyValue[0], keyType);
    V value = TypeUtil.fromObject(keyValue[1], valueType);
    if (map.containsKey(key)) {
      V oldValue = map.get(key);
      throw new IllegalArgumentException(
          "Key " + key.toString() + " already contains old value " + oldValue.toString());
    } else {
      map.put(key, value);
    }
  }

  /**
   * Convert List to Array.
   *
   * @param list list.
   * @return array.
   * @param <T> Object type.
   */
  @SuppressWarnings("unchecked")
  public static <T> T[] toArray(List<T> list) {
    if (list == null) {
      list = new ArrayList<>(0);
    }
    return (T[]) list.toArray(Object[]::new);
  }

  /**
   * Convert Set to Array.
   *
   * @param set set.
   * @return array.
   * @param <T> Object type.
   */
  @SuppressWarnings("unchecked")
  public static <T> T[] toArray(Set<T> set) {
    if (set == null) {
      set = new HashSet<>(0);
    }
    return (T[]) set.toArray(Object[]::new);
  }
}
