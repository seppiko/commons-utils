/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils;

import java.io.Serial;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Character utility
 *
 * @author Leonard Woo
 */
public class CharUtil implements Serializable {

  @Serial
  private static final long serialVersionUID = 7063635226739813394L;

  private CharUtil() {}

  /** NUL or {@code '\0'} */
  public static final Character NULL = 0x00;

  /** HT or {@code '\t'} */
  public static final Character HORIZONTAL_TABULATION = 0x09;

  /** LF or {@code '\n'} */
  public static final Character LINE_FEED = 0x0A;

  /** VT or {@code '\v'} */
  public static final Character VERTICAL_TABULATION = 0x0B;

  /** FF or {@code '\f'} */
  public static final Character FORM_FEED = 0x0C;

  /** CR or {@code '\r'} */
  public static final Character CARRIAGE_RETURN = 0x0D;

  /** CRLF or {@code "\r\n"} */
  public static final String CRLF = String.valueOf(new char[]{CARRIAGE_RETURN, LINE_FEED});

  /** ISO-LATIN-1(ASCII) digit char array */
  public static final char[] DIGIT = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

  /** ISO-LATIN-1(ASCII) digits {@code '0'} */
  public static final char DIGIT_ZERO = DIGIT[0];

  /** ISO-LATIN-1(ASCII) digits {@code '1'} */
  public static final char DIGIT_ONE = DIGIT[1];

  /** ISO-LATIN-1(ASCII) digits {@code '9'} */
  public static final char DIGIT_NINE = DIGIT[9];

  /** All uppercase letters char array */
  public static final char[] UPPERCASE = {
      'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
      'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
  };

  /** ISO-LATIN-1(ASCII) uppercase first letter {@code 'A'} */
  public static final char UPPERCASE_A = UPPERCASE[0];

  /** ISO-LATIN-1(ASCII) uppercase last letter {@code 'Z'} */
  public static final char UPPERCASE_Z = UPPERCASE[25];

  /** All lowercase letters char array */
  public static final char[] LOWERCASE = {
      'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
      't', 'u', 'v', 'w', 'x', 'y', 'z'
  };

  /** ISO-LATIN-1(ASCII) uppercase first letter {@code 'a'} */
  public static final char LOWERCASE_A = LOWERCASE[0];

  /** ISO-LATIN-1(ASCII) uppercase last letter {@code 'z'} */
  public static final char LOWERCASE_Z = LOWERCASE[25];

  /** {@code ' '} */
  public static final Character SPACE = 0x20;

  /** {@code '!'} */
  public static final Character EXCLAMATION_MARK = 0x21;

  /** {@code '"'} */
  public static final Character QUOTATION_MARK = 0x22;

  /** {@code '#'} */
  public static final Character NUMBER_SIGN = 0x23;

  /** {@code '$'} */
  public static final Character DOLLAR_SIGN = 0x24;

  /** {@code '%'} */
  public static final Character PERCENT_SIGN = 0x25;

  /** {@code '&'} */
  public static final Character AMPERSAND = 0x26;

  /** {@code '\\u0027'} */
  public static final Character APOSTROPHE = 0x27;

  /** {@code '('} */
  public static final Character LEFT_PARENTHESIS = 0x28;

  /** {@code ')'} */
  public static final Character RIGHT_PARENTHESIS = 0x29;

  /** {@code '*'} */
  public static final Character ASTERISK = 0x2A;

  /** {@code '+'} */
  public static final Character PLUS = 0x2B;

  /** {@code ','} */
  public static final Character COMMA = 0x2C;

  /** {@code '-'} */
  public static final Character HYPHEN_MINUS = 0x2D;

  /** {@code '.'} */
  public static final Character FULL_STOP = 0x2E;

  /** {@code '/'} */
  public static final Character SOLIDUS = 0x2F;

  /** {@code ':'} */
  public static final Character COLON = 0x3A;

  /** {@code ';'} */
  public static final Character SEMICOLON = 0x3B;

  /** {@code '<'} */
  public static final Character LESS_THAN_SIGN = 0x3C;

  /** {@code '='} */
  public static final Character EQUALS_SIGN = 0x3D;

  /** {@code '>'} */
  public static final Character GREATER_THAN_SIGN = 0x3E;

  /** {@code '?'} */
  public static final Character QUESTION_MARK = 0x3F;

  /** {@code '@'} */
  public static final Character COMMERCIAL_AT = 0x40;

  /** {@code '['} */
  public static final Character LEFT_SQUARE_BRACKET = 0x5B;

  /** {@code '\'} */
  public static final Character REVERSE_SOLIDUS = 0x5C;

  /** {@code ']'} */
  public static final Character RIGHT_SQUARE_BRACKET = 0x5D;

  /** {@code '^'} */
  public static final Character CIRCUMFLEX_ACCENT = 0x5E;

  /** {@code '_'} */
  public static final Character LOW_LINE = 0x5F;

  /** {@code '`'} */
  public static final Character GRAVE_ACCENT = 0x60;

  /** {@code '\\u007B' } */
  public static final Character LEFT_CURLY_BRACKET = 0x7B;

  /** {@code '|'} */
  public static final Character VERTICAL_LINE = 0x7C;

  /** {@code '\\u007D'} */
  public static final Character RIGHT_CURLY_BRACKET = 0x7D;

  /** {@code '~'} */
  public static final Character TILDE = 0x7E;

  /** All punctuation char array */
  public static final char[] PUNCTUATION = {
      EXCLAMATION_MARK, QUOTATION_MARK, NUMBER_SIGN, DOLLAR_SIGN, PERCENT_SIGN, AMPERSAND,
      APOSTROPHE, LEFT_PARENTHESIS, RIGHT_PARENTHESIS, ASTERISK, PLUS, COMMA, HYPHEN_MINUS,
      FULL_STOP, SOLIDUS, COLON, SEMICOLON, LESS_THAN_SIGN, EQUALS_SIGN, GREATER_THAN_SIGN,
      QUESTION_MARK, COMMERCIAL_AT, LEFT_SQUARE_BRACKET, REVERSE_SOLIDUS, RIGHT_SQUARE_BRACKET,
      CIRCUMFLEX_ACCENT, LOW_LINE, GRAVE_ACCENT, LEFT_CURLY_BRACKET, VERTICAL_LINE,
      RIGHT_CURLY_BRACKET, TILDE
  };

  /** Fullwidth digit char array */
  public static final char[] FULLWIDTH_DIGIT = {'０', '１', '２', '３', '４', '５', '６', '７', '８', '９'};

  /** Fullwidth digits {@code '０'} */
  public static final char FULLWIDTH_DIGIT_ZERO = FULLWIDTH_DIGIT[0];

  /** Fullwidth digits {@code '１'} */
  public static final char FULLWIDTH_DIGIT_ONE = FULLWIDTH_DIGIT[1];

  /** Fullwidth digits {@code '９'} */
  public static final char FULLWIDTH_DIGIT_NINE = FULLWIDTH_DIGIT[9];

  /** All fullwidth uppercase letters char array */
  public static final char[] FULLWIDTH_UPPERCASE = {
      'Ａ', 'Ｂ', 'Ｃ', 'Ｄ', 'Ｅ', 'Ｆ', 'Ｇ', 'Ｈ', 'Ｉ', 'Ｊ', 'Ｋ', 'Ｌ', 'Ｍ', 'Ｎ', 'Ｏ', 'Ｐ', 'Ｑ', 'Ｒ', 'Ｓ',
      'Ｔ', 'Ｕ', 'Ｖ', 'Ｗ', 'Ｘ', 'Ｙ', 'Ｚ'
  };

  /** ISO-LATIN-1(ASCII) uppercase first letter {@code 'Ａ'} */
  public static final char FULLWIDTH_UPPERCASE_A = FULLWIDTH_UPPERCASE[0];

  /** ISO-LATIN-1(ASCII) uppercase last letter {@code 'Ｚ'} */
  public static final char FULLWIDTH_UPPERCASE_Z = FULLWIDTH_UPPERCASE[25];

  /** All fullwidth lowercase letters char array */
  public static final char[] FULLWIDTH_LOWERCASE = {
      'ａ', 'ｂ', 'ｃ', 'ｄ', 'ｅ', 'ｆ', 'ｇ', 'ｈ', 'ｉ', 'ｊ', 'ｋ', 'ｌ', 'ｍ', 'ｎ', 'ｏ', 'ｐ', 'ｑ', 'ｒ', 'ｓ',
      'ｔ', 'ｕ', 'ｖ', 'ｗ', 'ｘ', 'ｙ', 'ｚ'
  };

  /** ISO-LATIN-1(ASCII) uppercase first letter {@code 'ａ'} */
  public static final char FULLWIDTH_LOWERCASE_A = FULLWIDTH_LOWERCASE[0];

  /** ISO-LATIN-1(ASCII) uppercase last letter {@code 'ｚ'} */
  public static final char FULLWIDTH_LOWERCASE_Z = FULLWIDTH_LOWERCASE[25];

  /** {@code '！'} */
  public static final Character FULLWIDTH_EXCLAMATION_MARK = 0xFF01;

  /** {@code '＂'} */
  public static final Character FULLWIDTH_QUOTATION_MARK = 0xFF02;

  /** {@code '＃'} */
  public static final Character FULLWIDTH_NUMBER_SIGN = 0xFF03;

  /** {@code '＄'} */
  public static final Character FULLWIDTH_DOLLAR_SIGN = 0xFF04;

  /** {@code '％'} */
  public static final Character FULLWIDTH_PERCENT_SIGN = 0xFF05;

  /** {@code '＆'} */
  public static final Character FULLWIDTH_AMPERSAND = 0xFF06;

  /** {@code '＇'} */
  public static final Character FULLWIDTH_APOSTROPHE = 0xFF07;

  /** {@code '（'} */
  public static final Character FULLWIDTH_LEFT_PARENTHESIS = 0xFF08;

  /** {@code '）'} */
  public static final Character FULLWIDTH_RIGHT_PARENTHESIS = 0xFF09;

  /** {@code '＊'} */
  public static final Character FULLWIDTH_ASTERISK = 0xFF0A;

  /** {@code '＋'} */
  public static final Character FULLWIDTH_PLUS = 0xFF0B;

  /** {@code '，'} */
  public static final Character FULLWIDTH_COMMA = 0xFF0C;

  /** {@code '－'} */
  public static final Character FULLWIDTH_HYPHEN_MINUS = 0xFF0D;

  /** {@code '．'} */
  public static final Character FULLWIDTH_FULL_STOP = 0xFF0E;

  /** {@code '／'} */
  public static final Character FULLWIDTH_SOLIDUS = 0xFF0F;

  /** {@code '：'} */
  public static final Character FULLWIDTH_COLON = 0xFF1A;

  /** {@code '；'} */
  public static final Character FULLWIDTH_SEMICOLON = 0xFF1B;

  /** {@code '＜'} */
  public static final Character FULLWIDTH_LESS_THAN_SIGN = 0xFF1C;

  /** {@code '＝'} */
  public static final Character FULLWIDTH_EQUALS_SIGN = 0xFF1D;

  /** {@code '＞'} */
  public static final Character FULLWIDTH_GREATER_THAN_SIGN = 0xFF1E;

  /** {@code '？'} */
  public static final Character FULLWIDTH_QUESTION_MARK = 0xFF1F;

  /** {@code '＠'} */
  public static final Character FULLWIDTH_COMMERCIAL_AT = 0xFF20;

  /** {@code '［'} */
  public static final Character FULLWIDTH_LEFT_SQUARE_BRACKET = 0xFF3B;

  /** {@code '＼'} */
  public static final Character FULLWIDTH_REVERSE_SOLIDUS = 0xFF3C;

  /** {@code '］'} */
  public static final Character FULLWIDTH_RIGHT_SQUARE_BRACKET = 0xFF3D;

  /** {@code '＾'} */
  public static final Character FULLWIDTH_CIRCUMFLEX_ACCENT = 0xFF3E;

  /** {@code '＿'} */
  public static final Character FULLWIDTH_LOW_LINE = 0xFF3F;

  /** {@code '｀'} */
  public static final Character FULLWIDTH_GRAVE_ACCENT = 0xFF40;

  /** {@code '｛'} */
  public static final Character FULLWIDTH_LEFT_CURLY_BRACKET = 0xFF5B;

  /** {@code '｜'} */
  public static final Character FULLWIDTH_VERTICAL_LINE = 0xFF5C;

  /** {@code '｝'} */
  public static final Character FULLWIDTH_RIGHT_CURLY_BRACKET = 0xFF5D;

  /** {@code '～'} */
  public static final Character FULLWIDTH_TILDE = 0xFF5E;

  /** All fullwidth punctuation char array */
  public static final char[] FULLWIDTH_PUNCTUATION = {
      FULLWIDTH_EXCLAMATION_MARK, FULLWIDTH_QUOTATION_MARK, FULLWIDTH_NUMBER_SIGN,
      FULLWIDTH_DOLLAR_SIGN, FULLWIDTH_PERCENT_SIGN, FULLWIDTH_AMPERSAND, FULLWIDTH_APOSTROPHE,
      FULLWIDTH_LEFT_PARENTHESIS, FULLWIDTH_RIGHT_PARENTHESIS, FULLWIDTH_ASTERISK, FULLWIDTH_PLUS,
      FULLWIDTH_COMMA, FULLWIDTH_HYPHEN_MINUS, FULLWIDTH_FULL_STOP, FULLWIDTH_SOLIDUS,
      FULLWIDTH_COLON, FULLWIDTH_SEMICOLON, FULLWIDTH_LESS_THAN_SIGN, FULLWIDTH_EQUALS_SIGN,
      FULLWIDTH_GREATER_THAN_SIGN, FULLWIDTH_QUESTION_MARK, FULLWIDTH_COMMERCIAL_AT,
      FULLWIDTH_LEFT_SQUARE_BRACKET, FULLWIDTH_REVERSE_SOLIDUS, FULLWIDTH_RIGHT_SQUARE_BRACKET,
      FULLWIDTH_CIRCUMFLEX_ACCENT, FULLWIDTH_LOW_LINE, FULLWIDTH_GRAVE_ACCENT,
      FULLWIDTH_LEFT_CURLY_BRACKET, FULLWIDTH_VERTICAL_LINE, FULLWIDTH_RIGHT_CURLY_BRACKET,
      FULLWIDTH_TILDE
  };

  /**
   * Charset decode
   *
   * @param charset charset, e.g. {@code StandardCharsets.UTF_8}.
   * @param data byte array.
   * @return character buffer object. If encoding exception capacity is 0.
   * @see Charset
   * @see StandardCharsets
   */
  public static CharBuffer charsetDecode(Charset charset, byte[] data) {
    try {
      return charset.decode(ByteBuffer.wrap(data));
    } catch (RuntimeException ignored) {
    }
    return CharBuffer.allocate(0);
  }

  /**
   * Charset encode
   *
   * @param charset charset, e.g. {@code StandardCharsets.UTF_8}.
   * @param data character buffer.
   * @return byte array. if encoding exception {@link Environment#EMPTY_BYTE_ARRAY}.
   * @see Charset
   * @see StandardCharsets
   */
  public static byte[] charsetEncode(Charset charset, CharBuffer data) {
    try {
      return charset.encode(data).array();
    } catch (RuntimeException ignored) {
    }
    return Environment.EMPTY_BYTE_ARRAY;
  }

  /**
   * Determines if the specified character is a digit.
   *
   * <p>Some Unicode character ranges that contain digits:
   *
   * <ul>
   *   <li>{@code '\u005Cu0030'} through {@code '\u005Cu0039'}, ISO-LATIN-1 digits ({@code '0'}
   *       through {@code '9'})
   *   <li>{@code '\u005CuFF10'} through {@code '\u005CuFF19'}, Fullwidth digits ({@code '０'}
   *       through {@code '９'})
   * </ul>
   *
   * @param ch the character to be tested.
   * @return true, if the character is a digit; false, otherwise.
   */
  public static boolean isDigit(char ch) {
    return NumberUtil.between(ch, DIGIT_ZERO, DIGIT_NINE) || NumberUtil.between(ch, FULLWIDTH_DIGIT_ZERO, FULLWIDTH_DIGIT_NINE);
  }

  /**
   * Determines if the specified character is a full stop.
   *
   * <ul>
   *   <li>{@code '\u005Cu002E'}, ISO-LATIN-1 full stop ({@code '.'}
   *   <li>{@code '\u005CuFF0E'}, Fullwidth digits ({@code '．'}
   * </ul>
   *
   * @param ch the character to be tested.
   * @return true, if the character is a digit; false, otherwise.
   */
  public static boolean isFullStop(char ch) {
    return ch == FULL_STOP || ch == FULLWIDTH_FULL_STOP;
  }

  /**
   * Find character index in character array.
   *
   * @param c character.
   * @param chars character array.
   * @return char in char array index, if -1 is not found.
   */
  public static int findIndex(char c, char[] chars) {
    if (NumberUtil.between(c, chars[0], chars[chars.length - 1])) {
      for (int i = 0; i < chars.length; i++) {
        if (c == chars[i]) {
          return i;
        }
      }
    }
    return -1;
  }

  /**
   * Find character index in character sequence.
   *
   * @param c character.
   * @param cs CharSequence.
   * @return char in CharSequence index, if -1 is not found.
   */
  public static int findIndex(char c, CharSequence cs) {
    if (NumberUtil.between(c, cs.charAt(0), cs.charAt(cs.length() -1))) {
      for (int i = 0; i < cs.length(); i++) {
        if (c == cs.charAt(i)) {
          return i;
        }
      }
    }
    return -1;
  }
}
