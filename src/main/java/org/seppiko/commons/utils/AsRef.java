/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * A simple implement like {@link java.util.Optional}
 *
 * @param <T> the type of value.
 * @see java.util.Optional
 * @author Leonard Woo
 */
public class AsRef<T> {

  /**
   * If non-null, the value; if null, indicates no value is present
   */
  private final T value;

  /**
   * Constructs an instance with the described value.
   *
   * @param value the value to describe
   */
  private AsRef(T value) {
    this.value = value;
  }

  /**
   * Returns an empty {@code AsRef} instance.
   *
   * @return an empty {@code AsRef}
   * @param <T> The type of the non-existent value
   */
  public static <T> AsRef<T> empty() {
    return new AsRef<>(null);
  }

  /**
   * Returns a {@code AsRef} describing the given value.
   *
   * @param value the value to describe
   * @return an {@code AsRef} with the value present
   * @param <T> the type of the value
   */
  public static <T> AsRef<T> of(T value) {
    return new AsRef<>(value);
  }

  /**
   * If a value is present, returns the value, otherwise throws {@code NoSuchElementException}.
   *
   * <p>
   * The preferred alternative to this method is {@link #orElseThrow()}.
   * </p>
   *
   * @return the non-{@code null} value described by this {@code AsRef}
   * @throws NoSuchElementException if no value is present
   */
  public T get() throws NoSuchElementException {
    return orElseThrow();
  }

  /**
   * If a value is present, returns the value, otherwise returns {@code other}.
   *
   * @param other the value to be returned, if no value is present
   * @return the value, if present, otherwise {@code other}
   */
  public T orElse(T other) {
    return isNull()? other: value;
  }

  /**
   * If a value is present, returns the value, otherwise returns the result produced by the
   * supplying function.
   *
   * @param supplier the supplying function that produces a value to be returned
   * @return the value, if present, otherwise the result produced by the supplying function
   * @throws NullPointerException if no value is present and the supplying function is {@code null}
   */
  public T orElse(Supplier<? extends T> supplier) throws NullPointerException {
    return isNull()? supplier.get(): value;
  }

  /**
   * If a value is present, returns the value, otherwise throws {@code NoSuchElementException}.
   *
   * @return the non-{@code null} value described by this {@code AsRef}
   * @throws NoSuchElementException if no value is present
   */
  public T orElseThrow() throws NoSuchElementException {
    if (isNull()) {
      throw new NoSuchElementException("No value present");
    }
    return value;
  }

  /**
   * If a value is present, returns the value, otherwise throws an exception
   * produced by the exception supplying function.
   *
   * <p>A method reference to the exception constructor with an empty argument
   * list can be used as the supplier. For example,
   * {@code IllegalStateException::new}</p>
   *
   * @param <X> Type of the exception to be thrown
   * @param exceptionSupplier the supplying function that produces an exception to be thrown
   * @return the value, if present
   * @throws X if no value is present
   * @throws NullPointerException if no value is present and the exception supplying function is
   *   {@code null}
   */
  public <X extends Throwable> T orElseThrow(Supplier<? extends X> exceptionSupplier)
      throws X, NullPointerException {
    if (isPresent()) {
      return value;
    } else {
      throw exceptionSupplier.get();
    }
  }

  /**
   * If a value is present, returns {@code true}, otherwise {@code false}.
   *
   * @return {@code true} if a value is present, otherwise {@code false}
   */
  public boolean isPresent() {
    return Objects.nonNull(value);
  }

  /**
   * If a value is not present, returns {@code true}, otherwise
   * {@code false}.
   *
   * @return  {@code true} if a value is not present, otherwise {@code false}
   */
  public boolean isNull() {
    return Objects.isNull(value);
  }

  /**
   * If a value is present, performs the given action with the value,
   * otherwise does nothing.
   *
   * @param action the action to be performed, if a value is present
   * @throws NullPointerException if value is present and the given action is
   *         {@code null}
   */
  public void ifPresent(Consumer<? super T> action) throws NullPointerException {
    if (isPresent()) {
      action.accept(value);
    }
  }

  /**
   * If a value is present, and the value matches the given predicate,
   * returns an {@code AsRef} describing the value, otherwise returns an
   * empty {@code AsRef}.
   *
   * @param predicate the predicate to apply to a value, if present
   * @return an {@code AsRef} describing the value of this
   *         {@code AsRef}, if a value is present and the value matches the
   *         given predicate, otherwise an empty {@code AsRef}
   * @throws NullPointerException if the predicate is {@code null}
   */
  public AsRef<T> filter(Predicate<? super T> predicate) throws NullPointerException {
    Objects.requireNonNull(predicate);
    return (isNull() || predicate.test(value)) ? this : empty();
  }

  /**
   * If a value is present, returns an {@code AsRef} describing (as if by
   * {@link #of}) the result of applying the given mapping function to
   * the value, otherwise returns an empty {@code AsRef}.
   *
   * <p>If the mapping function returns a {@code null} result then this method
   * returns an empty {@code AsRef}.
   *
   * <pre>{@code
   *     AsRef<Path> p =
   *         uris.stream().filter(uri -> !isProcessedYet(uri))
   *                       .findFirst()
   *                       .map(Paths::get);
   * }</pre>
   *
   * <p>Here, {@code findFirst} returns an {@code AsRef<URI>}, and then
   * {@code map} returns an {@code AsRef<Path>} for the desired
   * URI if one exists.
   *
   * @param mapper the mapping function to apply to a value, if present
   * @param <U> The type of the value returned from the mapping function
   * @return an {@code AsRef} describing the result of applying a mapping
   *         function to the value of this {@code AsRef}, if a value is
   *         present, otherwise an empty {@code AsRef}
   * @throws NullPointerException if the mapping function is {@code null}
   */
  public <U> AsRef<U> map(Function<? super T, ? extends U> mapper) throws NullPointerException {
    Objects.requireNonNull(mapper);
    return isNull()? empty(): of(mapper.apply(value));
  }

  /**
   * If a value is present, returns a sequential {@link Stream} containing
   * only that value, otherwise returns an empty {@code Stream}.
   *
   * <p>This method can be used to transform a {@code Stream} of AsRef
   * elements to a {@code Stream} of present value elements:
   *
   * <pre>{@code
   *     Stream<AsRef<T>> os = ..
   *     Stream<T> s = os.flatMap(AsRef::stream)
   * }</pre>
   *
   * @return the AsRef value as a {@code Stream}
   */
  public Stream<T> stream() {
    return isNull()? Stream.empty(): Stream.of(value);
  }

  /**
   * Indicates whether some other object is "equal to" this {@code AsRef}.
   * The other object is considered equal if:
   * <ul>
   * <li>it is also an {@code AsRef} and;
   * <li>both instances have no value present or;
   * <li>the present values are "equal to" each other via {@code equals()}.
   * </ul>
   *
   * @param obj an object to be tested for equality
   * @return {@code true} if the other object is "equal to" this object
   *         otherwise {@code false}
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    return (obj instanceof AsRef<?> asRef) &&
        Objects.equals(value, asRef.value);
  }

  /**
   * Returns the hash code of the value.
   * If present, otherwise {@code 0} (zero) if no value is present.
   *
   * @return hash code value of the present value or {@code 0} if no value is present
   */
  @Override
  public int hashCode() {
    return Objects.hashCode(value);
  }

  /**
   * Returns a non-empty string representation of this {@code AsRef} suitable for debugging.
   * The exact presentation format is unspecified and may vary between implementations and versions.
   *
   * @return the string representation of this instance
   */
  @Override
  public String toString() {
    return isNull() ?
        "AsRef.empty" :
        ("AsRef[" + value + "]");
  }
}
