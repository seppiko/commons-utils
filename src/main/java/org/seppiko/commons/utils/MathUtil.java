/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * Math utility
 *
 * @author Leonard Woo
 */
public class MathUtil {

  private MathUtil() {}

  /**
   * BigDecimal list max number
   *
   * @param nums number list
   * @return maximum
   * @throws NoSuchElementException list is null
   */
  public static BigDecimal maxDecimal(List<BigDecimal> nums) throws NoSuchElementException {
    return nums.stream()
        .filter(Objects::nonNull)
        .max(Comparator.comparing(n -> n))
        .orElseThrow();
  }

  /**
   * BigInteger list max number
   *
   * @param nums number list
   * @return maximum
   * @throws NoSuchElementException list is null
   */
  public static BigInteger maxInteger(List<BigInteger> nums) throws NoSuchElementException {
    return nums.stream()
        .filter(Objects::nonNull)
        .max(Comparator.comparing(n -> n))
        .orElseThrow();
  }

  /**
   * BigDecimal list min number
   *
   * @param nums number list
   * @return minimum
   * @throws NoSuchElementException list is null
   */
  public static BigDecimal minDecimal(List<BigDecimal> nums) throws NoSuchElementException {
    return nums.stream()
        .filter(Objects::nonNull)
        .min(Comparator.comparing(n -> n))
        .orElseThrow();
  }

  /**
   * BigInteger list min number
   *
   * @param nums number list
   * @return minimum
   * @throws NoSuchElementException list is null
   */
  public static BigInteger minInteger(List<BigInteger> nums) throws NoSuchElementException {
    return nums.stream()
        .filter(Objects::nonNull)
        .min(Comparator.comparing(n -> n))
        .orElseThrow();
  }

  /**
   * BigDecimal list avg number
   *
   * @param nums number list
   * @param roundingMode {@link RoundingMode}
   * @return average
   */
  public static BigDecimal avgDecimal(List<BigDecimal> nums, RoundingMode roundingMode) {
    return nums.stream()
        .map(Objects::requireNonNull)
        .reduce(BigDecimal.ZERO, BigDecimal::add)
        .divide(BigDecimal.valueOf(nums.size()), roundingMode);
  }

  /**
   * BigDecimal list avg number
   *
   * @param nums number list
   * @return average double
   * @throws NoSuchElementException list is null
   */
  public static double avgDecimal(List<BigDecimal> nums) throws NoSuchElementException {
    return nums.stream()
        .map(Objects::requireNonNull)
        .mapToDouble(BigDecimal::doubleValue)
        .average()
        .orElseThrow();
  }

  /**
   * BigInteger list avg number
   *
   * @param nums number list
   * @param roundingMode {@link RoundingMode}
   * @return average
   */
  public static BigDecimal avgInteger(List<BigInteger> nums, RoundingMode roundingMode) {
    BigInteger sum = nums.stream()
            .map(Objects::requireNonNull)
            .reduce(BigInteger.ZERO, BigInteger::add);

    return divide(sum, BigDecimal.valueOf(nums.size()), roundingMode);
  }

  /**
   * Returns the value of the first argument raised to the power of the second argument.
   *
   * @param a the base.
   * @param b the exponent.
   * @return the value {@code a}<sup>{@code b}</sup>.
   */
  public static BigInteger pow(int a, int b) {
    return pow(BigInteger.valueOf(a), b);
  }

  /**
   * Returns the value of the first argument raised to the power of the second argument.
   *
   * @param a the base.
   * @param b the exponent.
   * @return the value {@code a}<sup>{@code b}</sup>.
   */
  public static BigInteger pow(long a, int b) {
    return pow(BigInteger.valueOf(a), b);
  }

  /**
   * Returns the value of the first argument raised to the power of the second argument.
   *
   * @param a the base.
   * @param b the exponent.
   * @return the value {@code a}<sup>{@code b}</sup>.
   */
  public static BigInteger pow(BigInteger a, int b) {
    return a.pow(b);
  }

  /**
   * Returns the value of the first argument raised to the power of the second argument.
   *
   * @param a the base.
   * @param b the exponent.
   * @return the value {@code a}<sup>{@code b}</sup>.
   */
  public static BigDecimal pow(double a, int b) {
    return pow(BigDecimal.valueOf(a), b);
  }

  /**
   * Returns the value of the first argument raised to the power of the second argument.
   *
   * @param a the base.
   * @param b the exponent.
   * @return the value {@code a}<sup>{@code b}</sup>.
   */
  public static BigDecimal pow(BigDecimal a, int b) {
    return a.pow(b);
  }

  /**
   * Returns a {@code BigDecimal} whose value is (a / b).
   * If rounding must be performed to generate a result with the given scale,
   * the specified rounding mode is applied.
   *
   * @param a the divisor.
   * @param b the dividend.
   * @param roundingMode rounding mode
   * @return the value is {@code a/b} with round.
   */
  public static BigDecimal divide(long a, long b, RoundingMode roundingMode) {
    return divide(BigDecimal.valueOf(a), BigDecimal.valueOf(b), roundingMode);
  }

  /**
   * Returns a {@code BigDecimal} whose value is (a / b).
   * If rounding must be performed to generate a result with the given scale,
   * the specified rounding mode is applied.
   *
   * @param a the divisor.
   * @param b the dividend.
   * @param roundingMode rounding mode
   * @return the value is {@code a/b} with round.
   */
  public static BigDecimal divide(double a, double b, RoundingMode roundingMode) {
    return divide(BigDecimal.valueOf(a), BigDecimal.valueOf(b), roundingMode);
  }

  /**
   * Returns a {@code BigDecimal} whose value is (a / b).
   * If rounding must be performed to generate a result with the given scale,
   * the specified rounding mode is applied.
   *
   * @param a the divisor.
   * @param b the dividend.
   * @param roundingMode rounding mode
   * @return the value is {@code a/b} with round.
   */
  public static BigDecimal divide(BigInteger a, BigDecimal b, RoundingMode roundingMode) {
    return divide(new BigDecimal(a), b, roundingMode);
  }

  /**
   * Returns a {@code BigDecimal} whose value is (a / b).
   * If rounding must be performed to generate a result with the given scale,
   * the specified rounding mode is applied.
   *
   * @param a the divisor.
   * @param b the dividend.
   * @param roundingMode rounding mode
   * @return the value is {@code a/b} with round.
   */
  public static BigDecimal divide(BigDecimal a, BigDecimal b, RoundingMode roundingMode) {
    return a.divide(b, roundingMode);
  }

  /**
   * Convert decimal to alphabet number
   *
   * @param num number (greater than 0)
   * @param alphabet alphabet
   * @return alphabet base, if num lass than 1 will return null
   */
  public static String convertDecimalTo(final long num, char[] alphabet) {
    if (num <= 0) {
      return null;
    }
    StringBuilder s = new StringBuilder();
    final int length = alphabet.length;
    long n = num;
    while (n > 0) {
      long m = n % length;
      if (m == 0) {
        m = length;
      }
      s.insert(0, alphabet[(int) (m - 1L)]);
      n = (n - m) / length;
    }
    return s.toString();
  }

  /**
   * Convert alphabet number to decimal
   *
   * @param num number
   * @param alphabet alphabet
   * @return if return null than num parser failed
   */
  public static Long convertToDecimal(CharSequence num, char[] alphabet) {
    if (!StringUtil.hasText(num)) {
      return null;
    }
    long n = 0L;
    final int length = alphabet.length;
    final int nl = num.length();
    for (int i = 0; i < nl; i++) {
      int index = CharUtil.findIndex(num.charAt(i), alphabet) + 1;
      int last = nl - i - 1;
      n += BigInteger.valueOf(length).pow(last).multiply(BigInteger.valueOf(index)).longValue();
    }
    return n;
  }
}
