/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.reflect;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;

/**
 * Dynamic proxy handler implementation.
 *
 * @param <T> target object type.
 * @author Leonard Woo
 */
public class DynamicInvocationHandler<T> implements InvocationHandler {

  private final HashMap<String, Method> methods = new HashMap<>();
  private final T target;

  /**
   * constructor
   *
   * @param target proxy target instance.
   */
  public DynamicInvocationHandler(T target) {
    this.target = target;

    for(Method method: target.getClass().getDeclaredMethods()) {
      this.methods.put(method.getName(), method);
    }
  }

  /**
   * Proxy method invoke.
   *
   * @param proxy proxy object.
   * @param method target object method.
   * @param args target method argument.
   * @return target method return.
   * @throws Throwable target method throw.
   */
  @Override
  public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
    return methods.get(method.getName())
        .invoke(target, args);
  }
}
