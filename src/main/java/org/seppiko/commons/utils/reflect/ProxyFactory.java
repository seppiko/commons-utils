/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.reflect;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * Proxy factory.
 *
 * @author Leonard Woo
 */
public class ProxyFactory {

  private ProxyFactory(){}

  /**
   * call interface new instance from proxy.
   *
   * @param loader the class loader to define the proxy class.
   * @param clazz the list of interfaces for the proxy class to implement.
   * @param handler interface method invoke {@link InvocationHandler}.
   *     like {@link DynamicInvocationHandler}.
   * @param <T> interface class type.
   * @return interface method invoke return object.
   * @throws IllegalArgumentException – if any of the restrictions on the parameters are violated.
   * @throws SecurityException – if a security manager, s, is present and any of the following
   *     conditions is met: the given loader is null and the caller's class loader is not null and
   *     the invocation of s.checkPermission with RuntimePermission("getClassLoader") permission
   *     denies access; for each proxy interface, intf, the caller's class loader is not the same as
   *     or an ancestor of the class loader for intf and invocation of s.checkPackageAccess() denies
   *     access to intf; any of the given proxy interfaces is non-public and the caller class is not
   *     in the same runtime package as the non-public interface and the invocation of
   *     s.checkPermission with ReflectPermission("newProxyInPackage.{package name}") permission
   *     denies access.
   * @throws NullPointerException – if the interfaces array argument or any of its elements are
   *     null, or if the invocation handler, h, is null.
   */
  @SuppressWarnings("unchecked")
  public static <T> T newInstance(ClassLoader loader, Class<T> clazz, InvocationHandler handler)
      throws IllegalArgumentException, SecurityException, NullPointerException {
    return (T) Proxy.newProxyInstance(loader, new Class[] {clazz}, handler);
  }
}
