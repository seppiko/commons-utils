/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.reflect;

import java.io.Serializable;

/**
 * Package for value and type
 *
 * @param type object type class.
 * @param value object value.
 * @param <T> object.
 * @author Leonard Woo
 */
public record TypeAndValue<T>(Class<T> type, T value) implements Serializable {

  /** {@inheritDoc} */
  @Override
  public String toString() {
    return "Value " + value
        + "of type "+ type.getTypeName();
  }
}
