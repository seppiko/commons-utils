/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import org.seppiko.commons.utils.Assert;

/**
 * Class utility
 *
 * @author Leonard Woo
 */
public class ClassUtil {

  /** The package separator character: {@code '.'}. */
  private static final char PACKAGE_SEPARATOR = '.';

  /** The path separator character: {@code '/'}. */
  private static final char PATH_SEPARATOR = '/';

  /** The ".class" file suffix. */
  public static final String CLASS_FILE_SUFFIX = ".class";

  private ClassUtil() {}

  /**
   * Return the default ClassLoader to use: typically the thread context ClassLoader, if available;
   * the ClassLoader that loaded the {@code clazz} class will be used as fallback.
   *
   * <p>Call this method if you intend to use the thread context ClassLoader in a scenario where
   * you clearly prefer a non-null ClassLoader reference: for example, for class path resource
   * loading (but not necessarily for {@code Class.forName}, which accepts a {@code null}
   * ClassLoader reference as well).</p>
   *
   * @param clazz fallback class. {@code null} if not have fallback class.
   * @return the default ClassLoader (only {@code null} if even the system ClassLoader isn't
   * accessible).
   * @see Thread#getContextClassLoader()
   * @see ClassLoader#getSystemClassLoader()
   */
  public static ClassLoader getDefaultClassLoader(Class<?> clazz) {
    ClassLoader cl = null;

    try {
      cl = Thread.currentThread().getContextClassLoader();
    } catch (Throwable t) {
      // Cannot access thread context ClassLoader - falling back...
    }

    if (cl == null && clazz != null) {
      // No thread context class loader -> use class loader of this class.
      cl = clazz.getClassLoader();
    }

    if (cl == null) {
      // getClassLoader() returning null indicates the bootstrap ClassLoader
      try {
        cl = ClassLoader.getSystemClassLoader();
      } catch (Throwable t) {
        // Cannot access system ClassLoader - oh well, maybe the caller can live with null...
      }
    }

    return cl;
  }

  /**
   * Return the class declared instance without parameters.
   *
   * @param clazz target class.
   * @param <T> target class type.
   * @return class instance.
   * @throws NoSuchMethodException not found constructor method.
   * @throws IllegalAccessException can not access constructor method.
   * @throws InvocationTargetException can not call constructor method.
   * @throws InstantiationException can not instance.
   * @throws SecurityException access failed.
   */
  public static <T> T newInstance(Class<T> clazz)
      throws NoSuchMethodException, IllegalAccessException, InvocationTargetException,
      InstantiationException, SecurityException {
    return getConstructor(clazz).newInstance();
  }

  /**
   * Return the class declared instance with parameters.
   *
   * @param clazz target class.
   * @param initArgs class constructor parameter objects.
   * @param <T> target class type.
   * @return class instance.
   * @throws NoSuchMethodException not found constructor method.
   * @throws IllegalAccessException can not access constructor method.
   * @throws InvocationTargetException can not call constructor method.
   * @throws InstantiationException can not instance.
   * @throws SecurityException access failed.
   */
  public static <T> T newInstance(Class<T> clazz, Object... initArgs)
      throws NoSuchMethodException, IllegalAccessException, InvocationTargetException,
      InstantiationException, SecurityException {
    Class<?>[] paramTypes = TypeUtil.getTypes(initArgs);
    return newInstance(clazz, paramTypes, initArgs);
  }

  /**
   * Return the class declared instance with parameters and type.
   *
   * <p>If {@code argsType} is {@code null} or {@code initArgs} is {@code null},
   * call {@link #newInstance(Class)}.
   *
   * @param clazz target class.
   * @param argsType class constructor parameter types.
   * @param initArgs class constructor parameter objects.
   * @param <T> target class type.
   * @return class instance.
   * @throws NoSuchMethodException not found constructor method.
   * @throws IllegalAccessException can not access constructor method.
   * @throws InvocationTargetException can not call constructor method.
   * @throws InstantiationException can not instance.
   * @throws IllegalCallerException types length not equal args length.
   * @throws SecurityException access failed.
   */
  public static <T> T newInstance(Class<T> clazz, Class<?>[] argsType, Object[] initArgs)
      throws NoSuchMethodException, IllegalAccessException, InvocationTargetException,
      InstantiationException, IllegalCallerException, SecurityException {
    if (argsType == null || initArgs == null) {
      return newInstance(clazz);
    }
    if (argsType.length != initArgs.length) {
      throw new IllegalCallerException("arguments type length must equal arguments length");
    }
    return getConstructor(clazz, argsType).newInstance(initArgs);
  }

  /**
   * Return the class instance to use: typically the public constructor with no-args method, if available;
   * Retry the public declared constructor with no-args method, if available;
   *
   * <p>If need parameters use {@link #newInstance(Class, Object...)} or
   * {@link #newInstance(Class, Class[], Object[])}
   *
   * @param clazz class.
   * @return class instance.
   * @param <T> class type.
   * @throws NoSuchMethodException not found class no-args constructor.
   * @throws IllegalAccessException retry get class instance failed.
   * @throws IllegalArgumentException class is null.
   */
  public static <T> T getInstance(Class<T> clazz)
      throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException {
    Assert.notNull(clazz, "clazz must not be null");
    T target;
    try {
      // Try to get target class instance with no-args constructor
      target = clazz.getConstructor().newInstance();
    } catch (ReflectiveOperationException | RuntimeException e) {
      // Retry get the class instance maybe no public with no-args constructor
      try {
        target = newInstance(clazz);
      } catch (NoSuchMethodException ex) {
        throw ex;
      } catch (ReflectiveOperationException ex) {
        throw new IllegalAccessException("Retry get class " + clazz.getName() + " instance failed: " + ex.getMessage());
      }
    }
    return target;
  }

  /**
   * Return the class declared constructor with parameter types.
   *
   * @param clazz target class.
   * @param parameterTypes class constructor parameter types.
   *   If not parameter set {@link ReflectionUtil#EMPTY_CLASS_ARRAY}.
   * @param <T> target class type.
   * @return class instance.
   * @throws NoSuchMethodException not found constructor method.
   * @throws SecurityException access failed.
   */
  public static <T> Constructor<T> getConstructor(Class<T> clazz, Class<?> ...parameterTypes)
      throws NoSuchMethodException, SecurityException {
    return clazz.getDeclaredConstructor(parameterTypes);
  }

  /**
   * Return constructor modifier name.
   *
   * @param constructor class constructor.
   * @return class modifier name.
   */
  public static String getConstructorModifierName(Constructor<?> constructor) {
    return Modifier.toString(constructor.getModifiers());
  }

  /**
   * Convert a "/"-based resource path to a "."-based fully qualified class name.
   *
   * @param resourcePath the resource path pointing to a class
   * @return the corresponding fully qualified class name
   */
  public static String convertResourcePathToClassName(String resourcePath) {
    Assert.notNull(resourcePath, "Resource path must not be null");
    return resourcePath.replace(PATH_SEPARATOR, PACKAGE_SEPARATOR);
  }

  /**
   * Convert a "."-based fully qualified class name to a "/"-based resource path.
   *
   * @param className the fully qualified class name
   * @return the corresponding resource path, pointing to the class
   */
  public static String convertClassNameToResourcePath(String className) {
    Assert.notNull(className, "Class name must not be null");
    return className.replace(PACKAGE_SEPARATOR, PATH_SEPARATOR);
  }

  /**
   * Determine the name of the class file, relative to the containing
   * package: e.g. "String.class"
   *
   * @param clazz the class
   * @return the file name of the ".class" file
   */
  public static String getClassFileName(Class<?> clazz) {
    Assert.notNull(clazz, "Class must not be null");
    String className = clazz.getName();
    int lastDotIndex = className.lastIndexOf(PACKAGE_SEPARATOR);
    return className.substring(lastDotIndex + 1) + CLASS_FILE_SUFFIX;
  }

}
