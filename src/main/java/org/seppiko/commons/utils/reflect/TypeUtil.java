/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.reflect;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Objects;
import java.util.function.Supplier;
import org.seppiko.commons.utils.ArrayUtil;

/**
 * Type utility
 *
 * @author Leonard Woo
 */
public class TypeUtil {

  private TypeUtil(){}

  /**
   * Return the objects type.
   *
   * @param objs object array.
   * @return objects type.
   */
  public static Class<?>[] getTypes(Object[] objs) {
    if (ArrayUtil.isEmpty(objs)) {
      return null;
    }

    Class<?>[] types = new Class[objs.length];
    for (int i = 0; i < objs.length; i++) {
      Object obj = objs[i];
      types[i] = (obj == null ? null : obj.getClass());
    }

    return types;
  }

  /**
   * Return the type boxing class
   *
   * @param type type class
   * @return boxing class
   */
  public static Class<?> wrapper(Class<?> type) {
    if (Objects.isNull(type)) {
      throw new NullPointerException("Type must be not null");
    }
    if (type.isPrimitive()) {
      if (boolean.class == type) {
        return Boolean.class;
      }
      if (byte.class == type) {
        return Byte.class;
      }
      if (short.class == type) {
        return Short.class;
      }
      if (int.class == type) {
        return Integer.class;
      }
      if (long.class == type) {
        return Long.class;
      }
      if (float.class == type) {
        return Float.class;
      }
      if (double.class == type) {
        return Double.class;
      }
      if (char.class == type) {
        return Character.class;
      }
    }
    return type;
  }

  /**
   * Return the class unboxing type
   *
   * @param type type class
   * @return unboxing class
   */
  public static Class<?> unwrap(Class<?> type) {
    if (Objects.isNull(type)) {
      throw new NullPointerException("Type must be not null");
    }
    if (type == Boolean.class) {
      return boolean.class;
    }
    if (type == Byte.class) {
      return byte.class;
    }
    if (type == Short.class) {
      return short.class;
    }
    if (type == Integer.class) {
      return int.class;
    }
    if (type == Long.class) {
      return long.class;
    }
    if (type == Float.class) {
      return float.class;
    }
    if (type == Double.class) {
      return double.class;
    }
    if (type == Character.class) {
      return char.class;
    }
    return type;
  }


  /**
   * Parser number type.
   *
   * @param number number.
   * @return number type and value.
   */
  public static TypeAndValue<?> parserNumberType(Number number) {
    if (number instanceof Byte) {
      return new TypeAndValue<>(Byte.class, number.byteValue());
    }
    if (number instanceof Short) {
      return new TypeAndValue<>(Short.class, number.shortValue());
    }
    if (number instanceof Integer) {
      return new TypeAndValue<>(Integer.class, number.intValue());
    }
    if (number instanceof Long) {
      return new TypeAndValue<>(Long.class, number.longValue());
    }
    if (number instanceof Float) {
      return new TypeAndValue<>(Float.class, number.floatValue());
    }
    if (number instanceof Double) {
      return new TypeAndValue<>(Double.class, number.doubleValue());
    }
    if (number instanceof BigInteger) {
      return new TypeAndValue<>(BigInteger.class, (BigInteger) number);
    }
    if (number instanceof BigDecimal) {
      return new TypeAndValue<>(BigDecimal.class, (BigDecimal) number);
    }
    throw new IllegalArgumentException("Not found number " + number + " of type " + number.getClass());
  }

  /**
   * Return the object instance.
   *
   * @param obj object.
   * @param type object type class.
   * @param <T> the object type.
   * @return the object instance.
   * @throws NullPointerException object or type is {@code null}.
   */
  public static <T> T fromObject(Object obj, Class<T> type)
      throws NullPointerException {
    return Objects.requireNonNull(type).cast(Objects.requireNonNull(obj));
  }
}
