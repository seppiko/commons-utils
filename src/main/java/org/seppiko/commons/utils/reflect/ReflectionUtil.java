/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.reflect;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InaccessibleObjectException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.lang.reflect.RecordComponent;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.ServiceLoader;
import org.seppiko.commons.utils.ArrayUtil;
import org.seppiko.commons.utils.Assert;
import org.seppiko.commons.utils.Environment;
import org.seppiko.commons.utils.internal.DeclaredReflection;

/**
 * Reflection utility
 *
 * @author Leonard Woo
 */
public class ReflectionUtil {

  private ReflectionUtil(){}

  /** Empty class array with wildcard */
  public static final Class<?>[] EMPTY_CLASS_ARRAY = new Class<?>[0];

  /** Empty constructor array with wildcard */
  public static final Constructor<?>[] EMPTY_CONSTRUCTOR_ARRAY = new Constructor[0];

  /** Empty method array */
  public static final Method[] EMPTY_METHOD_ARRAY = new Method[0];

  /** Empty parameter array */
  public static final Parameter[] EMPTY_PARAMETER_ARRAY = new Parameter[0];

  /** Empty field array */
  public static final Field[] EMPTY_FIELD_ARRAY = new Field[0];

  /**
   * Find declared method.
   *
   * @param clazz method class.
   * @param methodName method name.
   * @param <T> class type.
   * @return method object.
   * @throws NoSuchMethodException not found method.
   * @throws SecurityException access failed.
   */
  public static <T> Method findMethod(Class<T> clazz, String methodName)
      throws NoSuchMethodException, SecurityException {
    return findDeclaredMethod(clazz, methodName);
  }

  /**
   * Find a declared method with parameter types.
   *
   * @param clazz method class.
   * @param methodName method name.
   * @param parameterTypes parameter types.
   * @param <T> class type.
   * @return method object.
   * @throws NoSuchMethodException if method not found.
   * @throws NullPointerException if method is null.
   * @throws SecurityException access failed.
   */
  public static <T> Method findDeclaredMethod(Class<T> clazz, String methodName, Class<?>... parameterTypes)
      throws NoSuchMethodException, NullPointerException, SecurityException {
    return clazz.getDeclaredMethod(methodName, parameterTypes);
  }

  /**
   * Return the class instance method caller.
   *
   * @param t target class instance.
   * @param methodName call method name.
   * @param args parameter objects. Can not use basic data types, cause nonsupport auto unboxing.
   * @param <T> target class.
   * @return method return object.
   * @throws ReflectiveOperationException not found method or can not access method.
   * @throws RuntimeException if method or parameter exception.
   */
  public static <T> Object invokeMethod(T t, String methodName, Object... args)
      throws ReflectiveOperationException, RuntimeException {
    return invokeMethod(t, methodName, TypeUtil.getTypes(args), args);
  }

  /**
   * Return the class instance method caller.
   *
   * @param t target class instance.
   * @param methodName call method name.
   * @param parameterTypes parameter types.
   * @param args parameter objects. Can not use basic data types, cause nonsupport auto unboxing.
   * @param <T> target class.
   * @return method return object.
   * @throws ReflectiveOperationException not found method or can not access method.
   * @throws RuntimeException if method or parameter exception.
   */
  public static <T> Object invokeMethod(
      T t, String methodName, Class<?>[] parameterTypes, Object... args)
      throws ReflectiveOperationException, RuntimeException {
    Method method = findDeclaredMethod(t.getClass(), methodName, parameterTypes);
    return invokeMethod(method, t, args);
  }

  /**
   * Invoke method on target instance.
   *
   * @param method target method.
   * @param target target class instance.
   * @param args method parameters.
   * @return method return object (if exist).
   * @throws InvocationTargetException method throws an exception.
   * @throws IllegalAccessException method can not access.
   * @throws IllegalArgumentException method parameter type or size exception.
   */
  public static Object invokeMethod(Method method, Object target, Object... args)
      throws InvocationTargetException, IllegalAccessException, IllegalArgumentException {
    return method.invoke(target, args);
  }

  /**
   * invoke method without parameter.
   *
   * @param method target method.
   * @param target target class instance.
   * @return method return object (if exist).
   * @throws InvocationTargetException method throws an exception.
   * @throws IllegalAccessException method can not access.
   * @throws IllegalArgumentException method parameter type or size exception.
   */
  public static Object invokeMethod(Method method, Object target)
      throws InvocationTargetException, IllegalAccessException, IllegalArgumentException {
    return invokeMethod(method, target, Environment.EMPTY_OBJECT_ARRAY);
  }

  /**
   * Return methods of this class.
   *
   * @param clazz target class.
   * @return methods.
   * @throws SecurityException can not access method or some else.
   */
  public static Method[] getDeclaredMethods(Class<?> clazz) throws SecurityException {
    Method[] declaredMethods = clazz.getDeclaredMethods();
    return declaredMethods.length == 0? declaredMethods: declaredMethods.clone();
  }

  /**
   * Return methods of this class.
   *
   * @param clazz target class.
   * @return methods.
   * @throws SecurityException can not access method or some else.
   * @throws IllegalAccessException method can not access.
   */
  public static Method[] getAllDeclaredMethods(Class<?> clazz)
      throws SecurityException, IllegalAccessException {
    final List<Method> methods = new ArrayList<>(32);
    DeclaredReflection.doWithMethods(clazz, methods::add);
    return methods.toArray(Method[]::new);
  }

  /**
   * Return a public static method of a class.
   *
   * @param clazz the class which defines the method
   * @param methodName the static method name
   * @param args the parameter types to the method
   * @return the static method, or {@code null} if no static method was found
   * @throws IllegalArgumentException if the method name is blank or the clazz is null
   */
  public static Method getStaticMethod(Class<?> clazz, String methodName, Class<?>... args) {
    Assert.notNull(clazz, "Class must not be null");
    Assert.notNull(methodName, "Method name must not be null");
    try {
      Method method = clazz.getMethod(methodName, args);
      if ( Modifier.isStatic(method.getModifiers()) ) {
        return method;
      }
    } catch (NoSuchMethodException ignored) {
    }
    return null;
  }

  /**
   * Return method modifier name.
   *
   * @param method class method.
   * @return method modifier name.
   */
  public static String getMethodModifierName(Method method) {
    return Modifier.toString(method.getModifiers());
  }

  /**
   * Find the special field with name and type.
   *
   * @param clazz target class.
   * @param name field name.
   * @param type field type.
   * @return field object.
   * @throws NoSuchFieldException not found field.
   */
  public static Field findField(Class<?> clazz, String name, Class<?> type)
      throws NoSuchFieldException {
    Class<?> searchType = clazz;

    final Field[] fields = getDeclaredFields(clazz);
    while (searchType != null && Object.class != searchType) {
      Field field = findField(fields, name, type);
      if (field != null) {
        return field;
      }
      searchType = searchType.getSuperclass();
    }

    throw new NoSuchFieldException(
        "Not found name is [ " + name + " ] and type is [ " + type.getName() + " ] field.");
  }

  /**
   * Find the special field with name and type.
   *
   * @param fields target class fields.
   * @param name field name.
   * @param type field type.
   * @return special field object. if not found the special field return {@code null}.
   * @throws IllegalArgumentException if some param is null or empty.
   */
  private static Field findField(Field[] fields, String name, Class<?> type) {
    Assert.notEmpty(fields, "fields must not be null or empty.");
    Assert.notBlank(name, "field name must not be null or empty.");
    Assert.notNull(type, "field type must not be null.");

    for (Field field : fields) {
      if ( name.equals(field.getName()) && type.equals(field.getType()) ) {
        return field;
      }
    }

    return null;
  }

  /**
   * Return all declared fields of this class.
   *
   * @param clazz target class.
   * @return fields.
   * @throws SecurityException field security.
   */
  public static Field[] getDeclaredFields(Class<?> clazz) throws SecurityException {
    return clazz.getDeclaredFields();
  }

  /**
   * Return record class all declared fields.
   *
   * @param clazz target record class.
   * @return fields.
   * @throws NoSuchFieldException if a field with the specified name is not found.
   * @throws SecurityException field security.
   */
  public static Field[] getRecordFields(Class<?> clazz)
      throws NoSuchFieldException, SecurityException {
    RecordComponent[] recordComponents = clazz.getRecordComponents();
    Field[] fields = new Field[recordComponents.length];

    for (int i = 0; i < recordComponents.length; i++) {
      fields[i] = clazz.getDeclaredField(recordComponents[i].getAccessor().getName());
    }
    return fields;
  }

  /**
   * Return the field value.
   *
   * @param field target class field.
   * @param accessible the field accessible flag.
   * @param target target field instance.
   * @param <T> field instance type.
   * @return target field value.
   * @throws IllegalArgumentException field not exist.
   * @throws IllegalAccessException field access failed.
   * @throws InaccessibleObjectException file cannot enable accessible.
   * @throws NullPointerException field is null.
   * @throws SecurityException field security.
   */
  public static <T> Object getField(Field field, boolean accessible, T target)
      throws IllegalArgumentException, IllegalAccessException, InaccessibleObjectException,
          SecurityException, NullPointerException {
    Objects.requireNonNull(field);
    field.setAccessible(accessible);
    return field.get(target);
  }

  /**
   * Set the field value.
   *
   * @param field target class field.
   * @param accessible the field accessible flag.
   * @param target target field instance.
   * @param value field value.
   * @param <T> target field instance type.
   * @param <V> field value type.
   * @throws NullPointerException field or something is null.
   * @throws IllegalArgumentException field not exist.
   * @throws IllegalAccessException field access failed.
   * @throws InaccessibleObjectException file cannot enable accessible.
   * @throws SecurityException field security.
   */
  public static <T, V> void setField(Field field, boolean accessible, T target, V value)
      throws IllegalArgumentException, IllegalAccessException, InaccessibleObjectException,
          NullPointerException, SecurityException {
    Objects.requireNonNull(field);
    field.setAccessible(accessible);
    field.set(target, value);
  }

  /**
   * Return the class fields.
   *
   * @param clazz target class.
   * @return fields.
   * @throws IllegalStateException if introspection fails
   */
  public static Field[] getAllFields(Class<?> clazz) {
    final List<Field> fields = new ArrayList<>(32);
    DeclaredReflection.doWithFields(clazz, fields::add);
    return fields.toArray(Field[]::new);
  }

  /**
   * Given the source object and the destination, which must be the same class
   * or a subclass, copy all fields, including inherited fields. Designed to
   * work on objects with public no-arg constructors.
   *
   * @param src source object
   * @param dest destination object
   * @throws IllegalStateException if introspection fails
   */
  public static void shallowCopyFieldState(final Object src, final Object dest) {
    Objects.requireNonNull(src, "Source for field copy cannot be null");
    Objects.requireNonNull(dest, "Destination for field copy cannot be null");

    if (!src.getClass().isAssignableFrom(dest.getClass())) {
      throw new IllegalArgumentException("Destination class [" + dest.getClass().getName() +
          "] must be same or subclass as source class [" + src.getClass().getName() + "]");
    }

    DeclaredReflection.doWithFields(src.getClass(), field -> {
      field.setAccessible(true);
      Object srcValue = field.get(src);
      field.set(dest, srcValue);
    }, DeclaredReflection.COPYABLE_FIELDS);
  }

  /**
   * Set record class all field.
   *
   * @param clazz record class.
   * @param accessible the record class constructor accessible flag.
   * @param parameterTypes record parameter types. If args is {@code null}, set this is {@code null}.
   * @param args record parameter values.
   * @return target record class.
   * @param <T> record class.
   * @throws NoSuchMethodException if a matching method is not found.
   * @throws InstantiationException if the class that declares the underlying constructor represents
   *     an abstract class.
   * @throws SecurityException field security.
   * @throws IllegalAccessException if this Constructor object is enforcing Java language access
   *     control and the underlying constructor is inaccessible.
   * @throws IllegalArgumentException if the number of actual and formal parameters differ; if an
   *     unwrapping conversion for primitive arguments fails; or if, after possible unwrapping, a
   *     parameter value cannot be converted to the corresponding formal parameter type by a method
   *     invocation conversion; if this constructor pertains to an enum class.
   * @throws InvocationTargetException if the underlying constructor throws an exception.
   */
  public static <T> T setRecordAllFields(
      Class<T> clazz, boolean accessible, Class<?>[] parameterTypes, Object[] args)
      throws NoSuchMethodException, InstantiationException, SecurityException,
          IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    Constructor<T> constructor = ClassUtil.getConstructor(clazz, parameterTypes);
    constructor.setAccessible(accessible);
    return constructor.newInstance(args);
  }

  /**
   * Return the class field map.
   *
   * @param clazz target record class.
   * @return class field map {@code <FieldName, FieldObject>}.
   */
  public static LinkedHashMap<String, Field> getFieldMap(Class<?> clazz) {
    LinkedHashMap<String, Field> fieldMap = new LinkedHashMap<>();

    RecordComponent[] recordComponents = clazz.getRecordComponents();
    if (!ArrayUtil.isEmpty(recordComponents)) {
      for (RecordComponent recordComponent : recordComponents) {
        String fieldName = recordComponent.getAccessor().getName();
        try {
          fieldMap.put(fieldName, clazz.getDeclaredField(fieldName));
        } catch (NoSuchFieldException e) {
          fieldMap.put(fieldName, null);
        }
      }
    } else {
      Field[] fields = getDeclaredFields(clazz);
      for (Field field : fields) {
        fieldMap.put(field.getName(), field);
      }
    }

    return fieldMap;
  }

  /**
   * Return an annotation object if the class has special annotation.
   *
   * @param clazz the target class.
   * @param annotationClazz annotation class.
   * @return annotation object, if not found the specified annotation type return {@code null}.
   * @param <A> annotation type.
   * @throws NullPointerException if annotation class is {@code null}.
   * @throws IllegalArgumentException if target class is {@code null}.
   */
  public static <A extends Annotation> A getAnnotation(Class<?> clazz, Class<A> annotationClazz) {
    if (clazz == null) {
      throw new IllegalArgumentException();
    }

    if (clazz.isAnnotationPresent(annotationClazz)) {
      return clazz.getAnnotation(annotationClazz);
    }

    return null;
  }

  /**
   * Get service loader.
   *
   * @param serviceClazz service provider interface.
   * @param loader The class loader to be used to load provider-configuration files and
   *     provider classes, or {@code null} if the current thread's
   *    {@linkplain java.lang.Thread#getContextClassLoader context class loader}.
   *    (or, failing that, the bootstrap class loader) is to be used.
   * @return A new service loader.
   * @param <S> service provider type.
   * @see ServiceLoader
   */
  public static <S> ServiceLoader<S> getServiceLoader(Class<S> serviceClazz, ClassLoader loader) {
    if (loader == null) {
      return ServiceLoader.load(serviceClazz);
    }
    return ServiceLoader.load(serviceClazz, loader);
  }

}
