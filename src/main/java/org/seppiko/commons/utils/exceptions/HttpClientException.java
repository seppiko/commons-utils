/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.exceptions;

import java.io.Serial;

/**
 * Http Client Exception
 *
 * @author Leonard Woo
 */
public class HttpClientException extends Exception {

  @Serial private static final long serialVersionUID = 1213027851703893574L;

  /** Default Exception */
  public HttpClientException() {
    super();
  }

  /**
   * Exception with message
   *
   * @param message message
   */
  public HttpClientException(String message) {
    super(message);
  }

  /**
   * Exception with message and Throwable
   *
   * @param message message
   * @param cause other exception
   */
  public HttpClientException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Exception with Throwable
   *
   * @param cause other exception
   */
  public HttpClientException(Throwable cause) {
    super(cause);
  }
}
