/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.exceptions;

import java.io.Serial;

/**
 * HTTP Timeout Exception
 *
 * @author Leonard Woo
 */
public class HttpTimeoutException extends HttpRuntimeException {

  @Serial private static final long serialVersionUID = 4336780200113583992L;

  /** Default Exception */
  public HttpTimeoutException() {
    super();
  }

  /**
   * Exception with message
   *
   * @param message message
   */
  public HttpTimeoutException(String message) {
    super(message);
  }

  /**
   * Exception with message and Throwable
   *
   * @param message message
   * @param cause other exception
   */
  public HttpTimeoutException(String message, Throwable cause) {
    super(message, cause);
  }
}
