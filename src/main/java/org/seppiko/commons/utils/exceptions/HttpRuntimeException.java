/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.exceptions;

import java.io.Serial;

/**
 * HTTP Request Process Exception
 *
 * @author Leonard Woo
 */
public class HttpRuntimeException extends RuntimeException {

  @Serial private static final long serialVersionUID = 698170283934426844L;

  /** Default Exception */
  public HttpRuntimeException() {
    super();
  }

  /**
   * Exception with message
   *
   * @param message message
   */
  public HttpRuntimeException(String message) {
    super(message);
  }

  /**
   * Exception with Throwable
   *
   * @param cause other exception
   */
  public HttpRuntimeException(Throwable cause) {
    super(cause);
  }

  /**
   * Exception with message and Throwable
   *
   * @param message message
   * @param cause other exception
   */
  public HttpRuntimeException(String message, Throwable cause) {
    super(message, cause);
  }
}
