/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * Array utility
 *
 * @see Arrays
 * @author Leonard Woo
 */
public class ArrayUtil {

  private ArrayUtil() {}

  /**
   * Returns {@code true} if the provided reference is array,
   * otherwise returns {@code false}.
   *
   * @param obj a reference to be checked against.
   * @param <T> reference type.
   * @return {@code true} if the provided reference is array, otherwise {@code false}.
   */
  public static <T> boolean isArray(T obj) {
    return Objects.nonNull(obj) && obj.getClass().isArray();
  }

  /**
   * Returns {@code true} if the array is {@code null} or empty,
   * otherwise returns {@code false}.
   *
   * @param array a reference to be checked against.
   * @param <T> reference type.
   * @return {@code true} if the provided array is {@code null}, otherwise {@code false}.
   */
  public static <T> boolean isEmpty(T[] array) {
    return (isNull(array) || array.length == 0);
  }

  /**
   * Returns {@code true} if the array is {@code null} or empty,
   * otherwise returns {@code false}.
   *
   * @param array a reference to be checked against.
   * @return {@code true} if the provided array is {@code null}, otherwise {@code false}.
   */
  public static boolean isEmpty(boolean[] array) {
    return null == array || array.length == 0;
  }

  /**
   * Returns {@code true} if the array is {@code null} or empty,
   * otherwise returns {@code false}.
   *
   * @param array a reference to be checked against.
   * @return {@code true} if the provided array is {@code null}, otherwise {@code false}.
   */
  public static boolean isEmpty(byte[] array) {
    return null == array || array.length == 0;
  }

  /**
   * Returns {@code true} if the array is {@code null} or empty,
   * otherwise returns {@code false}.
   *
   * @param array a reference to be checked against.
   * @return {@code true} if the provided array is {@code null}, otherwise {@code false}.
   */
  public static boolean isEmpty(short[] array) {
    return null == array || array.length == 0;
  }

  /**
   * Returns {@code true} if the array is {@code null} or empty,
   * otherwise returns {@code false}.
   *
   * @param array a reference to be checked against.
   * @return {@code true} if the provided array is {@code null}, otherwise {@code false}.
   */
  public static boolean isEmpty(int[] array) {
    return null == array || array.length == 0;
  }

  /**
   * Returns {@code true} if the array is {@code null} or empty,
   * otherwise returns {@code false}.
   *
   * @param array a reference to be checked against.
   * @return {@code true} if the provided array is {@code null}, otherwise {@code false}.
   */
  public static boolean isEmpty(long[] array) {
    return null == array || array.length == 0;
  }

  /**
   * Returns {@code true} if the array is {@code null} or empty,
   * otherwise returns {@code false}.
   *
   * @param array a reference to be checked against.
   * @return {@code true} if the provided array is {@code null}, otherwise {@code false}.
   */
  public static boolean isEmpty(float[] array) {
    return null == array || array.length == 0;
  }

  /**
   * Returns {@code true} if the array is {@code null} or empty,
   * otherwise returns {@code false}.
   *
   * @param array a reference to be checked against.
   * @return {@code true} if the provided array is {@code null}, otherwise {@code false}.
   */
  public static boolean isEmpty(double[] array) {
    return null == array || array.length == 0;
  }

  /**
   * Returns {@code true} if the provided reference is {@code null},
   * otherwise returns {@code false}.
   *
   * @param obj a reference to be checked against {@code null}.
   * @param <T> reference type.
   * @return {@code true} if the provided reference is {@code null}, otherwise {@code false}.
   */
  public static <T> boolean isNull(T[] obj) {
    return null == obj;
  }

  /**
   * Returns {@code true} if the provided reference is non-{@code null},
   * otherwise returns {@code false}.
   *
   * @param obj a reference to be checked against {@code null}.
   * @param <T> reference type.
   * @return {@code true} if the provided reference is non-{@code null}, otherwise {@code false}.
   */
  public static <T> boolean nonNull(T[] obj) {
    return null != obj;
  }

  /**
   * Return {@code true} if objsThan equals objsOther
   *
   * @param objsThan This is the first array to be tested for equality.
   * @param objsOther This is the second array to be tested for equality.
   * @return This method returns {@code true} if the two arrays, over the specified ranges, are equal.
   */
  public static boolean isEquals(Object[] objsThan, Object[] objsOther) {
    if (objsThan == null) {
      return objsOther == null || objsOther.length == 0;
    }

    if (objsOther == null) {
      return objsThan.length == 0;
    }

    if (objsThan.length != objsOther.length) {
      return false;
    }

    for (int i = 0; i < objsThan.length; i++) {
      if (objsThan[i] != objsOther[i]) {
        return false;
      }
    }

    return true;
  }

  /**
   * Swap T array index a and b.
   *
   * @param oriTs origin T array.
   * @param a first index.
   * @param b second index.
   * @return swap result.
   * @param <T> Object type.
   */
  public static <T> T[] swap(T[] oriTs, int a, int b) {
    T tmp = oriTs[a];
    oriTs[a] = oriTs[b];
    oriTs[b] = tmp;
    return oriTs;
  }

  /**
   * Get Supplier object with not null array.
   *
   * @param suppliers supplier array.
   * @return object array.
   * @throws NullPointerException supplier array is {@code null}.
   */
  public static Object[] getSuppliers(Supplier<Object>[] suppliers) throws NullPointerException {
    Objects.requireNonNull(suppliers);
    return Arrays.stream(suppliers)
        .filter(Objects::nonNull)
        .map(Supplier::get)
        .toArray(Object[]::new);
  }
}
