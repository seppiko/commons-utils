/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import org.seppiko.commons.utils.reflect.ClassUtil;
import org.seppiko.commons.utils.reflect.ReflectionUtil;

/**
 * Object utility
 *
 * @see Objects
 * @see Optional
 * @author Leonard Woo
 */
public class ObjectUtil {

  private ObjectUtil() {}

  /**
   * Get value not null or else.
   *
   * <p>
   * Like this <code>Optional.ofNullable(value).orElse(defaultValue);</code>
   *
   * @param value the possibly-null value to describe.
   * @param other the value to be returned, if value is {@code null}. May be {@code null}.
   * @return the value, if value not {@code null}, otherwise other.
   * @param <T> the type of value.
   */
  public static <T> T requireWithElse(T value, T other) {
    return Objects.nonNull(value)? value: other;
  }

  /**
   * Get {@link Optional} with ignore value nullable.
   *
   * @param value the possibly-null value to describe.
   * @return the {@link Optional}.
   * @param <T> the type of value.
   */
  public static <T> Optional<T> optionalOf(T value) {
    return Optional.ofNullable(value);
  }

  /**
   * Deep-copy the given object using Java Object Serialization.
   *
   * @param obj the object to deep-copy.
   * @return a deep-copy of the given object.
   * @throws IOException if an I/O error occurs.
   * @throws SecurityException if the stream header is incorrect.
   * @throws NullPointerException source object is {@code null}.
   */
  public static Object deepCopy(Object obj)
      throws IOException, SecurityException, NullPointerException {
    Objects.requireNonNull(obj);
    try {
      byte[] _tmp = SerializationUtil.serialize(obj);
      return SerializationUtil.deserialize(_tmp, Object.class);
    } catch (ReflectiveOperationException e) {
      throw new IOException("Copy this object failed.", e);
    }
  }

  /**
   * Convert map to class instance
   *
   * @param map name and value map {@code <FieldName, FieldValue>}.
   * @param clazz target class.
   * @return class instance.
   * @param <T> class type.
   * @param <V> field value type.
   * @throws IllegalCallerException set target class field failed.
   * @throws ReflectiveOperationException retry set target class failed.
   */
  public static <T, V> T mapConvert(LinkedHashMap<String, V> map, Class<T> clazz)
      throws IllegalCallerException, ReflectiveOperationException {
    Assert.notEmpty(map, "map must not be empty");
    Assert.notNull(clazz, "clazz must not be null");

    LinkedHashMap<String, Field> fieldMap = ReflectionUtil.getFieldMap(clazz);
    T t;
    try {
      t = ClassUtil.getInstance(clazz);
    } catch (NoSuchMethodException e) {
      t = null;
    }

    AtomicInteger i = new AtomicInteger(0);
    Class<?>[] parameterTypes = new Class[map.size()];
    Object[] values = new Object[map.size()];

    T finalT = t;
    map.forEach((k, v) -> {
      Field field = fieldMap.get(k);
      if (Objects.nonNull(field) && Objects.nonNull(finalT)) {
        try {
          ReflectionUtil.setField(field, true, finalT, v);
        } catch (ReflectiveOperationException | RuntimeException e) {
          throw new IllegalCallerException("Set target class field failed.", e);
        }
      } else {
        parameterTypes[i.get()] = field.getType();
        values[i.get()] = v;
      }

      i.incrementAndGet();
    });

    if ((null == t) && (i.get() == map.size())) {
      try {
        t = ReflectionUtil.setRecordAllFields(clazz, true, parameterTypes, values);
      } catch (ReflectiveOperationException e) {
        throw new ReflectiveOperationException("Retry set class field failed. " + e.getMessage());
      }
    }

    return t;
  }

  /**
   * Convert entity class field to another class set method
   *
   * @param entity Entity class
   * @param clazz Target class
   * @param fieldMap Field name map {@code <EntityFieldName, TargetFieldName>}
   * @param flag If {@code true} inject entity field to target class, otherwise call setXxx method
   * @return Target object
   * @param <E> Entity class type
   * @param <T> Target class type
   * @throws NullPointerException some param is null
   * @throws IllegalArgumentException target class has no-args public constructor
   * @throws IllegalCallerException get entity field or call target class set-method failed
   * @throws RuntimeException call setXxx or something is failed
   * @throws IllegalAccessException get class instance failed
   */
  public static <E, T> T entityConvert(
      final E entity, Class<T> clazz, LinkedHashMap<String, String> fieldMap, boolean flag)
      throws NullPointerException, IllegalArgumentException, IllegalCallerException,
      RuntimeException, IllegalAccessException {
    Assert.notNull(entity, "entity must not be null");
    Assert.notNull(clazz, "clazz must not be null");
    if (Objects.isNull(fieldMap) || fieldMap.isEmpty()) {
      throw new NullPointerException("fieldMap must not be null or empty");
    }

    // get entity fields
    Field[] entityFields;
    try {
      entityFields = ReflectionUtil.getAllFields(entity.getClass());
    } catch (RuntimeException e) {
      entityFields = ReflectionUtil.getDeclaredFields(entity.getClass());
    }

    T target;
    try {
      target = ClassUtil.getInstance(clazz);
    } catch (NoSuchMethodException e) {
      // If flag is true, inject entity fields to target class fields
      if(flag) {
        return injectFields(entityFields, entity, clazz);
      } else {
        throw new IllegalArgumentException("Class " + clazz.getName() + " has no public no-args constructor.", e);
      }
    }

    // Traverse the entity fields and Call setXxx method
    Field[] finalEntityFields = entityFields;
    T finalTarget = target;
    fieldMap.forEach((k, v) -> callSetters(finalEntityFields, entity, finalTarget, k, v));

    return finalTarget;
  }

  // inject entity fields to class and return instance
  private static <E, T> T injectFields(Field[] fields, E entity, Class<T> clazz)
      throws IllegalCallerException, IllegalArgumentException {
    Class<?>[] parameterTypes = new Class[fields.length];
    Object[] values = new Object[fields.length];
    try {
      // get entity fields type and value
      for (int i = 0; i < fields.length; i++) {
        try {
          fields[i].setAccessible(true);
          parameterTypes[i] = fields[i].getType();
          values[i] = fields[i].get(entity);
        } catch (ReflectiveOperationException ex) {
          throw new IllegalCallerException("Get entity field failed.", ex);
        }
      }

      return ReflectionUtil.setRecordAllFields(clazz, true, parameterTypes, values);
    } catch (ReflectiveOperationException ex) {
      throw new IllegalArgumentException("Set target class " + clazz.getName() + " field failed.", ex);
    }
  }

  // call setXxx method with field
  private static <E, T> void callSetters(
      Field[] entityFields, E entity, T target, String entityFieldName, String targetFieldName)
      throws IllegalCallerException {
    for (Field field : entityFields) {
      try {
        if (entityFieldName.equals(field.getName())) {
          // Entity field value
          Object efv = ReflectionUtil.getField(field, true, entity);
          ReflectionUtil.invokeMethod(target, "set" + StringUtil.toFirstUpperCase(targetFieldName), efv);
        }
      } catch (ReflectiveOperationException e) {
        throw new IllegalCallerException(
            "Get entity field or call target class setXxx method failed.", e);
      }
    }
  }
}
