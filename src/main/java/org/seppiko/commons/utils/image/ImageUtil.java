/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.image;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Objects;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.stream.ImageOutputStream;
import org.seppiko.commons.utils.Assert;

/**
 * Image utility
 *
 * @author Leonard Woo
 */
public class ImageUtil {

  private ImageUtil() {}

  /**
   * Compress image.
   *
   * @param image a {@link BufferedImage} instance.
   * @param formatName a {@code String} containing the informal name of a format. (e.g., "jpeg" or
   *     "tiff").
   * @param quality a {@code float} between {@code 0} and {@code 1} indicating the desired quality
   *     level.
   * @param enableProgressiveMode if {@code true} enable progressive mode.
   * @return the image byte array.
   * @throws IOException I/O Error.
   * @throws NullPointerException Image is {@code null}.
   * @throws IllegalArgumentException format name is {@code null}. Or
   * @see ImageIO#getImageWritersByFormatName(String)
   * @see ImageWriter
   */
  public static byte[] compressor(
      BufferedImage image, String formatName, float quality, boolean enableProgressiveMode)
      throws IOException, NullPointerException, IllegalArgumentException {
    return compressor(image, formatName, quality, true, enableProgressiveMode);
  }

  /**
   * Compress image.
   *
   * @param image a {@link BufferedImage} instance.
   * @param formatName a {@code String} containing the informal name of a format. (e.g., "jpeg" or
   *     "tiff").
   * @param quality a {@code float} between {@code 0} and {@code 1} indicating the desired quality
   *     level.
   * @param disableMetadata if {@code true} image metadata is {@code null}, otherwise write image
   *     metadata from image instance.
   * @param enableProgressiveMode if {@code true} enable progressive mode.
   * @return the image byte array.
   * @throws IOException I/O Error.
   * @throws NullPointerException Image is {@code null}.
   * @throws IllegalArgumentException format name is {@code null}. Or
   * @see ImageIO#getImageWritersByFormatName(String)
   * @see ImageWriter
   */
  public static byte[] compressor(BufferedImage image, String formatName, float quality,
      boolean disableMetadata, boolean enableProgressiveMode)
      throws IOException, NullPointerException, IllegalArgumentException {
    Objects.requireNonNull(image, "Image must be not null.");
    Assert.notBlank(formatName, "Format name must be not null.");

    try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageOutputStream ios = ImageIO.createImageOutputStream(baos)) {

      ImageWriter writer = ImageIO.getImageWritersByFormatName(formatName).next();
      writer.setOutput(ios);

      IIOMetadata metadata = disableMetadata?
          null :
          writer.getDefaultImageMetadata(
              ImageTypeSpecifier.createFromRenderedImage(image), null);

      ImageWriteParam param = writer.getDefaultWriteParam();

      if (param.canWriteCompressed()) {
        param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        param.setCompressionQuality(quality);
      }

      if (enableProgressiveMode && param.canWriteProgressive()) {
        param.setProgressiveMode(ImageWriteParam.MODE_DEFAULT);
      }

      writer.write(metadata, new IIOImage(image, null, metadata), param);
      writer.dispose();

      return baos.toByteArray();
    }
  }

  /**
   * Write image to byte array.
   *
   * @param image a {@code BufferedImage} instance.
   * @param formatName a {@code String} containing the informal name of a format.
   * @return the image byte array. {@code null} if no appropriate writer is found.
   * @throws IOException I/O Error.
   * @throws NullPointerException Image is {@code null}.
   */
  public static byte[] writeImage(BufferedImage image, String formatName)
      throws IOException, NullPointerException {
    try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
      Objects.requireNonNull(image);
      if (ImageIO.write(image, formatName, baos)) {
        return baos.toByteArray();
      }
      return null;
    }
  }
}
