/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils;

import java.io.File;
import java.io.Serial;
import java.io.Serializable;

/**
 * Environmental constant
 *
 * @author Leonard Woo
 */
public class Environment implements Serializable {

  @Serial
  private static final long serialVersionUID = -2855120233208109544L;

  private Environment(){}

  /** Get system line interval symbol. Or System properties {@code line.separator}. */
  public static final String NEW_LINE = System.lineSeparator();

  /** Get File Separator */
  public static final String FILE_SEPARATOR = File.separator;

  /** Get Current Process */
  public static final ProcessHandle CURRENT_PROCESS = ProcessHandle.current();

  /** Get Current Thread */
  public static final Thread CURRENT_THREAD = Thread.currentThread();

  /** Digit length */
  public static final int DIGIT_LENGTH = 10;

  /** Alphabet length */
  public static final int ALPHABET_LENGTH = 26;

  /** Digit and Alphabet size */
  public static final int DIGIT_ALPHABET_ALL_COUNT = 62;

  /** Digit and upper and lower letters ,like regex {@code "[0-9A-Za-z]"} */
  public static final char[] DIGIT_ALPHABET_ALL = new char[] {
      '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
      'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
      'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
      'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
      't', 'u', 'v', 'w', 'x', 'y', 'z'
  };

  /** Fullwidth digit and upper and lower letters, like regex {@code "[０-９Ａ-Ｚａ-ｚ]"} */
  public static final char[] FULLWIDTH_DIGIT_ALPHABET_ALL  = new char[] {
      '０', '１', '２', '３', '４', '５', '６', '７', '８', '９',
      'Ａ', 'Ｂ', 'Ｃ', 'Ｄ', 'Ｅ', 'Ｆ', 'Ｇ', 'Ｈ', 'Ｉ', 'Ｊ', 'Ｋ', 'Ｌ', 'Ｍ', 'Ｎ', 'Ｏ', 'Ｐ', 'Ｑ', 'Ｒ', 'Ｓ',
      'Ｔ', 'Ｕ', 'Ｖ', 'Ｗ', 'Ｘ', 'Ｙ', 'Ｚ',
      'ａ', 'ｂ', 'ｃ', 'ｄ', 'ｅ', 'ｆ', 'ｇ', 'ｈ', 'ｉ', 'ｊ', 'ｋ', 'ｌ', 'ｍ', 'ｎ', 'ｏ', 'ｐ', 'ｑ', 'ｒ', 'ｓ',
      'ｔ', 'ｕ', 'ｖ', 'ｗ', 'ｘ', 'ｙ', 'ｚ'
  };

  /** Empty boolean array */
  public static final boolean[] EMPTY_BOOLEAN_ARRAY = new boolean[0];

  /** Empty byte array */
  public static final byte[] EMPTY_BYTE_ARRAY = new byte[0];

  /** Empty short array */
  public static final short[] EMPTY_SHORT_ARRAY = new short[0];

  /** Empty int array */
  public static final int[] EMPTY_INT_ARRAY = new int[0];

  /** Empty long array */
  public static final long[] EMPTY_LONG_ARRAY = new long[0];

  /** Empty float array */
  public static final float[] EMPTY_FLOAT_ARRAY = new float[0];

  /** Empty double array */
  public static final double[] EMPTY_DOUBLE_ARRAY = new double[0];

  /** Empty char array */
  public static final char[] EMPTY_CHAR_ARRAY = new char[0];

  /** Empty object array */
  public static final Object[] EMPTY_OBJECT_ARRAY = new Object[0];

  /** Config file parameter suffix {@code configFile} */
  public static final String CONFIG_FILE_PARAMETER_SUFFIX = "configFile";

  /** {@code config.yaml} */
  public static final String CONFIG_FILENAME_YAML = "config.yaml";

  /** {@code config.yml} */
  public static final String CONFIG_FILENAME_YML = "config.yml";

  /** {@code config.json} */
  public static final String CONFIG_FILENAME_JSON = "config.json";

  /** {@code config.jsn} */
  public static final String CONFIG_FILENAME_JSN = "config.jsn";

  /** {@code config.toml} */
  public static final String CONFIG_FILENAME_TOML = "config.toml";

  /** {@code config.ini} */
  public static final String CONFIG_FILENAME_INI = "config.ini";
}
