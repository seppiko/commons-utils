/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;
import org.seppiko.commons.utils.internal.VersionComparator;

/**
 * Semantic Version Utility
 *
 * @see <a href="https://semver.org/">Semantic Versioning</a>
 * @author Leonard Woo
 */
public class VersionUtil implements Serializable {

  @Serial
  private static final long serialVersionUID = -1334772796709558040L;

  /** version check regex */
  private static final String REGEX_STR = "^(0|[1-9]\\d*)\\.(0|[1-9]\\d*)\\.(0|[1-9]\\d*)"
      + "(?:-((?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\\.(?:0|[1-9]\\d*|"
      + "\\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\\+([0-9a-zA-Z-]+(?:\\.[0-9a-zA-Z-]+)*))?$";

  /** version string supplementary offset */
  private static final int SUPPL_OFFSET = 1;

  /** version string */
  private final String version;

  /** version major */
  private final int major;

  /** version minor */
  private final int minor;

  /** version patch */
  private final int patch;

  /** version qualifier */
  private final String qualifier;

  /** version build */
  private final String build;

  private VersionUtil(final String versionString) {
    if (StringUtil.isNullOrEmpty(versionString)) {
      throw new IllegalArgumentException("Version string must be not null");
    }
    if (!check(versionString)) {
      throw new IllegalArgumentException("Version string format must conform to SemVer 2.0.0 (https://semver.org/)");
    }

    int major = 0;
    int minor = 0;
    int patch = 0;
    String qualify = "";
    String build = "";

    int length = versionString.length();
    int type = 0;
    int val = 0;
    for (int offset = 0; offset < length; offset++) {
      char c = versionString.charAt(offset);
      if (c < CharUtil.DIGIT_ZERO || c > CharUtil.DIGIT_NINE) {
        switch (type) {
          case 0 -> major = val;
          case 1 -> minor = val;
          case 2 -> {
            patch = val;
            qualify = versionString.substring(offset);
            offset = length;
          }
        }
        type++;
        val = 0;
      } else {
        val = val * 10 + c - CharUtil.DIGIT_ZERO;
      }
    }
    if (type == 2) {
      patch = val;
    }

    if (!qualify.isEmpty()) {
      int qualifyIdx = qualify.indexOf("-");
      if (qualifyIdx >= 0) {
        qualify = qualify.substring(qualifyIdx + SUPPL_OFFSET);
      }

      int buildIdx = qualify.indexOf("+");
      if (buildIdx >= 0) {
        build = qualify.substring(buildIdx + SUPPL_OFFSET);
        qualify = qualify.substring(0, buildIdx);
      }
    }

    this.major = major;
    this.minor = minor;
    this.patch = patch;
    this.qualifier = qualify;
    this.build = build;
    this.version = versionString;
  }

  private VersionUtil(int major, int minor, int patch, String qualifier, String build) {
    this.major = major;
    this.minor = minor;
    this.patch = patch;
    this.qualifier = ObjectUtil.requireWithElse(qualifier, "");
    this.build = ObjectUtil.requireWithElse(build, "");
    this.version = generator(major, minor, patch, qualifier, build);
  }

  private String generator(int major, int minor, int patch, String qualify, String build) {
    String ver = String.format("%d.%d.%d", major, minor, patch);
    if (StringUtil.hasText(qualify)) {
      ver = String.format("%s-%s", ver, qualify);
    }
    if (StringUtil.hasText(build)) {
      ver = String.format("%s+%s", ver, build);
    }
    return ver;
  }

  /**
   * parser version string
   *
   * @param versionString version string
   * @return {@link VersionUtil} instance
   */
  public static VersionUtil parser(String versionString) {
    return new VersionUtil(versionString);
  }

  /**
   * Return a {@code VersionUtil} describing the given value
   *
   * @param major major version
   * @param minor minor version
   * @param patch version path
   * @param qualifier version qualify
   * @param build version build
   * @return {@link VersionUtil} instance
   */
  public static VersionUtil of(int major, int minor, int patch, String qualifier, String build) {
    return new VersionUtil(major, minor, patch, qualifier, build);
  }

  /**
   * Check version string format
   *
   * @param versionString version string
   * @return {@code true} if version string matches SemVer
   */
  private boolean check(String versionString) {
    return RegexUtil.matches(REGEX_STR, versionString);
  }

  /**
   * get version string
   *
   * @return version string
   */
  public String versionString() {
    return version;
  }

  /**
   * get major version
   *
   * @return major number
   */
  public int major() {
    return major;
  }

  /**
   * get minor version
   *
   * @return minor number
   */
  public int minor() {
    return minor;
  }

  /**
   * get patch version
   *
   * @return path number
   */
  public int patch() {
    return patch;
  }

  /**
   * get version qualifier
   *
   * @return qualifier string
   */
  public String qualifier() {
    return qualifier;
  }

  /**
   * get version build
   *
   * @return build string
   */
  public String build() {
    return build;
  }

  /**
   * compare two version string
   *
   * @param other another {@code VersionUtil} instance
   * @return if equal return 0, if this greater than other return 1, if this lesser than other return -1
   */
  public int compareTo(VersionUtil other) {
    if (null == other) {
      throw new NullPointerException("`other` must be not null");
    }
    return new VersionComparator().compare(this, other);
  }

  /**
   * Check version equal
   *
   * @param o version object
   * @return {@code true} if object is {@code VersionUtil} and equals
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VersionUtil that = (VersionUtil) o;
    return version.equals(that.versionString()) ||
        ( major == that.major() && minor == that.minor() && patch == that.patch() &&
            Objects.equals(qualifier, that.qualifier()) && Objects.equals(build, that.build) );
  }

  /** VersionUtil instance hashcode */
  @Override
  public int hashCode() {
    return Objects.hash(version, major, minor, patch, qualifier, build);
  }

  /** Version string */
  @Override
  public String toString() {
    return version;
  }
}
