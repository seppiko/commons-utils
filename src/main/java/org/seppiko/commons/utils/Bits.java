/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils;

/**
 * A Bits with byte size.
 *
 * @author Leonard Woo
 */
public class Bits {

  private static final int BIT_OF_BYTE = 8;

  private boolean[] bits;

  /**
   * Bits constructor
   */
  public Bits() {
    clean();
  }

  /**
   * Initializes a newly created {@code Bits} object so that it represents
   * the same sequence of characters as the argument.
   *
   * @param original A {@link Bits}
   */
  public Bits(Bits original) {
    this.bits = original.bits;
  }

  /**
   * Put a bit with position
   *
   * <pre><code>
   *   0  1  2  3  4  5  6  7
   * +--+--+--+--+--+--+--+--+
   * |         1 byte        |
   * +--+--+--+--+--+--+--+--+
   * </code></pre>
   *
   * @param value bit value
   * @param position this bit position
   */
  public void putBit(boolean value, int position) {
    if (position >= BIT_OF_BYTE) {
      throw new IndexOutOfBoundsException("position must be than less 8");
    }
    bits[position] = value;
  }

  /**
   * Put a byte to bit
   *
   * @param b byte
   */
  public void putByte(byte b) {
    for (int i = 0; i < BIT_OF_BYTE; i++) {
      bits[i] = (0x01 & (b >> i)) != 0;
    }
  }

  /**
   * Get a bits with boolean array
   *
   * @return boolean array
   */
  public boolean[] getBits() {
    return bits;
  }

  /**
   * Get this bits byte
   *
   * @return a byte
   */
  public byte getByte() {
    byte b = 0b00000000;
    for(int i = 0; i < BIT_OF_BYTE; i++) {
      b <<= 1;
      boolean bit = bits[i];
      if (bit) {
        b |= 1;
      }
    }
    return b;
  }

  /**
   * Clean bits value.
   */
  public void clean() {
    bits = new boolean[BIT_OF_BYTE];
  }
}
