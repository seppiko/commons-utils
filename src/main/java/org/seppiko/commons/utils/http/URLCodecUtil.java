/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.http;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.StandardCharsets;
import java.nio.charset.UnsupportedCharsetException;
import java.util.Objects;

/**
 * URL Encoding and Decoding utility
 *
 * @author Leonard Woo
 */
public class URLCodecUtil {

  /** Default URL Codec charset */
  private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

  private URLCodecUtil() {}

  /**
   * The default charset used for string decoding and encoding.
   *
   * @return the default string charset name
   */
  public static String getDefaultCharset() {
    return DEFAULT_CHARSET.name();
  }

  /**
   * Translates a string into application/x-www-form-urlencoded format using default charset.
   *
   * @param str String to be translated
   * @return the translated String
   * @throws NullPointerException if str is null
   */
  public static String encode(String str) throws NullPointerException {
    return encode(str, DEFAULT_CHARSET);
  }

  /**
   * Translates a string into application/x-www-form-urlencoded format using a specific encoding
   * scheme.
   *
   * @param str String to be translated
   * @param charsetName the given charset name
   * @return the translated String
   * @throws UnsupportedEncodingException if this charset is unsupported
   * @throws NullPointerException if str or charsetName is null
   */
  public static String encode(String str, String charsetName)
      throws UnsupportedEncodingException, NullPointerException {
    try {
      Charset charset = Charset.forName(Objects.requireNonNull(charsetName));
      return encode(str, charset);
    } catch (IllegalCharsetNameException | UnsupportedCharsetException e) {
      throw new UnsupportedEncodingException(charsetName);
    } catch (IllegalArgumentException e) {
      throw new NullPointerException();
    }
  }

  /**
   * Translates a string into application/x-www-form-urlencoded format using a specific {@link
   * Charset}.
   *
   * @param str String to be translated
   * @param charset the given charset
   * @return the translated String
   * @throws NullPointerException if str or charset is null
   */
  public static String encode(String str, Charset charset) throws NullPointerException {
    return URLEncoder.encode(str, charset);
  }

  /**
   * Decodes an application/x-www-form-urlencoded string using default charset.
   *
   * @param str the String to decode
   * @return the newly decoded string
   * @throws UnsupportedEncodingException can not decode with default charset
   * @throws NullPointerException if str is null
   */
  public static String decode(String str)
      throws UnsupportedEncodingException, NullPointerException {
    return decode(str, DEFAULT_CHARSET);
  }

  /**
   * Decodes an application/x-www-form-urlencoded string using a specific encoding scheme.
   *
   * @param str the String to decode
   * @param charsetName the given charset name
   * @return the newly decoded string
   * @throws UnsupportedEncodingException if this charset could not decode or unsupported charset
   * @throws NullPointerException if str or charsetName is null
   */
  public static String decode(String str, String charsetName)
      throws UnsupportedEncodingException, NullPointerException {
    try {
      Charset charset = Charset.forName(Objects.requireNonNull(charsetName));
      return decode(str, charset);
    } catch (IllegalCharsetNameException | UnsupportedCharsetException e) {
      throw new UnsupportedEncodingException(charsetName);
    } catch (IllegalArgumentException e) {
      throw new NullPointerException();
    }
  }

  /**
   * Decodes an application/x-www-form-urlencoded string using a specific {@link Charset}.
   *
   * @param str the String to decode
   * @param charset the given charset
   * @return the newly decoded string
   * @throws UnsupportedEncodingException if this charset could not decode
   * @throws NullPointerException if str or charset is null
   */
  public static String decode(String str, Charset charset)
      throws UnsupportedEncodingException, NullPointerException {
    try {
      return URLDecoder.decode(str, charset);
    } catch (IllegalArgumentException e) {
      throw new UnsupportedEncodingException(charset.name());
    }
  }
}
