/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.http;

import java.net.URI;
import java.net.http.HttpClient.Version;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Optional;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import javax.net.ssl.SSLSession;
import org.seppiko.commons.utils.exceptions.HttpInterruptedException;
import org.seppiko.commons.utils.exceptions.HttpRuntimeException;

/**
 * HttpResponse implementation
 *
 * @param <T> Http body type
 * @author Leonard Woo
 * @see HttpResponse
 */
public class HttpResponseImpl<T> implements HttpResponse<T> {

  private final CompletableFuture<HttpResponse<T>> resp;

  /**
   * Constructor
   *
   * @param resp http response
   */
  protected HttpResponseImpl(CompletableFuture<HttpResponse<T>> resp) {
    this.resp = resp;
  }

  /**
   * Returns the status code for this response.
   *
   * @return the response code
   */
  @Override
  public int statusCode() {
    try {
      return resp.thenApply(HttpResponse::statusCode).get();
    } catch (ExecutionException | CancellationException e) {
      throw runtime(e);
    } catch (InterruptedException e) {
      throw interrupt(e);
    }
  }

  /**
   * Returns the HttpRequest corresponding to this response.
   *
   * @return the request
   * @see #previousResponse()
   */
  @Override
  public HttpRequest request() {
    try {
      return resp.thenApply(HttpResponse::request).get();
    } catch (ExecutionException | CancellationException e) {
      throw runtime(e);
    } catch (InterruptedException e) {
      throw interrupt(e);
    }
  }

  /**
   * Returns an {@code Optional} containing the previous intermediate response if one was received.
   *
   * @return an Optional containing the HttpResponse, if any.
   */
  @Override
  public Optional<HttpResponse<T>> previousResponse() {
    try {
      return resp.thenApply(HttpResponse::previousResponse).get();
    } catch (ExecutionException | CancellationException e) {
      throw runtime(e);
    } catch (InterruptedException e) {
      throw interrupt(e);
    }
  }

  /**
   * Returns the received response headers.
   *
   * @return the response headers
   */
  @Override
  public HttpHeaders headers() {
    try {
      return resp.thenApply(HttpResponse::headers).get();
    } catch (ExecutionException | CancellationException e) {
      throw runtime(e);
    } catch (InterruptedException e) {
      throw interrupt(e);
    }
  }

  /**
   * Returns the body.
   *
   * <p>If this {@code HttpResponse} was returned from an invocation of {@link #previousResponse()}
   * then this method returns {@code null}
   *
   * @return the body
   */
  @Override
  public T body() {
    try {
      return resp.thenApply(HttpResponse::body).get();
    } catch (ExecutionException | CancellationException e) {
      throw runtime(e);
    } catch (InterruptedException e) {
      throw interrupt(e);
    }
  }

  /**
   * Returns an {@link Optional} containing the {@link SSLSession} in effect for this response.
   *
   * @return an {@code Optional} containing the {@code SSLSession} associated with the response
   */
  @Override
  public Optional<SSLSession> sslSession() {
    try {
      return resp.thenApply(HttpResponse::sslSession).get();
    } catch (ExecutionException | CancellationException e) {
      throw runtime(e);
    } catch (InterruptedException e) {
      throw interrupt(e);
    }
  }

  /**
   * Returns the {@code URI} that the response was received from. This may be different from the
   * request {@code URI} if redirection occurred.
   *
   * @return the URI of the response
   */
  @Override
  public URI uri() {
    try {
      return resp.thenApply(HttpResponse::uri).get();
    } catch (ExecutionException | CancellationException e) {
      throw runtime(e);
    } catch (InterruptedException e) {
      throw interrupt(e);
    }
  }

  /**
   * Returns the HTTP protocol version that was used for this response.
   *
   * @return HTTP protocol version
   */
  @Override
  public Version version() {
    try {
      return resp.thenApply(HttpResponse::version).get();
    } catch (ExecutionException | CancellationException e) {
      throw runtime(e);
    } catch (InterruptedException e) {
      throw interrupt(e);
    }
  }

  private HttpRuntimeException runtime(Exception ex) {
    return new HttpRuntimeException(ex);
  }

  private RuntimeException interrupt(InterruptedException ex) {
    Thread.currentThread().interrupt();
    return new HttpInterruptedException(ex);
  }
}
