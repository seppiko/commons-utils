/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.http;

import java.io.Serializable;
import org.seppiko.commons.utils.Assert;
import org.seppiko.commons.utils.ObjectUtil;
import org.seppiko.commons.utils.StringUtil;

/**
 * Http Cookie
 *
 * @param name cookie name.
 * @param value cookie value.
 * @see <a href="https://tools.ietf.org/html/rfc6265">RFC 6265</a>
 * @see <a href="https://en.wikipedia.org/wiki/HTTP_cookie">HTTP cookie</a>
 * @author Leonard Woo
 */
public record HttpCookie(String name, String value) implements Serializable {

  /**
   * Add a cookie
   *
   * @param name cookie name
   * @param value cookie value
   */
  public HttpCookie(String name, String value) {
    Assert.isTrue(StringUtil.hasLength(name), "cookie name is required and must not be empty");
    this.name = name;
    this.value = ObjectUtil.requireWithElse(value, "");
  }

  /**
   * check cookie is equals
   *
   * @param o the reference object with which to compare.
   * @return true if cookie is equal
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof HttpCookie otherCookie)) {
      return false;
    }
    return (this.name.equalsIgnoreCase(otherCookie.name()));
  }

  /**
   * get cookie hashcode
   *
   * @return hashcode
   */
  @Override
  public int hashCode() {
    return this.name.hashCode();
  }

  /**
   * get cookie pair string
   *
   * @return cookie string
   */
  @Override
  public String toString() {
    if (StringUtil.hasLength(this.value)) {
      return String.format("%s=%s",this.name, this.value);
    }
    return this.name;
  }
}
