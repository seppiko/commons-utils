/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.http;

import java.util.Objects;

/**
 * Http methods
 *
 * @see <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods">Http Method</a>
 * @see <a href="https://httpwg.org/specs/rfc7231.html#method.definitions">RFC7231</a>
 * @author Leonard Woo
 */
public enum HttpMethod {

  /**
   * The GET method requests a representation of the specified resource. Requests using GET should
   * only retrieve data.
   */
  GET,

  /**
   * The POST method submits an entity to the specified resource, often causing a change in state or
   * side effects on the server.
   */
  POST,

  /**
   * The PUT method replaces all current representations of the target resource with the request
   * payload.
   */
  PUT,

  /** The DELETE method deletes the specified resource. */
  DELETE,

  /**
   * The HEAD method asks for a response identical to a GET request, but without the response body.
   */
  HEAD,

  /** The OPTIONS method describes the communication options for the target resource. */
  OPTIONS,

  /** The CONNECT method establishes a tunnel to the server identified by the target resource. */
  CONNECT,

  /** The TRACE method performs a message loop-back test along the path to the target resource. */
  TRACE,

  /** The PATCH method applies partial modifications to a resource. */
  PATCH,
  ;

  private static final HttpMethod[] VALUES;

  static {
    VALUES = values();
  }

  /**
   * Return an {@link HttpMethod} object for the given value.
   *
   * @param name the method value as a String.
   * @return the corresponding {@link HttpMethod}.
   * @throws NullPointerException method name is null.
   * @throws IllegalArgumentException name not found.
   */
  public static HttpMethod resolve(String name) throws NullPointerException, IllegalArgumentException {
    Objects.requireNonNull(name, "http method name must be not null");
    final String methodName = name.strip().toUpperCase();
    for (HttpMethod method : VALUES) {
      if (method.name().equals(methodName)) {
        return method;
      }
    }
    throw new IllegalArgumentException("http method name not found");
  }
}
