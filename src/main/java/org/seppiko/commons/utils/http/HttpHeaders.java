/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.http;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

/**
 * Simple Http Headers implementation for {@link java.net.http.HttpRequest}
 *
 * @author Leonard Woo
 */
public class HttpHeaders implements Serializable {

  @Serial
  private static final long serialVersionUID = -7241706861359666774L;

  private static final String HTTP_HEADER_VALUE_SEPARATOR = "; ";

  /** http header map */
  private final LinkedHashMap<String, ArrayList<String>> headers = new LinkedHashMap<>();

  /** Default user agent */
  public static final String DEFAULT_USER_AGENT;

  static {
    String javaVersion = System.getProperty("java.version");
    DEFAULT_USER_AGENT = String.format("Mozilla/5.0 (compatible; JavaHttpClient/%s)", javaVersion);
  }

  private HttpHeaders() {}

  private HttpHeaders(Map<String, List<String>> headerMap) {
    if (!headerMap.isEmpty()) {
      headerMap.forEach( (k, v) -> headers.put(k, new ArrayList<>(v)) );
    }
  }

  /**
   * Create http headers.
   *
   * @return The new {@link HttpHeaders} instance.
   */
  public static HttpHeaders newHeaders() {
    return new HttpHeaders();
  }

  /**
   * Create http headers with Map.
   *
   * @param headerMap header map.
   * @return The new {@link HttpHeaders} instance.
   */
  public static HttpHeaders newHeaders(Map<String, List<String>> headerMap) {
    return new HttpHeaders(headerMap);
  }

  /**
   * Set header.
   *
   * @param name header name.
   * @param values header values.
   * @return this instance.
   */
  public HttpHeaders setHeaders(String name, String... values) {
    return setHeaders(name, Arrays.asList(values));
  }

  /**
   * Set header.
   *
   * @param name header name.
   * @param values header values.
   * @return this instance.
   */
  public HttpHeaders setHeaders(String name, List<String> values) {
    headers.put(name, new ArrayList<>(values));
    return this;
  }

  /**
   * Add headers.
   *
   * @param name header name.
   * @param values header values.
   * @return this instance.
   * @throws IllegalArgumentException copy old values failed.
   */
  public HttpHeaders addHeaders(String name, String... values) throws IllegalArgumentException {
    return addHeaders(name, Arrays.asList(values));
  }

  /**
   * Add headers.
   *
   * @param name header name.
   * @param values header values.
   * @return this instance.
   * @throws IllegalArgumentException copy old values failed.
   */
  public HttpHeaders addHeaders(String name, List<String> values) throws IllegalArgumentException {
    if (headers.containsKey(name)) {
      ArrayList<String> valueOld = headers.get(name);
      ArrayList<String> valueNew = new ArrayList<>(values);
      for (String value : valueOld) {
        if (!valueNew.contains(value)) {
          try {
            valueNew.add(value);
          } catch (UnsupportedOperationException | ClassCastException | IllegalArgumentException
              | NullPointerException ex) {
            throw new IllegalArgumentException(ex);
          }
        }
      }
      headers.put(name, valueNew);
    } else {
      setHeaders(name, values);
    }
    return this;
  }

  /**
   * Get {@link java.net.http.HttpRequest} header map.
   *
   * @return Header string array.
   * @throws IllegalCallerException Object initialization parameter exception.
   * @throws IllegalArgumentException Parameter exception.
   */
  public LinkedHashMap<String, String> getHeaderMap() throws IllegalCallerException, IllegalArgumentException {
    checkHeaders();

    LinkedHashMap<String, String> headerMap = new LinkedHashMap<>();
    if (!headers.isEmpty()) {
      headers.forEach((name, value) -> headerMap.put(name, valuesHandler(value)) );
    }
    return headerMap;
  }

  private String valuesHandler(ArrayList<String> valueList) {
    return String.join(HTTP_HEADER_VALUE_SEPARATOR, valueList);
  }

  /**
   * Return http header.
   *
   * @return http header string.
   */
  @Override
  public String toString() {
    StringJoiner sj = new StringJoiner(", ");
    headers.forEach((k, v) -> {
      sj.add("\"" + k + "\"=" + v.toString());
    });
    return sj.toString();
  }

  private void checkHeaders() {
    // check and add ua
    if (!this.headers.containsKey(HttpHeader.USER_AGENT)) {
      addHeaders(HttpHeader.USER_AGENT, DEFAULT_USER_AGENT);
    }
  }
}
