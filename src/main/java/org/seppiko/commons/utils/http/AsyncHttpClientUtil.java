/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.http;

import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.InetSocketAddress;
import java.net.ProxySelector;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import javax.net.ssl.SSLContext;
import org.seppiko.commons.utils.exceptions.HttpResponseException;
import org.seppiko.commons.utils.exceptions.HttpRuntimeException;

/**
 * Http Async Response utility with {@link java.net.http}
 *
 * <p>{@code HttpRequest} see {@link HttpClientUtil}
 *
 * @author Leonard Woo
 */
public class AsyncHttpClientUtil extends HttpClientUtil {

  AsyncHttpClientUtil() {}

  /**
   * Get Async String Response
   *
   * @param req HttpRequest instance.
   * @param sslContext HTTP TLS Context, if unused set {@link TLSUtil#NULL_SSL_CONTEXT}. see {@link TLSUtil}.
   * @param proxy HTTP Proxy, Not null is enabled proxy.
   * @return Exception is {@code null}.
   * @throws HttpResponseException if async response exception.
   * @throws IllegalArgumentException if the request argument is not a request that could have been
   *     validly built as specified.
   * @throws HttpRuntimeException http response field exception.
   */
  public static HttpResponse<String> getAsyncResponseString(
      HttpRequest req, SSLContext sslContext, InetSocketAddress proxy)
      throws HttpRuntimeException, IllegalArgumentException, HttpResponseException {
    return createHttpResponse(getAsyncResponseRaw(req, BodyHandlers.ofString(), sslContext, proxy));
  }

  /**
   * Get Async byte array Response
   *
   * @param req HttpRequest instance.
   * @param sslContext HTTP TLS Context, if unused set {@link TLSUtil#NULL_SSL_CONTEXT}. see {@link TLSUtil}.
   * @param proxy HTTP Proxy, Not null is enabled proxy.
   * @return Exception is {@code null}.
   * @throws HttpResponseException get async response exception.
   * @throws IllegalArgumentException if the request argument is not a request that could have been
   *     validly built as specified.
   * @throws HttpRuntimeException http response field exception.
   */
  public static HttpResponse<byte[]> getAsyncResponseByteArray(
      HttpRequest req, SSLContext sslContext, InetSocketAddress proxy)
      throws HttpRuntimeException, IllegalArgumentException, HttpResponseException {
    return createHttpResponse(getAsyncResponseRaw(req, BodyHandlers.ofByteArray(), sslContext, proxy));
  }

  /**
   * Get Async InputStream Response
   *
   * @param req HttpRequest instance.
   * @param sslContext HTTP TLS Context, if unused set {@link TLSUtil#NULL_SSL_CONTEXT}. see {@link TLSUtil}.
   * @param proxy HTTP Proxy, Not null is enabled proxy.
   * @return Exception is {@code null}.
   * @throws HttpResponseException get async response exception.
   * @throws IllegalArgumentException if the request argument is not a request that could have been
   *     validly built as specified.
   * @throws HttpRuntimeException http response field exception.
   */
  public static HttpResponse<InputStream> getAsyncResponseInputStream(
      HttpRequest req, SSLContext sslContext, InetSocketAddress proxy)
      throws HttpRuntimeException, IllegalArgumentException, HttpResponseException {
    return createHttpResponse(getAsyncResponseRaw(req, BodyHandlers.ofInputStream(), sslContext, proxy));
  }

  /**
   * Return Http Response implementation
   *
   * @param resp Async HttpResponse instance.
   * @return HttpResponse instance.
   * @param <T> body type.
   * @throws HttpRuntimeException Response implementation exceptions.
   */
  private static <T> HttpResponse<T> createHttpResponse(CompletableFuture<HttpResponse<T>> resp)
      throws HttpRuntimeException {
    return new HttpResponseImpl<>(resp);
  }

  /**
   * Get Async HTTP Response raw
   *
   * @param req HTTP request instance.
   * @param responseBodyHandler Response body content type.
   * @param sslContext HTTP TLS Context, if unused set {@code null}. see {@link TLSUtil}.
   * @param proxy HTTP Proxy, Not null is enabled proxy.
   * @return async http response instance.
   * @param <T> Response content type.
   * @throws HttpResponseException IO or something is {@code null}.
   */
  protected static <T> CompletableFuture<HttpResponse<T>> getAsyncResponseRaw(
      HttpRequest req,
      HttpResponse.BodyHandler<T> responseBodyHandler,
      SSLContext sslContext,
      InetSocketAddress proxy)
      throws HttpResponseException {
    try {
      HttpClient.Builder builder = HttpClient.newBuilder();
      if (Objects.nonNull(proxy)) {
        builder = builder.proxy(ProxySelector.of(proxy));
      }
      if (sslContext != TLSUtil.NULL_SSL_CONTEXT) {
        builder = builder.sslContext(sslContext);
      }
      return builder.build()
          .sendAsync(req, responseBodyHandler);
    } catch (UncheckedIOException | IllegalArgumentException ex) {
      throw new HttpResponseException(ex);
    }
  }
}
