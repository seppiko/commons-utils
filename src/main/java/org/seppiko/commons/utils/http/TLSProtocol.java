/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.http;

/**
 * TLS protocol standard name for the requested protocol.
 *
 * @see <a
 *     href="https://docs.oracle.com/en/java/javase/21/docs/specs/security/standard-names.html#sslcontext-algorithms">
 *     Java Security Standard Algorithm Names</a>
 * @author Leonard Woo
 */
public enum TLSProtocol {

  /** Supports some version of TLS; may support other SSL/TLS versions. */
  TLS("TLS"),

  /**
   * Supports RFC 4346: TLS version 1.1; may support other SSL/TLS versions.
   * This version is unsafe.
   */
  @Deprecated
  TLSv11("TLSv1.1"),

  /** Supports RFC 5246: TLS version 1.2; may support other SSL/TLS versions. */
  TLSv12("TLSv1.2"),

  /** Supports RFC 8446: TLS version 1.3; may support other SSL/TLS versions. */
  TLSv13("TLSv1.3"),

  /** Supports the default provider-dependent versions of DTLS versions. */
  DTLS("DTLS"),
  /** Supports RFC 6347: DTLS version 1.2; may support other DTLS versions. */
  DTLSv12("DTLSv1.2"),

  // Add JDK 21
  /** Supports RFC 9147: DTLS version 1.3; may support other DTLS versions. */
  DTLSv13("DTLSv1.3")
  ;

  private final String name;

  TLSProtocol(String name) {
    this.name = name;
  }

  /**
   * Get SSLContext algorithm name
   *
   * @return algorithm name
   */
  public String getName() {
    return name;
  }
}
