/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.http;

import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * HTTP Media type
 *
 * @author Leonard Woo
 * @see <a href="https://www.iana.org/assignments/media-types/media-types.xhtml">Media Types</a>
 */
public enum MediaType {

  /** Public constant media type that includes all media ranges (i.e. "&#42;/&#42;"). */
  ALL("*", "*"),

  /** Public constant media type for {@code application/octet-stream}. */
  APPLICATION_OCTET_STREAM("application", "octet-stream"),

  /** Public constant media type for {@code text/event-stream}. */
  TEXT_EVENT_STREAM("text", "event-stream"),

  /** Public constant media type for {@code application/atom+xml}. */
  APPLICATION_ATOM_XML("application", "atom+xml"),

  /** Public constant media type for {@code application/rss+xml}. */
  APPLICATION_RSS_XML("application", "rss+xml"),

  /** Public constant media type for {@code application/soap+xml}. */
  APPLICATION_SOAP_XML("application", "soap+xml"),

  /** Public constant media type for {@code application/x-www-form-urlencoded}. */
  APPLICATION_FORM_URLENCODED("application", "x-www-form-urlencoded"),

  /** Public constant media type for {@code multipart/form-data}. */
  MULTIPART_FORM_DATA("multipart", "form-data"),

  /** Public constant media type for {@code multipart/encrypted}. */
  MULTIPART_ENCRYPTED("multipart", "encrypted"),

  /** Public constant media type for {@code application/json}. */
  APPLICATION_JSON("application", "json"),

  /** Public constant media type for {@code application/xml}. */
  APPLICATION_XML("application", "xml"),

  /** Public constant media type for {@code text/xml}. */
  TEXT_XML("text", "xml"),

  /** Public constant media type for {@code application/xslt+xml}. */
  APPLICATION_XSLT("application", "xslt+xml"),

  /** Public constant media type for {@code application/xhtml+xml}. */
  APPLICATION_XHTML_XML("application", "xhtml+xml"),

  /** Public constant media type for {@code text/css}. */
  TEXT_CSS("text", "css"),

  /** Public constant media type for {@code text/html}. */
  TEXT_HTML("text", "html"),

  /** Public constant media type for {@code text/javascript}. */
  TEXT_JAVASCRIPT("text", "javascript"),

  /** Public constant media type for {@code application/pdf}. */
  APPLICATION_PDF("application", "pdf"),

  /** Public constant media type for {@code font/otf}. */
  FONT_OTF("font", "otf"),

  /** Public constant media type for {@code font/ttf}. */
  FONT_TTF("font", "ttf"),

  /** Public constant media type for {@code font/woff}. */
  FONT_WOFF("font", "woff"),

  /** Public constant media type for {@code font/woff2}. */
  FONT_WOFF2("font", "woff2"),

  /** Public constant media type for {@code image/avif}. AV1 Image File Format */
  IMAGE_AVIF("image", "avif"),

  /** Public constant media type for {@code image/gif}. */
  IMAGE_GIF("image", "gif"),

  /** Public constant media type for {@code image/jpeg}. */
  IMAGE_JPEG("image", "jpeg"),

  /** Public constant media type for {@code image/png}. */
  IMAGE_PNG("image", "png"),

  /** Public constant media type for {@code image/svg+xml}. */
  IMAGE_SVG("image", "svg+xml"),

  /** Public constant media type for {@code image/webp}. */
  IMAGE_WEBP("image", "webp"),

  /** Public constant media type for {@code image/x-icon}. */
  IMAGE_ICO("image", "x-icon"),

  /** Public constant media type for {@code image/jxl}. */
  IMAGE_JPEG_XL("image", "jxl"),

  /** Public constant media type for {@code image/heic}. */
  IMAGE_HEIC("image", "heic"),

  /** Public constant media type for {@code text/asciidoc}. */
  TEXT_ASCIIDOC("text", "asciidoc"),

  /** Public constant media type for {@code text/csv}. */
  TEXT_CSV("text", "csv"),

  /** Public constant media type for {@code text/markdown}. */
  TEXT_MARKDOWN("text", "markdown"),

  /** Public constant media type for {@code text/plain}. */
  TEXT_PLAIN("text", "plain"),

  /** Public constant media type for {@code video/mp4}. */
  VIDEO_MP4("video", "mp4"),

  /** Public constant media type for {@code video/av1}. */
  VIDEO_AV1("video", "av1"),

  /** Public constant media type for {@code video/ogg}. */
  VIDEO_OGG("video", "ogg"),

  /** Public constant media type for {@code video/webm}. */
  VIDEO_WEBM("video", "webm"),

  /** Public constant media type for {@code audio/webm}. */
  AUDIO_WEBM("audio", "webm"),
  ;

  private static final Map<String, MediaType> MEDIA_TYPE_MAP;

  static {
    Map<String, MediaType> map = new ConcurrentHashMap<>();
    for (MediaType type : MediaType.values()) {
      map.put(type.getValue(), type);
    }
    MEDIA_TYPE_MAP = Collections.unmodifiableMap(map);
  }

  private final String type;
  private final String subtype;
  private final Charset charset;

  /**
   * Create a new {@link MediaType} for the given primary type and subtype.
   *
   * <p>The parameters are empty.
   *
   * @param type the primary type
   * @param subtype the subtype
   */
  MediaType(String type, String subtype) {
    this(type, subtype, null);
  }

  /**
   * Create a new {@link MediaType} for the given type, subtype, and character set.
   *
   * @param type the primary type
   * @param subtype the subtype
   * @param charset the character set
   */
  MediaType(String type, String subtype, Charset charset) {
    this.type = type;
    this.subtype = subtype;
    this.charset = charset;
  }

  /**
   * Find a media type by value
   *
   * @param value check type value
   * @return null - if not found
   */
  public static MediaType findByValue(String value) {
    return MEDIA_TYPE_MAP.get(value.toLowerCase());
  }

  /**
   * Get Media type value
   *
   * @return Media type value
   */
  public String getValue() {
    StringBuilder sb = new StringBuilder();
    sb.append(type);
    sb.append("/");
    sb.append(subtype);
    if (charset != null) {
      sb.append("; charset=");
      sb.append(charset.name());
    }
    return sb.toString();
  }

  /**
   * get Media Type value
   *
   * @return MediaType common
   */
  @Override
  public String toString() {
    return getValue();
  }

  /**
   * Media type equals.
   *
   * @param type target media type.
   * @return true if type and subtype is equals, false otherwise.
   */
  public boolean equals(MediaType type) {
    return this.type.equals(type.type) && this.subtype.equals(type.subtype);
  }
}
