/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.http;

/**
 * Http (request) header field names.
 *
 * @see <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers">HTTP headers</a>
 * @author Leonard Woo
 */
public class HttpHeader {

  /** private constructor */
  private HttpHeader() {
    throw new AssertionError("No " + this.getClass().getTypeName() + " instances for you!");
  }

  /**
   * The HTTP {@code Accept} header field name.
   * @see <a href="https://tools.ietf.org/html/rfc7231#section-5.3.2">Section 5.3.2 of RFC 7231</a>
   */
  public static final String ACCEPT = "Accept";

  /**
   * The HTTP {@code Accept-Encoding} header field name.
   * @see <a href="https://tools.ietf.org/html/rfc7231#section-5.3.4">Section 5.3.4 of RFC 7231</a>
   */
  public static final String ACCEPT_ENCODING = "Accept-Encoding";

  /**
   * The HTTP {@code Accept-Language} header field name.
   * @see <a href="https://tools.ietf.org/html/rfc7231#section-5.3.5">Section 5.3.5 of RFC 7231</a>
   */
  public static final String ACCEPT_LANGUAGE = "Accept-Language";

  /**
   * The CORS {@code Access-Control-Request-Headers} request header field name.
   * @see <a href="https://www.w3.org/TR/cors/">CORS W3C recommendation</a>
   */
  public static final String ACCESS_CONTROL_REQUEST_HEADERS = "Access-Control-Request-Headers";

  /**
   * The CORS {@code Access-Control-Request-Method} request header field name.
   * @see <a href="https://www.w3.org/TR/cors/">CORS W3C recommendation</a>
   */
  public static final String ACCESS_CONTROL_REQUEST_METHOD = "Access-Control-Request-Method";

  /**
   * The HTTP {@code Authorization} header field name.
   * @see <a href="https://tools.ietf.org/html/rfc7235#section-4.2">Section 4.2 of RFC 7235</a>
   */
  public static final String AUTHORIZATION = "Authorization";

  /**
   * The HTTP {@code Cache-Control} header field name.
   * @see <a href="https://tools.ietf.org/html/rfc7234#section-5.2">Section 5.2 of RFC 7234</a>
   */
  public static final String CACHE_CONTROL = "Cache-Control";

  /**
   * The HTTP {@code Connection} header field name.
   * @see <a href="https://tools.ietf.org/html/rfc7230#section-6.1">Section 6.1 of RFC 7230</a>
   */
  public static final String CONNECTION = "Connection";

  /**
   * The HTTP {@code Content-Encoding} header field name.
   * @see <a href="https://tools.ietf.org/html/rfc7231#section-3.1.2.2">Section 3.1.2.2 of RFC 7231</a>
   */
  public static final String CONTENT_ENCODING = "Content-Encoding";

  /**
   * The HTTP {@code Content-Disposition} header field name.
   * @see <a href="https://tools.ietf.org/html/rfc6266">RFC 6266</a>
   */
  public static final String CONTENT_DISPOSITION = "Content-Disposition";

  /**
   * The HTTP {@code Content-Language} header field name.
   * @see <a href="https://tools.ietf.org/html/rfc7231#section-3.1.3.2">Section 3.1.3.2 of RFC 7231</a>
   */
  public static final String CONTENT_LANGUAGE = "Content-Language";

  /**
   * The HTTP {@code Content-Length} header field name.
   * @see <a href="https://tools.ietf.org/html/rfc7230#section-3.3.2">Section 3.3.2 of RFC 7230</a>
   */
  public static final String CONTENT_LENGTH = "Content-Length";

  /**
   * The HTTP {@code Content-Range} header field name.
   * @see <a href="https://tools.ietf.org/html/rfc7233#section-4.2">Section 4.2 of RFC 7233</a>
   */
  public static final String CONTENT_RANGE = "Content-Range";

  /**
   * The HTTP {@code Content-Type} header field name.
   * @see <a href="https://tools.ietf.org/html/rfc7231#section-3.1.1.5">Section 3.1.1.5 of RFC 7231</a>
   */
  public static final String CONTENT_TYPE = "Content-Type";

  /**
   * The HTTP {@code Cookie} header field name.
   * @see <a href="https://tools.ietf.org/html/rfc2109#section-4.3.4">Section 4.3.4 of RFC 2109</a>
   */
  public static final String COOKIE = "Cookie";

  /**
   * The HTTP {@code Origin} header field name.
   * @see <a href="https://tools.ietf.org/html/rfc6454">RFC 6454</a>
   */
  public static final String ORIGIN = "Origin";

  /**
   * The HTTP {@code Pragma} header field name.
   * @see <a href="https://tools.ietf.org/html/rfc7234#section-5.4">Section 5.4 of RFC 7234</a>
   */
  public static final String PRAGMA = "Pragma";

  /**
   * The HTTP {@code Proxy-Authorization} header field name.
   * @see <a href="https://tools.ietf.org/html/rfc7235#section-4.4">Section 4.4 of RFC 7235</a>
   */
  public static final String PROXY_AUTHORIZATION = "Proxy-Authorization";

  /**
   * The HTTP {@code Range} header field name.
   * @see <a href="https://tools.ietf.org/html/rfc7233#section-3.1">Section 3.1 of RFC 7233</a>
   */
  public static final String RANGE = "Range";

  /**
   * The HTTP {@code Referer} header field name.
   * @see <a href="https://tools.ietf.org/html/rfc7231#section-5.5.2">Section 5.5.2 of RFC 7231</a>
   */
  public static final String REFERER = "Referer";

  /**
   * The HTTP User-agent client hints field name.
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Sec-CH-UA">Sec-CH-UA</a>
   */
  public static final String SEC_CH_UA = "Sec-CH-UA";

  /**
   * The HTTP User-agent client hints field name.
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Sec-CH-UA-Arch">
   *   Sec-CH-UA-Arch</a>
   */
  public static final String SEC_CH_UA_ARCH = "Sec-CH-UA-Arch";

  /**
   * The HTTP User-agent client hints field name.
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Sec-CH-UA-Bitness">
   *   Sec-CH-UA-Bitness</a>
   */
  public static final String SEC_CH_UA_BITNESS = "Sec-CH-UA-Bitness";

  /**
   * The HTTP User-agent client hints field name.
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Sec-CH-UA-Full-Version-List">
   *   Sec-CH-UA-Full-Version-List</a>
   */
  public static final String SEC_CH_UA_FULL_VERSION_LIST = "Sec-CH-UA-Full-Version-List";

  /**
   * The HTTP User-agent client hints field name.
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Sec-CH-UA-Mobile">
   *   Sec-CH-UA-Mobile</a>
   */
  public static final String SEC_CH_UA_MOBILE = "Sec-CH-UA-Mobile";

  /**
   * The HTTP User-agent client hints field name.
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Sec-CH-UA-Model">
   *   Sec-CH-UA-Model</a>
   */
  public static final String SEC_CH_UA_MODEL = "Sec-CH-UA-Model";

  /**
   * The HTTP User-agent client hints field name.
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Sec-CH-UA-Platform">
   *   Sec-CH-UA-Platform</a>
   */
  public static final String SEC_CH_UA_PLATFORM = "Sec-CH-UA-Platform";

  /**
   * The HTTP User-agent client hints field name.
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Sec-CH-UA-Platform-Version">
   *   Sec-CH-UA-Platform-Version</a>
   */
  public static final String SEC_CH_UA_PLATFORM_VERSION = "Sec-CH-UA-Platform-Version";

  /**
   * The HTTP User-agent client hints field name.
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Sec-Fetch-Dest">
   *   Sec-Fetch-Dest</a>
   */
  public static final String SEC_FETCH_DEST = "Sec-Fetch-Dest";

  /**
   * The HTTP User-agent client hints field name.
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Sec-Fetch-Mode">
   *   Sec-Fetch-Mode</a>
   */
  public static final String SEC_FETCH_MODE = "Sec-Fetch-Mode";

  /**
   * The HTTP User-agent client hints field name.
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Sec-Fetch-Site">
   *   Sec-Fetch-Site</a>
   */
  public static final String SEC_FETCH_SITE = "Sec-Fetch-Site";

  /**
   * The HTTP User-agent client hints field name.
   * @see <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Sec-Fetch-User">
   *   Sec-Fetch-User</a>
   */
  public static final String SEC_FETCH_USER = "Sec-Fetch-User";

  /**
   * The HTTP {@code Upgrade} header field name.
   * @see <a href="https://tools.ietf.org/html/rfc7230#section-6.7">Section 6.7 of RFC 7230</a>
   */
  public static final String UPGRADE = "Upgrade";

  /**
   * The HTTP {@code Upgrade-Insecure-Requests} header field name.
   * @see <a href="https://w3c.github.io/webappsec-upgrade-insecure-requests/#preference">
   *   3.2.1. The Upgrade-Insecure-Requests HTTP Request Header Field</a>
   */
  public static final String UPGRADE_INSECURE_REQUESTS = "Upgrade-Insecure-Requests";

  /**
   * The HTTP {@code User-Agent} header field name.
   * @see <a href="https://tools.ietf.org/html/rfc7231#section-5.5.3">Section 5.5.3 of RFC 7231</a>
   */
  public static final String USER_AGENT = "User-Agent";

  /**
   * The HTTP {@code Via} header field name.
   * @see <a href="https://tools.ietf.org/html/rfc7230#section-5.7.1">Section 5.7.1 of RFC 7230</a>
   */
  public static final String VIA = "Via";

}
