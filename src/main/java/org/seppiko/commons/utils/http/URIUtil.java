/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.http;

import java.net.URI;
import java.util.Objects;
import org.seppiko.commons.utils.CharUtil;
import org.seppiko.commons.utils.RegexUtil;
import org.seppiko.commons.utils.StringUtil;

/**
 * URI utility
 *
 * <pre><code>
 * [scheme ":"] ["//" authority] "/" path ["?" query] ["#" fragment]
 * authority = [userinfo "@"] host [":" port]
 * userinfo = username [":" password]
 * </code></pre>
 *
 * But the URI definition requires that the scheme must be used.
 *
 * @author Leonard Woo
 */
public class URIUtil {

  private URIUtil() {}

  private static final String IPV4_REGEX = "(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])\\."
          + "(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])\\."
          + "(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])\\."
          + "(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])";

  private static final String IPV6_REGEX = "("
      + "([\\da-fA-F]{1,4}:){7,7}[\\da-fA-F]{1,4}|"
      + "([\\da-fA-F]{1,4}:){1,7}:|"
      + "([\\da-fA-F]{1,4}:){1,6}:[\\da-fA-F]{1,4}|"
      + "([\\da-fA-F]{1,4}:){1,5}(:[\\da-fA-F]{1,4}){1,2}|"
      + "([\\da-fA-F]{1,4}:){1,4}(:[\\da-fA-F]{1,4}){1,3}|"
      + "([\\da-fA-F]{1,4}:){1,3}(:[\\da-fA-F]{1,4}){1,4}|"
      + "([\\da-fA-F]{1,4}:){1,2}(:[\\da-fA-F]{1,4}){1,5}|"
      + "[\\da-fA-F]{1,4}:((:[\\da-fA-F]{1,4}){1,6})|"
      + ":((:[\\da-fA-F]{1,4}){1,7}|:)|"
      + "fe80:(:[\\da-fA-F]{0,4}){0,4}%[\\da-zA-Z]{1,}|"
      + "::(ffff(:0{1,4}){0,1}:){0,1}(" + IPV4_REGEX + ")|"
      + "([\\da-fA-F]{1,4}:){1,4}:(" + IPV4_REGEX + ")"
      + ")";

  /**
   * get URI object
   *
   * @param uri URI address (like url)
   * @return URI object
   * @throws NullPointerException If uri string is null
   * @throws IllegalArgumentException If the given string violates RFC 2396
   */
  protected static URI getUri(String uri) throws NullPointerException, IllegalArgumentException {
    return URI.create(uri);
  }

  /**
   * Test ip string is IPv4
   *
   * @param ip ip string
   * @return true if ip is IPv4 string, false otherwise
   */
  public static boolean isIPv4(String ip) {
    return RegexUtil.matches("^" + IPV4_REGEX + "$", ip);
  }

  /**
   * Test ip string is IPv6
   *
   * @param ip ip string
   * @return true if ip is IPv6 string, false otherwise
   */
  public static boolean isIPv6(String ip) {
    return RegexUtil.matches("^" + IPV6_REGEX + "$", ip);
  }

  /**
   * Test hostname string is IPv6 hostname
   *
   * @param hostname hostname string
   * @return true if ip is IPv6 string, false otherwise
   */
  public static boolean isIPv6Hostname(String hostname) {
    return RegexUtil.matches("^\\[" + IPV6_REGEX + "\\]$", hostname);
  }

  /**
   * Get last path name (e.g. filename)
   *
   * @param uri URI
   * @return last path content e.g. filename
   * @throws IllegalArgumentException uri is not legal URI
   * @throws NullPointerException uri is null
   */
  public static String getLastPathname(String uri)
      throws IllegalArgumentException, NullPointerException {
    String filename = getUri(uri).getPath();
    return filename.substring(filename.lastIndexOf(CharUtil.SOLIDUS) + 1);
  }

  /**
   * get file extension
   *
   * @param filename file name (e.g. last pathname)
   * @return file extension, if not found return null
   * @throws NullPointerException filename is null
   */
  public static String getFileExtension(String filename) throws NullPointerException {
    Objects.requireNonNull(filename);
    if (filename.lastIndexOf(CharUtil.FULL_STOP) >= 0) {
      if (RegexUtil.matches(".*\\p{Alnum}+$", filename)) {
        return filename.substring(filename.lastIndexOf(CharUtil.FULL_STOP) + 1).toLowerCase();
      }
    }
    return null;
  }
}
