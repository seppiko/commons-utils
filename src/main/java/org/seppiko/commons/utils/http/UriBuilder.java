/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.http;

import java.net.URI;
import java.util.Arrays;
import java.util.Objects;
import java.util.StringJoiner;
import org.seppiko.commons.utils.NumberUtil;
import org.seppiko.commons.utils.StringUtil;

/**
 * URI builder
 *
 * <pre><code>
 * [scheme ":"] ["//" authority] "/" path ["?" query] ["#" fragment]
 * authority = [userinfo "@"] host [":" port]
 * userinfo = username [":" password]
 * </code></pre>
 *
 * @see URI
 * @author Leonard Woo
 */
public final class UriBuilder {

  private String scheme;
  private String userInfo;
  private String hostname;
  private Integer port;
  private final StringJoiner pathJoiner = new StringJoiner("/");
  private final StringJoiner queryJoiner = new StringJoiner("&");
  private String fragment;

  private UriBuilder() {}

  /**
   * {@link UriBuilder} from url string
   *
   * @param url URL
   * @return {@link UriBuilder} instance.
   * @throws NullPointerException If the url is null.
   * @throws IllegalArgumentException If the given string violates RFC 2396.
   */
  public static UriBuilder fromString(String url)
      throws NullPointerException, IllegalArgumentException {
    URI uri = URIUtil.getUri(url);

    UriBuilder builder = new UriBuilder().withScheme(uri.getScheme());
    String userInfo = uri.getUserInfo();
    String path = uri.getPath();
    String query = uri.getQuery();

    if (!StringUtil.isNullOrEmpty(userInfo)) {
      if (userInfo.contains(":"))  {
        String[] user = userInfo.split(":");
        builder.withUserInfo(user[0], user[1]);
      } else {
        builder.withUserInfo(userInfo, "");
      }
    }

    builder.withHostname(uri.getHost());

    if (uri.getPort() >= 0) {
      builder.withPort(uri.getPort());
    }

    if (!StringUtil.isNullOrEmpty(path)) {
      builder.addPath(path);
    }

    if (!StringUtil.isNullOrEmpty(query)) {
      String[] params = query.split("&");
      Arrays.stream(params).forEach(param -> {
        String[] part = param.split("=");
         builder.addQuery(part[0], part[1]);
      });
    }

    if (!StringUtil.isNullOrEmpty(uri.getFragment())) {
      builder.withFragment(uri.getFragment());
    }

    return builder;
  }

  /**
   * Create a {@link UriBuilder} instance.
   *
   * @return A new UriBuilder instance.
   */
  public static UriBuilder create() {
    return new UriBuilder();
  }

  /**
   * With URI scheme.
   *
   * @param scheme URI scheme.
   * @return this instance.
   */
  public UriBuilder withScheme(String scheme) {
    Objects.requireNonNull(scheme);
    this.scheme = scheme;
    return this;
  }

  /**
   * With URI user info.
   *
   * @param username Login username.
   * @param password Login password.
   * @return this instance.
   */
  public UriBuilder withUserInfo(String username, String password) {
    if (!StringUtil.isNullOrEmpty(username)) {
      this.userInfo = username;
      if (!StringUtil.isNullOrEmpty(password)) {
        this.userInfo += ":" + password;
      }
    } else {
      throw new NullPointerException("Not found user info");
    }
    return this;
  }

  /**
   * With URI host.
   *
   * @param hostname Hostname
   * @return this instance.
   */
  public UriBuilder withHostname(String hostname) {
    Objects.requireNonNull(hostname);
    if (hostname.endsWith("/")) {
      hostname = hostname.substring(0, hostname.length() - 1);
    }
    if (!hostname.startsWith("[") && !hostname.endsWith("]") && URIUtil.isIPv6(hostname)) {
      hostname = "[" + hostname + "]";
    }
    this.hostname = hostname;
    return this;
  }

  /**
   * With URI port
   *
   * @param port Port
   * @return this instance.
   */
  public UriBuilder withPort(int port) {
    if (NumberUtil.between(port, 1, 65535)) {
      this.port = port;
    } else {
      throw new IllegalArgumentException("Port must between 1 and 65535");
    }
    return this;
  }

  /**
   * Add URI path
   *
   * @param path URI path
   * @return this instance.
   */
  public UriBuilder addPath(String path) {
    Objects.requireNonNull(path);
    if (path.startsWith("/")) {
      path = path.substring(1);
    }
    pathJoiner.add(path);
    return this;
  }

  /**
   * Add URI query
   *
   * @param name query name
   * @param value query value
   * @return this instance.
   */
  public UriBuilder addQuery(String name, String value) {
    Objects.requireNonNull(name);
    Objects.requireNonNull(value);
    queryJoiner.add(name + "=" + value);
    return this;
  }

  /**
   * With URI fragment
   *
   * @param fragment fragment
   * @return this instance.
   */
  public UriBuilder withFragment(String fragment) {
    if (!StringUtil.isNullOrEmpty(fragment)) {
      this.fragment = fragment;
    }
    return this;
  }

  /**
   * Return URI string
   *
   * @return URI string.
   */
  public String toUri() {
    StringBuilder uriSB = new StringBuilder();
    if (!StringUtil.isNullOrEmpty(scheme)) {
      uriSB.append(scheme);
      uriSB.append("://");
    }
    if (!StringUtil.isNullOrEmpty(userInfo)) {
      uriSB.append(userInfo);
      uriSB.append("@");
    }
    uriSB.append(hostname);
    if (port != null) {
      uriSB.append(":");
      uriSB.append(port);
    }
    uriSB.append("/");
    if (pathJoiner.length() > 0) {
      uriSB.append(pathJoiner);
    }
    if (queryJoiner.length() > 0) {
      uriSB.append("?");
      uriSB.append(queryJoiner);
    }
    if (fragment != null) {
      uriSB.append("#");
      uriSB.append(fragment);
    }
    return uriSB.toString();
  }
}
