/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

/**
 * Java Socket and NIO Socket utility
 *
 * @author Leonard Woo
 */
public class SocketUtil {

  private SocketUtil() {}

  /**
   * Create {@link InetSocketAddress} instance
   *
   * @param host socket hostname, if {@code null} ignore this.
   * @param port socket port
   * @return InetSocketAddress instance.
   * @throws IllegalArgumentException
   *   if the port parameter is outside the specified range of valid port values.
   */
  public static InetSocketAddress createSocketAddress(String host, int port) throws IllegalArgumentException {
    if (host == null || host.isEmpty()) {
      return new InetSocketAddress(port);
    }
    return new InetSocketAddress(host, port);
  }

  /**
   * Create client socket
   *
   * @param address remote socket address
   * @param timeout connect timeout
   * @return Socket instance
   * @throws IOException if an error occurs during the connection.
   * @throws IllegalArgumentException if endpoint is null or is a SocketAddress subclass not
   *     supported by this socket, or if {@code timeout} is negative.
   */
  public static Socket createSocket(InetSocketAddress address, int timeout)
      throws IOException, IllegalArgumentException {
    Socket socket = new Socket();
    socket.connect(address, timeout);
    return socket;
  }

  /**
   * Create server socket
   *
   * @param address server bind address
   * @param backlog requested maximum length of the queue of incoming connections
   * @return ServerSocket instance
   * @throws IOException if the bind operation fails, or if the socket is already bound.
   */
  public static ServerSocket createServerSocket(InetSocketAddress address, int backlog) throws IOException {
    ServerSocket server = new ServerSocket();
    server.bind(address, backlog);
    return server;
  }

  /**
   * Create NIO client socket channel
   *
   * @param address remote socket address
   * @param blocking if {@code true} then this channel will be placed in blocking mode;
   *     if {@code false} then it will be placed non-blocking mode
   * @return SocketChannel instance
   * @throws IOException If some other I/O error occurs.
   */
  public static SocketChannel createNIOSocket(InetSocketAddress address, boolean blocking)
      throws IOException {
    SocketChannel socketChannel = SocketChannel.open();
    socketChannel.connect(address);
    socketChannel.configureBlocking(blocking);
    return socketChannel;
  }

  /**
   * Create NIO server socket channel
   *
   * @param address server bind address
   * @param backlog requested maximum length of the queue of incoming connections
   * @param blocking if {@code true} then this channel will be placed in blocking mode;
   *     if {@code false} then it will be placed non-blocking mode
   * @return ServerSocketChannel instance
   * @throws IOException If some other I/O error occurs.
   */
  public static ServerSocketChannel createNIOServerSocket(
      InetSocketAddress address, int backlog, boolean blocking)
      throws IOException {
    ServerSocketChannel serverChannel = ServerSocketChannel.open();
    serverChannel.bind(address, backlog);
    serverChannel.configureBlocking(blocking);
    return serverChannel;
  }
}
