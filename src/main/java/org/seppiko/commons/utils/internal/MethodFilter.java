/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.internal;

import java.lang.reflect.Method;

/**
 * Callback optionally used to filter methods to be operated on by a method callback.
 *
 * @author Spring framework team
 */
@FunctionalInterface
public interface MethodFilter {

  /**
   * Determine whether the given method matches.
   *
   * @param method the method to check.
   */
  boolean matches(Method method);

  /**
   * Create a composite filter based on this filter <em>and</em> the provided filter.
   *
   * <p>If this filter does not match, the next filter will not be applied.
   *
   * @param next the next {@link MethodFilter}.
   * @return a composite {@link MethodFilter}.
   */
  default MethodFilter and(MethodFilter next) {
    return method -> matches(method) && next.matches(method);
  }

}
