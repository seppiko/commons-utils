/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.internal;

import java.util.Comparator;
import org.seppiko.commons.utils.VersionUtil;

/**
 * Version Comparator implementation.
 *
 * @author Leonard Woo
 */
public class VersionComparator implements Comparator<VersionUtil> {

  /**
   * Indicates that a version has equivalent precedence.
   */
  private static final int EQUAL = 0;

  /**
   * Indicates that a version has greater precedence.
   */
  private static final int GREATER = 1;

  /**
   * Indicates that a version has lesser precedence.
   */
  private static final int LESSER = -1;

  /**
   * Compares two version numbers to determine their precedence.
   *
   * @param left The left hand side.
   * @param right The right hand side.
   * @return
   *   If the left hand side has a greater precedence than the right, {@code 1} (one) is returned.
   *   If both sides have equal precedence, {@code 0} (zero) is returned.
   *   If the right hand side has a greater precedence than the left, {@code -1} is returned.
   */
  @Override
  public int compare(VersionUtil left, VersionUtil right) {
    if (null == left || null == right) {
      throw new NullPointerException("Version must be not null");
    }

    if (left.equals(right)) {
      return EQUAL;
    } else if (left.major() > right.major()) {
      return GREATER;
    } else if (left.major() < right.major()) {
      return LESSER;
    } else if (left.minor() > right.minor()) {
      return GREATER;
    } else if (left.minor() < right.minor()) {
      return LESSER;
    } else if (left.patch() > right.patch()) {
      return GREATER;
    } else if (left.patch() < right.patch()) {
      return LESSER;
    }

    int qualify = left.qualifier().compareTo(right.qualifier());
    if (qualify != EQUAL) {
      return qualify;
    }

    return left.build().compareTo(right.build());
  }
}
