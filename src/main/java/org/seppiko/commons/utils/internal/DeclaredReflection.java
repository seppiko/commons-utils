/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils.internal;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.seppiko.commons.utils.reflect.ReflectionUtil;

/**
 * Get all declared fields
 *
 * @author Spring framework team
 */
public class DeclaredReflection {

  /**
   * Pre-built FieldFilter that matches all non-static, non-final fields.
   */
  public static final FieldFilter COPYABLE_FIELDS =
      (field -> !(Modifier.isStatic(field.getModifiers()) || Modifier.isFinal(field.getModifiers())));

  /**
   * Pre-built {@link MethodFilter} that matches all non-bridge non-synthetic methods
   * which are not declared on {@code java.lang.Object}.
   */
  public static final MethodFilter USER_DECLARED_METHODS =
      (method -> !method.isBridge() && !method.isSynthetic() && (method.getDeclaringClass() != Object.class));

  /**
   * Invoke the given callback on all fields in the target class, going up the
   * class hierarchy to get all declared fields.
   *
   * @param clazz the target class to analyze
   * @param fc the callback to invoke for each field
   * @throws IllegalStateException if introspection fails
   */
  public static void doWithFields(Class<?> clazz, FieldCallback fc) {
    doWithFields(clazz, fc, null);
  }

  /**
   * Invoke the given callback on all fields in the target class, going up the
   * class hierarchy to get all declared fields.
   *
   * @param clazz the target class to analyze
   * @param fc the callback to invoke for each field
   * @param ff the filter that determines the fields to apply the callback to
   * @throws IllegalStateException if introspection fails
   */
  public static void doWithFields(Class<?> clazz, FieldCallback fc, FieldFilter ff) {
    // Keep backing up the inheritance hierarchy.
    Class<?> targetClass = clazz;
    do {
      Field[] fields = ReflectionUtil.getDeclaredFields(targetClass);

      for (Field field : fields) {
        if (ff != null && !ff.matches(field)) {
          continue;
        }

        try {
          fc.doWith(field);
        } catch (IllegalAccessException ex) {
          throw new IllegalStateException("Not allowed to access field '" + field.getName() + "': " + ex);
        }
      }

      targetClass = targetClass.getSuperclass();

    } while (targetClass != null && targetClass != Object.class);
  }

  /**
   * Get class all methods include superclass
   *
   * @param clazz target class.
   * @param mc method callback.
   * @throws SecurityException can not access method or some else.
   * @throws IllegalAccessException can not access class.
   */
  public static void doWithMethods(Class<?> clazz, MethodCallback mc)
      throws SecurityException, IllegalAccessException {
    doWithMethods(clazz, mc, null);
  }

  /**
   * Get class all methods include superclass
   *
   * @param clazz target class.
   * @param mc method callback.
   * @param mf method filter.
   * @throws SecurityException can not access method or some else.
   * @throws IllegalStateException if introspection fails
   */
  public static void doWithMethods(Class<?> clazz, MethodCallback mc, MethodFilter mf)
      throws SecurityException, IllegalStateException {
    if (mf == USER_DECLARED_METHODS && clazz == Object.class) {
      // nothing to introspect
      return;
    }

    Method[] methods = ReflectionUtil.getDeclaredMethods(clazz);
    for (Method method : methods) {
      if (mf != null && !mf.matches(method)) {
        continue;
      }

      try {
        mc.doWith(method);
      } catch (IllegalAccessException ex) {
        throw new IllegalStateException("Not allowed to access method '" + method.getName() + "': " + ex);
      }
    }

    if (clazz.getSuperclass() != null &&
        (mf != USER_DECLARED_METHODS || clazz.getSuperclass() != Object.class) ) {
      doWithMethods(clazz.getSuperclass(), mc, mf);
    } else if (clazz.isInterface()) {
      for (Class<?> superIfc : clazz.getInterfaces()) {
        doWithMethods(superIfc, mc, mf);
      }
    }

  }
}
