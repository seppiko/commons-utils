/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** Seppiko Commons Utils module */
module seppiko.commons.utils {
  requires java.desktop;
  requires java.naming;
  requires java.net.http;
  requires java.sql;

  exports org.seppiko.commons.utils;
  exports org.seppiko.commons.utils.codec;
  exports org.seppiko.commons.utils.concurrent;
  exports org.seppiko.commons.utils.crypto;
  exports org.seppiko.commons.utils.crypto.crc;
  exports org.seppiko.commons.utils.crypto.spec;
  exports org.seppiko.commons.utils.exceptions;
  exports org.seppiko.commons.utils.http;
  exports org.seppiko.commons.utils.image;
  exports org.seppiko.commons.utils.jdbc;
  exports org.seppiko.commons.utils.reflect;
}
