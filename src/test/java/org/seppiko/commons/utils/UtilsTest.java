/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.net.InetSocketAddress;
import java.net.URISyntaxException;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.SecureRandom;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.SSLContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.seppiko.commons.utils.codec.Base16;
import org.seppiko.commons.utils.codec.Base32;
import org.seppiko.commons.utils.codec.Base36;
import org.seppiko.commons.utils.codec.Base62;
import org.seppiko.commons.utils.codec.Base64Util;
import org.seppiko.commons.utils.codec.Base64Ext;
import org.seppiko.commons.utils.codec.HexUtil;
import org.seppiko.commons.utils.crypto.AsymmetricUtil;
import org.seppiko.commons.utils.crypto.CRCUtil;
import org.seppiko.commons.utils.crypto.CryptoUtil;
import org.seppiko.commons.utils.crypto.KeyGeneratorUtil;
import org.seppiko.commons.utils.crypto.HashUtil;
import org.seppiko.commons.utils.crypto.HmacAlgorithms;
import org.seppiko.commons.utils.crypto.MessageDigestAlgorithms;
import org.seppiko.commons.utils.crypto.CipherNameUtil;
import org.seppiko.commons.utils.crypto.SignatureUtil;
import org.seppiko.commons.utils.crypto.SymmetricUtil;
import org.seppiko.commons.utils.crypto.spec.KeySpecUtil;
import org.seppiko.commons.utils.crypto.spec.ParameterSpecUtil;
import org.seppiko.commons.utils.http.AsyncHttpClientUtil;
import org.seppiko.commons.utils.exceptions.HttpClientException;
import org.seppiko.commons.utils.http.HttpClientUtil;
import org.seppiko.commons.utils.http.HttpCookie;
import org.seppiko.commons.utils.http.HttpHeader;
import org.seppiko.commons.utils.http.HttpHeaders;
import org.seppiko.commons.utils.http.HttpMethod;
import org.seppiko.commons.utils.http.HttpStatus;
import org.seppiko.commons.utils.http.MediaType;
import org.seppiko.commons.utils.http.TLSUtil;
import org.seppiko.commons.utils.reflect.ClassUtil;
import org.seppiko.commons.utils.reflect.DynamicInvocationHandler;
import org.seppiko.commons.utils.reflect.ProxyFactory;
import org.seppiko.commons.utils.reflect.ReflectionUtil;

/**
 * @author Leonard Woo
 */
public class UtilsTest {

  private static final Logger log = LogManager.getLogger(UtilsTest.class);

  @Test
  public void baseTest() {
    String text = "SCU-BaseN::{encode, decode} >> Seppiko 万能ツールキット";
    log.info("Origin: " + text);
    byte[] textb = text.getBytes();
    log.info("Origin bytes: " + HexUtil.encodeString(textb).toUpperCase() );

    log.info("--- Base64 ---");
    byte[] base64 = Base64Util.encode(textb);
//    log.info("Encode bytes: " + HexUtil.encodeString(base64).toUpperCase() );
    log.info("Encode: " + StandardCharsets.ISO_8859_1.decode(ByteBuffer.wrap(base64)));
    log.info("Decode: " + Base64Util.decodeString(base64) );

    log.info("--- Radix 64 for bcrypt ---");
    final char[] RADIX64_ALPHABET = {
        '.', '/',
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
        'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
        'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
    };
    Base64Ext radix64 = new Base64Ext(RADIX64_ALPHABET, false).withoutPadding();
    byte[] base64x = radix64.encode(textb);
    log.info("Encode: " + CharUtil.charsetDecode(StandardCharsets.ISO_8859_1, base64x).toString() );
    log.info("Decode: " + new String(radix64.decode(base64x)));

    log.info("--- Base32 ---");
    String base32 = Base32.RFC4648.encodeString(textb);
    log.info("Encode: " + base32);
    log.info("ZEncode: " + Base32.ZBASE32.encodeString(textb));
    log.info("Decode: " + new String(Base32.RFC4648.decode(base32)) );
  }

  @Test
  public void hexTest() {
    byte[] bytes = "SCU::Hex & SCU::Base16 Test".getBytes();
    log.info("Origin: " + new String(bytes));
    log.info("HEX   : " + HexUtil.encodeString(bytes));
    String base16 = Base16.RFC4648.encodeString(bytes);
    log.info("Base16: " + base16 );
    log.info("Decode: " + new String( Base16.RFC4648.decode(base16) ) );
  }

  private final String UA =
      "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36 Edg/118.0.2088.69";

  @Test
  public void httpTest()
      throws Exception {
    String url = "https://srl.cx/204";

    HttpHeaders headers = HttpHeaders.newHeaders();
    headers.addHeaders("Accept", MediaType.ALL.getValue());
    headers.addHeaders("Content-Type", MediaType.ALL.getValue());
    headers.addHeaders("Cache-Control", "no-cache");
    headers.addHeaders("Pragma", "no-cache");
    headers.addHeaders("Upgrade-Insecure-Requests", "1");
    headers.addHeaders("User-Agent", UA);
    headers.addHeaders("Sec-CH-UA",
        "\"Chromium\";v=\"118\", \"Microsoft Edge\";v=\"118\", \"Not=A?Brand\";v=\"99\""); // default
//    headers.addHeaders("Sec-CH-UA-Arch", "?x86");
//    headers.addHeaders("Sec-CH-UA-Bitness", "64");
//    headers.addHeaders("Sec-CH-UA-Full-Version-List", "\"Chromium\";v=\"116.0.5845.97\", \"Not)A;Brand\";v=\"24.0.0.0\", \"Microsoft Edge\";v=\"116.0.1938.54\"");
    headers.addHeaders("Sec-CH-UA-Mobile", "?0"); // default
//    headers.addHeaders("Sec-CH-UA-Model", "");
    headers.addHeaders("Sec-CH-UA-Platform", "\"Windows\""); // default
//    headers.addHeaders("Sec-CH-UA-Platform-Version", "\"10.0\"");
    headers.addHeaders("Sec-Fetch-Dest", "document"); // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Sec-Fetch-Dest
    headers.addHeaders("Sec-Fetch-Mode","navigate"); // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Sec-Fetch-Mode
    headers.addHeaders("Sec-Fetch-Site","none"); // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Sec-Fetch-Site
    headers.addHeaders("Sec-Fetch-User","?1"); // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Sec-Fetch-User

    SSLContext sslContext = TLSUtil.NULL_SSL_CONTEXT;
    InetSocketAddress proxy = null;

    HttpRequest req = HttpClientUtil.getRequest(url, HttpMethod.GET, 5, headers, "");
    HttpResponse<String> resp = AsyncHttpClientUtil.getAsyncResponseString(req, sslContext, proxy);
    log.info(HttpStatus.resolve(resp.statusCode()).toString());
    log.info(HttpHeaders.newHeaders(resp.headers().map()).toString());
    log.info(resp.body());
  }

  @Test
  public void httpHeaderTest() {
    HttpHeaders headers = HttpHeaders.newHeaders();
    headers.addHeaders(HttpHeader.CACHE_CONTROL, "no-cache")
        .addHeaders(HttpHeader.PRAGMA, "no-cache")
        .addHeaders(HttpHeader.UPGRADE_INSECURE_REQUESTS, "1");
//    headers.addHeaders(HttpHeader.USER_AGENT, UA);

    ArrayList<HttpCookie> cookies = new ArrayList<>();
    cookies.add(new HttpCookie("_ga", "234"));
    cookies.add(new HttpCookie("_ga_234", "56789"));

    headers.addHeaders(HttpHeader.COOKIE, cookies.stream().map(HttpCookie::toString).toArray(String[]::new));

    log.info("Headers: " + headers);
  }

  @Test
  public void http2Test() throws HttpClientException, URISyntaxException, InterruptedException {
    String url = "https://srl.cx/204";
    HttpHeaders headers = HttpHeaders.newHeaders();
    headers.addHeaders(HttpHeader.CACHE_CONTROL, "no-cache");
    headers.addHeaders(HttpHeader.PRAGMA, "no-cache");
    headers.addHeaders(HttpHeader.UPGRADE_INSECURE_REQUESTS, "1");
    headers.addHeaders(HttpHeader.USER_AGENT, UA);
    HttpRequest req = HttpClientUtil.getRequest(url, HttpMethod.GET, 5, headers, "");
    HttpResponse<String> resp = AsyncHttpClientUtil.getAsyncResponseString(req, null, null);
    log.info(HttpStatus.resolve(resp.statusCode()).toString());
    log.info(HttpHeaders.newHeaders(resp.headers().map()).toString());
    log.info(resp.body());
  }

  public static class Demo {
    private Integer p;

    public Demo() {
      p = -1;
    }

    public String newString(String str, Integer i) {
      return "{\"" + str + "\": " + i + "}";
    }

    public String noArgs() {
      return "Demo::new.noArgs";
    }

    public void setP(Integer p) {
      this.p = p;
    }

    public Integer getP() {
      return p;
    }

    @Override
    public String toString() {
      return Util.toJson(this);
    }
  }

  @Test
  public void reflectTest() throws Exception {
//    log.info(Demo.class.getName());
    Class<?> tClazz = Class.forName("org.seppiko.commons.utils.UtilsTest$Demo");
    var t = ClassUtil.newInstance(tClazz);
    log.info(ReflectionUtil.invokeMethod(t,  "newString", "demo", 1));
    log.info(ReflectionUtil.invokeMethod(t, "noArgs"));

    ReflectionUtil.invokeMethod(t, "setP", 1);
    log.info("p: " + ReflectionUtil.invokeMethod(t, "getP"));

    var f = ReflectionUtil.findField(tClazz, "p", Integer.class);
    ReflectionUtil.setField(f, true, t, 2);
    log.info("reset p: " + ReflectionUtil.invokeMethod(t, "getP"));

    log.info(ReflectionUtil.invokeMethod(t, "toString"));

    for (Method declaredMethod : ReflectionUtil.getAllDeclaredMethods(tClazz)) {
      log.info("Class " + tClazz.getName() + " found method " + declaredMethod.getName());
    }
  }

  @Test
  public void dynamicProxyTest() throws Exception {
    var proxyInstance = ProxyFactory.newInstance(this.getClass().getClassLoader(), Map.class,
        new DynamicInvocationHandler<>(new HashMap<>()));

    proxyInstance.put("hello", "world");
    log.info(Util.toJson(proxyInstance));
  }

  @Test
  public void datetimeTest() {
//    log.info("current time: " + DatetimeUtil.toEpochMilliSecond( ZonedDateTime.of(LocalDateTime.now(), ZoneId.systemDefault()) ) );

    log.info("Database: " + DatetimeUtil.DATABASE_DATE_TIME.format(LocalDateTime.now()));

    ZonedDateTime now = ZonedDateTime.now(ZoneId.of("+09:00"));
    log.info("RFC3339 offset: " + DatetimeUtil.RFC_3339_OFFSET_DATE_TIME.format(now));
    log.info("ISO datetime: " + DateTimeFormatter.ISO_DATE_TIME.format(LocalDateTime.now()));
    log.info("Basic ISO datetime: " + DatetimeUtil.BASIC_ISO_DATE.format(LocalDate.now()) + " " + DatetimeUtil.BASIC_ISO_TIME.format(LocalTime.now()));

    ZonedDateTime utcNow = ZonedDateTime.now(Clock.systemUTC());
    log.info("ISO: " + DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(utcNow));
    log.info("ISO time: " + DateTimeFormatter.ISO_OFFSET_TIME.format(utcNow));
    log.info("Basic ISO time: " + DatetimeUtil.BASIC_ISO_OFFSET_TIME.format(utcNow));
  }

  @Test
  public void mathTest() {
    ArrayList<BigDecimal> numList = new ArrayList<>();
    numList.add(new BigDecimal("1"));
    numList.add(new BigDecimal("2.5"));
    numList.add(new BigDecimal("7"));
    numList.add(new BigDecimal("9.2"));
    numList.add(new BigDecimal("3.67"));
    numList.add(new BigDecimal("5"));

    log.info("max: " + MathUtil.maxDecimal(numList));
    log.info("min: " + MathUtil.minDecimal(numList));
    log.info("avg with rounding: " + MathUtil.avgDecimal(numList, RoundingMode.HALF_UP));
    log.info("avg: " + MathUtil.avgDecimal(numList));

    log.info("2^2=" + MathUtil.pow(2, 2));
  }

  @Test
  public void numberTest() {
    log.info("" + StringUtil.isNumeric("0.0"));
    log.info("" + StringUtil.isNumeric("12"));
    log.info("" + StringUtil.isNumeric("-12"));
    log.info("" + StringUtil.isNumeric("-12.34"));
    log.info("" + StringUtil.isNumeric("-0.0"));
    log.info("" + StringUtil.isNumeric("－０．２４"));
    log.info("--------");
    log.info("" + StringUtil.isInteger("-1"));
    log.info("" + StringUtil.isInteger("12"));
    log.info("" + StringUtil.isInteger("12.0"));
    log.info("" + StringUtil.isInteger("12.34"));
    log.info("" + StringUtil.isInteger("２４"));
    log.info("--------");
    log.info("" + StringUtil.isDecimal("-1"));
    log.info("" + StringUtil.isDecimal("-1.2"));
    log.info("" + StringUtil.isDecimal("23"));
    log.info("" + StringUtil.isDecimal("23.34"));
    log.info("" + StringUtil.isDecimal("２４．６８"));
  }

  @Test
  public void baseDigitTest() {
    BigInteger num = BigInteger.valueOf(9223372036854775807L);
    log.info("Number: " + num);

    log.info("--- Base62 ----");
    String base62 = Base62.encode(num);
    log.info("Base62: " + base62);
    log.info("Decode: " + Base62.decode(base62));

    byte[] base62Bytes = "Seppiko Commons Util Base62 converter between text and base62 string.".getBytes();
    base62 = Base62.encode(base62Bytes);
    log.info("Base62: " + base62);
    log.info("Decode: " + new String(Base62.decodeToBytes(base62)));

    log.info("--- Base36 ----");
    String base36 = Base36.encode(num);
    log.info("Base36: " + base36);
    log.info("Decode: " + Base36.decode(base36));

    byte[] base36Bytes = "Seppiko Commons Util Base36 converter between text and base36 string.".getBytes();
    base36 = Base36.encodeBytes(base36Bytes);
    log.info("Base36: " + base36);
    log.info("Decode: " + new String(Base36.decodeBytes(base36)));
  }

  @Test
  public void hashTest() throws Exception {
    byte[] data = "SCU::Hash-Test".getBytes(StandardCharsets.UTF_8);
    log.info("Data: " + new String(data));
    log.info("SHA256: " + HashUtil.mdHashString(MessageDigestAlgorithms.SHA_256, CryptoUtil.NONPROVIDER, data));

    byte[] key = "1234".getBytes(StandardCharsets.UTF_8);
    log.info("Salt: " + new String(key));
    log.info("HmacSHA256: " + HashUtil.hmacHashString(HmacAlgorithms.HMAC_SHA_256, CryptoUtil.NONPROVIDER, data, key));

    byte[] salt = new byte[32];
    KeyGeneratorUtil.secureRandom().nextBytes(salt);
    log.info("Dynamic Salt: " + HexUtil.encode(salt));
    char[] pwd = new String(data).toCharArray();
    String pbkdf2Result = HashUtil.pbkdf2HashString(HmacAlgorithms.HMAC_SHA_256, CryptoUtil.NONPROVIDER,
        pwd, salt, 1000, salt.length * 8);
    log.info("PBKDF2WithHmacSHA256: " + pbkdf2Result);
  }

  @Test
  public void securityTest() {
    try {
      String data = "SCU::Security-Test";
      log.info("Text: " + data);
      byte[] result;
      byte[] key;

      SecureRandom secureRandom = KeyGeneratorUtil.secureRandom();

      byte[] iv = new byte[12];
      secureRandom.nextBytes(iv);
      log.info("IV: " + Base64Util.encodeString(iv) );

      log.info("--- ChaCha20-Poly1305 ---");
      key = secureRandom.generateSeed(32);
      log.info("Salt: " + Base64Util.encodeString(key) );

      SecretKeySpec keySpec = KeySpecUtil.getSecret(key, "ChaCha20-Poly1305");
      IvParameterSpec ivParamSpec = ParameterSpecUtil.getIV(iv);

      SymmetricUtil symcrypt = new SymmetricUtil("ChaCha20-Poly1305");
      symcrypt.setKey(keySpec);
      symcrypt.setParameterSpec(ivParamSpec);
      result = symcrypt.encrypt(data.getBytes());
      log.info("Encrypt: " + Base64Util.encodeString(result));
      result = symcrypt.decrypt(result);
      log.info("Decrypt: " + new String(result));

      log.info("--- AES_128/GCM/NoPadding ---");
      key = secureRandom.generateSeed(16);
      log.info("Key: " + Base64Util.encodeString(key) );
      keySpec = KeySpecUtil.getSecret(key, "AES");
      GCMParameterSpec gcmParamSpec = ParameterSpecUtil.getGCM(128, iv);

      symcrypt = new SymmetricUtil("AES_128/GCM/NoPadding");
      symcrypt.setKey(keySpec);
      symcrypt.setParameterSpec(gcmParamSpec);
      result = symcrypt.encrypt(data.getBytes());
      log.info("Encrypt: " + Base64Util.encodeString(result));
      result = symcrypt.decrypt(result);
      log.info("Decrypt: " + new String(result));

      log.info("--- RSA ---");
      KeyPair keyPair = KeyGeneratorUtil.keyPairGenerator("RSA", CryptoUtil.NONPROVIDER, 1024);
      RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
      log.info("PrivateKey: " + Base64Util.encodeString(privateKey.getEncoded()));
      RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
      log.info("PublicKey: " + Base64Util.encodeString(publicKey.getEncoded()));

      AsymmetricUtil asymcrypt = new AsymmetricUtil("RSA");
      asymcrypt.setKeyPair(publicKey, privateKey);
      result = asymcrypt.encrypt(data.getBytes());
      log.info("Encrypt: " + Base64Util.encodeString(result));
      result = asymcrypt.decrypt(result);
      log.info("Decrypt: " + new String(result));
    } catch (Exception ex) {
      log.error("", ex);
    }
  }

  @Test
  public void signTest() throws Exception {
    final String algorithmName = "Ed25519";
    String data = "SCU::Signature-Test";
    log.info("Origin: " + data);

    KeyPair keyPair = KeyGeneratorUtil.keyPairGenerator("EdDSA", CryptoUtil.NONPROVIDER, 255);
    log.info("PrivateKey: " + Base64Util.encodeString(keyPair.getPrivate().getEncoded()));
    log.info("PublicKey: " + Base64Util.encodeString(keyPair.getPublic().getEncoded()));

    byte[] result = SignatureUtil.sign(algorithmName, keyPair.getPrivate(), data.getBytes());
    log.info("SignMsg: " + Base64Util.encodeString(result));

    log.info("Verify: " + SignatureUtil.verify(algorithmName, keyPair.getPublic(), data.getBytes(), result));
  }

  @Test
  public void nmpTest() {
    try {
      CipherNameUtil nmp = new CipherNameUtil("AES_128/GCM/NoPadding");
      log.info(String.format("name: %s, mode: %s, padding: %s", nmp.getName(), nmp.getMode(), nmp.getPadding()));

      nmp = new CipherNameUtil("AES_128");
      log.info(String.format("name: %s, mode: %s, padding: %s", nmp.getName(), nmp.getMode(), nmp.getPadding()));
    } catch (Exception ex) {
      log.error("", ex);
    }
  }

  @Test
  public void stringTest() {
    String data = "seppiko";
    log.info(StringUtil.toFirstUpperCase(data));
    data = StringUtil.toFullWidth(data);
    log.info(StringUtil.toFirstUpperCase(data));

    log.info("---");
    byte[] bs = new byte[]{(byte) 0x23, (byte) 0x8A, (byte) 0x9E, (byte) 0x03};
    log.info("Origin: " + Arrays.toString(bs));
    data = HexUtil.encodeString(bs);
    log.info("Hex: " + data.toUpperCase());
    log.info("Decode: " + Arrays.toString( HexUtil.decodeString(data) ) );

    log.info("---");
    log.info("Test: " + StringUtil.unicodeEncode("¥１５０"));
    data = "雪彦のプロジェクト😃🇯🇵";
    String unicode = StringUtil.unicodeEncode(data);
    log.info(unicode);
    log.info(StringUtil.unicodeDecode(unicode));

    String[] strs1 = StringUtil.splitToArray("12345678", 3);
    log.info("Split array: " + Arrays.toString(strs1));
  }

  @Test
  public void crcTest() {
    byte[] data = "SCU::CRC-Test".getBytes();
    log.info("PLAINTEXT: " + new String(data));
    log.info("CRC16    : " + CRCUtil.getCRC16M(data) );
    log.info("CRC24    : " + CRCUtil.getCRC24C(data) );
    log.info("CRC32    : " + CRCUtil.getCRC32(data) );
    log.info("CRC64    : " + CRCUtil.getCRC64E(data) );
  }

  @Test
  public void radixTest() {
    log.info("" + MathUtil.convertDecimalTo(500, CharUtil.LOWERCASE));
    log.info("" + MathUtil.convertToDecimal("sf", CharUtil.LOWERCASE));
  }

  @Test
  public void envTest() {
//    log.info(System.getProperties().toString());
    log.info("JVM Version: " + System.getProperty("java.specification.version"));
    log.info("Java Version: " + System.getProperty("java.version"));
    log.info("OS: " + System.getProperty("os.name") + " " + System.getProperty("os.version"));
    log.info("OS Arch: " + System.getProperty("os.arch"));
  }
}
