/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

/**
 * Mock JDBC ResultSet
 *
 * @author Leonard Woo
 */
public class MockResultSet {

  private final Map<String, Integer> columnIndices;
  private final Object[][] data;
  private int rowIndex;

  private MockResultSet(final String[] columnNames, final Object[][] data) {
    this.columnIndices = IntStream.range(0, columnNames.length)
        .boxed()
        .collect(Collectors.toMap(
            k -> columnNames[k],
            Function.identity(),
            (a, b) -> { throw new RuntimeException("Duplicate column " + a); },
            LinkedHashMap::new
        ));
    this.data = data;
    this.rowIndex = -1;
  }

  public ResultSet buildMock() throws SQLException {
    final var rs = Mockito.mock(ResultSet.class);

    // mock rs.next()
    Mockito.doAnswer(invocation -> {
      rowIndex++;
      return rowIndex < data.length;
    }).when(rs).next();

    // mock rs.getString(columnName)
    Mockito.doAnswer(invocation -> {
      final var columnName = invocation.getArgument(0, String.class);
      final var columnIndex = columnIndices.get(columnName);
      return (String) data[rowIndex][columnIndex];
    }).when(rs).getString(ArgumentMatchers.anyString());

    // mock rs.getString(columnIndex)
    Mockito.doAnswer(invocation -> {
      final var index = invocation.getArgument(0, Integer.class);
      return (String)data[rowIndex][index - 1];
    }).when(rs).getString(ArgumentMatchers.anyInt());

    // mock rs.getInt(columnName)
    Mockito.doAnswer(invocation -> {
      final var columnName = invocation.getArgument(0, String.class);
      final var columnIndex = columnIndices.get(columnName);
      return (Integer) data[rowIndex][columnIndex];
    }).when(rs).getInt(ArgumentMatchers.anyString());

    // mock rs.getObject(columnName)
    Mockito.doAnswer(invocation -> {
      final var columnName = invocation.getArgument(0, String.class);
      final var columnIndex = columnIndices.get(columnName);
      return data[rowIndex][columnIndex];
    }).when(rs).getObject(ArgumentMatchers.anyString());

    // mock rs.getObject(columnIndex)
    Mockito.doAnswer(invocation -> {
      final var index = invocation.getArgument(0, Integer.class);
      return data[rowIndex][index - 1];
    }).when(rs).getObject(ArgumentMatchers.anyInt());

    final var rsmd = Mockito.mock(ResultSetMetaData.class);

    // mock rsmd.getColumnCount()
    Mockito.doReturn(columnIndices.size()).when(rsmd).getColumnCount();

    // mock rsmd.getColumnName(int)
    Mockito.doAnswer(invocation -> {
      final var index = invocation.getArgument(0, Integer.class);
      return columnIndices.keySet().stream().skip(index - 1).findFirst().orElse("");
    }).when(rsmd).getColumnName(ArgumentMatchers.anyInt());

    // mock rs.getMetaData()
    Mockito.doReturn(rsmd).when(rs).getMetaData();

    return rs;
  }

  /**
   * Creates the mock ResultSet.
   *
   * @param columnNames the names of the columns
   * @param data data arrays
   * @return a mocked ResultSet
   * @throws SQLException Data mock exception
   */
  public static ResultSet create(final String[] columnNames, final Object[][] data) throws SQLException {
    return new MockResultSet(columnNames, data).buildMock();
  }
}
