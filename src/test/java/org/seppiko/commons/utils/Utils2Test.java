/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.utils;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.random.RandomGenerator;
import java.util.random.RandomGeneratorFactory;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.seppiko.commons.utils.codec.HexUtil;
import org.seppiko.commons.utils.concurrent.RetryableTask;
import org.seppiko.commons.utils.crypto.PHCFormatUtil;
import org.seppiko.commons.utils.http.URIUtil;
import org.seppiko.commons.utils.http.UriBuilder;
import org.seppiko.commons.utils.jdbc.ConnectionUtil;
import org.seppiko.commons.utils.jdbc.ResultSetUtil;
import org.seppiko.commons.utils.jdbc.SQLExecutor;
import org.seppiko.commons.utils.reflect.ReflectionUtil;

/**
 * @author Leonard Woo
 */
public class Utils2Test {

  private static final Logger log = LogManager.getLogger(Utils2Test.class);

//  @Test
//  public void throwTest() {
//    try {
//      throw new Exception("test");
//    } catch (Throwable t) {
//      throw Assert.sneakyThrow(t);
//    }
//  }

  @Test
  public void ioTest() {
    try {
      File file = new File(System.getProperty("user.dir") + "/target/test/r");
      if (file.exists()) {
        log.info("Directory: " + file.isDirectory());
        log.info("File: " + file.isFile());
        log.info("Absolute: " + file.isAbsolute());
        log.info("Hidden: " + file.isHidden());
        log.info("exists: " + file.exists());
        log.info("Length: " + file.length());
      }
//    file.mkdirs();
//    file.createNewFile();
//    log.info("pathname: " + file.getPath());
      IOStreamUtil.writeFile("263".getBytes(StandardCharsets.UTF_8), file, true);
    } catch (Exception ex) {
      log.error("", ex);
    }
  }

  @Test
  public void serialTest() throws Exception {
    var data = "SCU::Serialization-Test";
    byte[] bins = SerializationUtil.serialize(data);
    log.info("Serialize: " + HexUtil.encodeString(bins));
    log.info("Deserialize: " + SerializationUtil.deserialize(bins, data.getClass()));
  }

  @Test
  public void pathTest() {
    String pathname = "\\tmp\\test\\demo.txt";
    int lastVirguleIndex = getLastVirguleIndex(pathname);
    log.info("Last virgule index: " + lastVirguleIndex);
    log.info("First index of '.' is " + pathname.indexOf(".", lastVirguleIndex));
    log.info("last dirs: " + pathname.substring(0, lastVirguleIndex));
  }

  private int getLastVirguleIndex(String pathname) {
    int lastVirguleIndex = pathname.lastIndexOf("/");
    if (lastVirguleIndex < 0)  {
      lastVirguleIndex = pathname.lastIndexOf("\\");
    }
    return lastVirguleIndex;
  }

  @Test
  public void uriTest() throws Exception {
    String uri = "https://commons.seppiko.org/commons/docs/commons-utils/org/seppiko/commons/utils/http/package-summary.html?123=33";
    String fn = URIUtil.getLastPathname(uri);
    log.info("File Pathname: " + fn);
    log.info("File Extension: " + URIUtil.getFileExtension(fn));
    log.info(UriBuilder.fromString(uri).withScheme("http").addQuery("tags", "1").withFragment("summary").toUri());

    log.info(UriBuilder.create().withScheme("http").withHostname("127.0.0.1").addPath("test4").toUri());
    log.info(UriBuilder.create().withScheme("http").withHostname("::1").addPath("test6").toUri());
    log.info(UriBuilder.create().withScheme("http").withHostname("[::1]").addPath("test6-2").toUri());
    log.info(UriBuilder.create().withScheme("http").withHostname("example.com").addPath("testd").toUri());
  }

  @Test
  public void widthTest() {
    log.info(StringUtil.toFullWidth("SCU::StringUtil#toFullwidth-Test"));
    log.info(StringUtil.toHalfWidth("ＳＣＵ：：ＳｔｒｉｎｇＵｔｉｌ＃ｔｏＦｕｌｌｗｉｄｔｈ－Ｔｅｓｔ"));
  }

  @Test
  public void digit2Test() {
    log.info("９+2=" + new BigDecimal("９").add(new BigDecimal("2")).intValue());
    log.info(NumberUtil.convert("＋６．３", NumberUtil.LATIN_DIGITS));
    log.info(NumberUtil.convert("＋２", NumberUtil.LATIN_DIGITS));
    log.info(NumberUtil.convert("-２.9", NumberUtil.FULLWIDTH_DIGITS));
    log.info(NumberUtil.convert("-5", NumberUtil.FULLWIDTH_DIGITS));
  }

  @Test
  public void jdbcTest() throws Exception {
//    var conn = ConnectionUtil.getConnection("org.mariadb.jdbc.Driver", "jdbc:mariadb://localhost:3306/test", "root", "123456");
//    var executor = new SQLExecutorImpl(conn);
//    final var rs = executor.query("select * from test");
    final var rs = MockResultSet.create(
        new String[] { "id", "name" }, //columns
        new Object[][] { // data
            { 1, "te" },
            { 2, "es" },
            { 3, "st" }
        });

    ArrayList<DemoRecordEntity> test = ResultSetUtil.convert(rs, DemoRecordEntity.class);
    log.info(test.size() + " " + test.toString());
    // ResultSet can't be converted multiple times.
//    ArrayList<HashMap<String, Object>> test2 = ResultSetUtil.convert(rs);
//    log.info(test2.size() + " " + Util.toJson(test2));
    ResultSetUtil.close(rs);
  }

  private static final String badStrReg = "\\b(and|or)\\b.{1,6}?(=|>|<|\\bin\\b|\\blike\\b)|\\/\\*.+?\\*\\/|<\\s*script\\b|\\bEXEC\\b|UNION.+?SELECT|UPDATE.+?SET|INSERT\\s+INTO.+?VALUES|(SELECT|DELETE).+?FROM|(CREATE|ALTER|DROP|TRUNCATE)\\s+(TABLE|DATABASE)";
  static class SQLExecutorImpl extends SQLExecutor {
    public SQLExecutorImpl(Connection conn) {
      super(conn);
    }

    @Override
    public void sqlValidation(String sql) throws SQLException, IllegalArgumentException {
      if (StringUtil.isNullOrEmpty(sql)) {
        throw new IllegalArgumentException("SQL must not be null or empty.");
      }
      if (RegexUtil.getMatcher(badStrReg, Pattern.CASE_INSENSITIVE, sql.toLowerCase()).find()) {
        throw new SQLException("SQL statement [ " + sql + " ] contains illegal keywords.");
      }
    }
  }

  @Test
  public void collectionTest() {
    ArrayList<DemoEntity> list = new ArrayList<>();
    list.add(new DemoEntity(1, "test"));
    list.add(new DemoEntity(2, "collection"));
    list.forEach(t -> {
      log.info(t.toString());
    });
    log.info("list toString: " + CollectionUtil.toString(list, DemoEntity::toString, ", "));
    log.info("map toString: " + CollectionUtil.toString(list, entity -> entity.getId() + ": " + entity.getName(), ", "));

    CollectionUtil.populateList(list, DemoEntity::getId).forEach(log::info);

    list.add(new DemoEntity(2, "util"));

    Map<Integer, DemoEntity> map = CollectionUtil.populateMap(list, DemoEntity::getId);
//    map.forEach((k, v) -> {
//      log.info(k + " " + v.toString());
//    });
    log.info("object toString: " + Util.toJson(map));
  }

  @Test
  public void arrayTest() {
    log.info("<" + ((char) -1) + ">");

    Integer[] ias = new Integer[10];
    Arrays.fill(ias, -1);
    log.info("Array >> " + Arrays.toString(ias));
  }

  @Test
  public void retryTest() {
    var task = RetryableTask.caller(() -> {
      log.info("retrying");
      return false;
    }, 3, Util::instanceOfBoolean, 1, TimeUnit.SECONDS);
    log.info("<RETURN>: " + task.toString());
  }

  @Test
  public void randomTest() throws Exception {
    RandomGeneratorFactory<RandomGenerator> factory = RandomGeneratorFactory.of("L128X128MixRandom");
    RandomGenerator generator = factory.create();
    IntStream is = generator.ints(10,0,100); // count, min, max
    int[] randoms = is.toArray();
    for (int i = 0; i < randoms.length; i++) {
      log.info("Num <" + i + ">: " + randoms[i]);
    }
    log.info("random count: " + randoms.length);
  }

  @Test
  public void versionTest() {
    VersionUtil vers = VersionUtil.parser("2.11.0-beta.1+build-20240219");
//    log.info("Major  : " + vers.major());
//    log.info("Minor  : " + vers.minor());
//    log.info("Path   : " + vers.patch());
//    log.info("Qualify: " + vers.qualifier());
//    log.info("Build  : " + vers.build());
    log.info("Test 0 : " + Util.toJson(vers));
    log.info("Test 1 : " + Util.toJson(VersionUtil.parser("1.1.2-prerelease+meta")));
    log.info("Test 2 : " + Util.toJson(VersionUtil.parser("1.0.0-alpha")));
    log.info("Test 3 : " + Util.toJson(VersionUtil.parser("1.0.0-rc.1+build.20240219")));
    log.info("Test 4 : " + Util.toJson(VersionUtil.parser("1.2.3-SNAPSHOT-1")));
    log.info("Test 5 : " + Util.toJson(VersionUtil.parser("0.0.4")));
    log.info("Test 6 : " + Util.toJson(VersionUtil.parser("10.20.30")));
    log.info("Test 7 : " + Util.toJson(VersionUtil.parser("2.0.0+build.1848")));
  }

  @Test
  public void bitTest() {
    Bits bits = new Bits();
    bits.putBit(true, 1);
    bits.putBit(true, 3);
    bits.putBit(true, 5);
    bits.putBit(true, 7);
    log.info(BytesUtil.toBinaryString(bits.getByte()));

    bits.clean();
    bits.putByte((byte) 0xff);
    log.info(BytesUtil.toBinaryString(bits.getByte()));
  }

  @Test
  public void phcTest() {
    HashMap<String, Object> params = new HashMap<>();
    params.put("i", 1000);
    String salt = "fGZKIF68JTlz+pZJ6GenUh5gmmh8aw7kfFsgWFHV/sU=";
    String hash = "D4aCZ0/1G4UfbJSXhU3itHqWo1H7yptQmBixPPIQhpk=";
    String phcString = PHCFormatUtil.format("pbkdf2-HmacSHA256", null, params, salt, hash);
    log.info("PHC: " + phcString);
    log.info("PHCFormatUtil.Entity: " + Util.toJson(PHCFormatUtil.parser(phcString)));
  }

  record typeEntity(int i, String s, Object o, AsRef<?> as) {
  }

  @Test
  public void typeTest() throws Exception {
    Field[] fields = ReflectionUtil.getAllFields(typeEntity.class);
    for (Field field : fields) {
      log.info("Type: " + field.getType().getName());
      log.info("Name: " + field.getName());
    }
  }

  @Test
  public void e2cTest() throws Exception {
    var entity = new DemoRecordEntity(1, "demo");
    log.info("Entity: " + entity);

    LinkedHashMap<String, String> fieldMap = new LinkedHashMap<>();
    fieldMap.put("id", "id");
    fieldMap.put("name", "name");

    Class<?> entityClass = DemoEntity.class;
    var entity2 = ObjectUtil.entityConvert(entity, entityClass, fieldMap, true);
    log.info("Result: " + (entity2 == null ? (entityClass.getName() + " is null") : entity2));
  }

  @Test
  public void m2cTest() throws Exception {
    LinkedHashMap<String, Object> fieldMap = new LinkedHashMap<>();
    fieldMap.put("id", 1);
    fieldMap.put("name", "test");

    Class<?> entityClass = DemoRecordEntity.class;
    var entity = ObjectUtil.mapConvert(fieldMap, entityClass);
    log.info((entity == null ? (entityClass.getName() + " is null") : entity));
  }
}
